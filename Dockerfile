#基础镜像
FROM openjdk:8u212-jdk-alpine
ENV jarname=hhxy-web.jar

# 容器中创建目录
RUN mkdir -p /app

COPY hhxy-web/target/${jarname} /app/${jarname}
# COPY entrypoint.sh /

# ENTRYPOINT ["/bin/sh","entrypoint.sh"]

ENTRYPOINT ["java", "-jar", "/app/hhxy-web.jar"]