```aidl
helm                                    - chart包的目录名
├── templates                           - k8s配置模版目录
│   ├── deployment.yaml                 - Deployment配置模板，定义如何部署Pod
│   ├── _helpers.tpl                    - 以下划线开头的文件，helm视为公共库定义文件，用于定义通用的子模版、函数、变量等
│   ├── ingress.yaml                    - Ingress配置模板，定义外部如何访问Pod提供的服务，类似于Nginx的域名路径配置
│   ├── NOTES.txt                       - chart包的帮助信息文件，执行helm install命令成功后会输出这个文件的内容
│   └── service.yaml                    - Service配置模板，配置访问Pod的服务抽象，有NodePort与ClusterIp等
|── values.yaml                         - chart包的参数配置文件，各模版文件可以引用这里的参数
├── Chart.yaml                          - chart定义，可以定义chart的名字，版本号等信息
├── charts                              - 依赖的子包目录，里面可以包含多个依赖的chart包，一般不存在依赖，我这里将其删除了
```
