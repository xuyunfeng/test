package com.hhxy.common.cache;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @since 2021/4/14 21:34
 */
@ConfigurationProperties(prefix = "cache")
@Component
public class CacheConfig implements CommandLineRunner {

    private String model = "memory";

    private String hostPorts;

    private String authKey = null;

    private int maxIdle = 8;

    private int maxTotal = 8;

    private int minIdle = 1;

    private long maxWait = -1;

    private boolean testOnBorrow = false;

    private int connTimeout = 2000;

    private int readTimeout = 2000;

    public int getConnTimeout() {
        return connTimeout;
    }

    public CacheConfig setConnTimeout(int connTimeout) {
        this.connTimeout = connTimeout;
        return this;
    }

    public int getReadTimeout() {
        return readTimeout;
    }

    public CacheConfig setReadTimeout(int readTimeout) {
        this.readTimeout = readTimeout;
        return this;
    }

    public int getMaxIdle() {
        return maxIdle;
    }

    public CacheConfig setMaxIdle(int maxIdle) {
        this.maxIdle = maxIdle;
        return this;
    }

    public int getMaxTotal() {
        return maxTotal;
    }

    public CacheConfig setMaxTotal(int maxTotal) {
        this.maxTotal = maxTotal;
        return this;
    }

    public int getMinIdle() {
        return minIdle;
    }

    public CacheConfig setMinIdle(int minIdle) {
        this.minIdle = minIdle;
        return this;
    }

    public long getMaxWait() {
        return maxWait;
    }

    public CacheConfig setMaxWait(long maxWait) {
        this.maxWait = maxWait;
        return this;
    }

    public boolean isTestOnBorrow() {
        return testOnBorrow;
    }

    public CacheConfig setTestOnBorrow(boolean testOnBorrow) {
        this.testOnBorrow = testOnBorrow;
        return this;
    }

    public String getModel() {
        return model;
    }

    public CacheConfig setModel(String model) {
        this.model = model;
        return this;
    }

    public String getHostPorts() {
        return hostPorts;
    }

    public CacheConfig setHostPorts(String hostPorts) {
        this.hostPorts = hostPorts;
        return this;
    }

    public String getAuthKey() {
        return authKey;
    }

    public CacheConfig setAuthKey(String authKey) {
        this.authKey = authKey;
        return this;
    }

    @Override
    public void run(String... args) throws Exception {
        CacheFactory.getInstance().buildCache(this);
    }
}
