/**
 * Project Name:BasicData
 * File Name:CacheConstants.java
 * Package Name:com.aostarit.framework.constants
 * Date:2019年1月31日上午9:54:16
 * Copyright (c) 2019, yangzhi@sgitg.sgcc.com.cn All Rights Reserved.
 *
*/

package com.hhxy.common.cache;

/**
 * ClassName:CacheConstants <br/>
 * Function: 缓存常量类. <br/>
 * Reason: 用于提供与缓存相关的常量值. <br/>
 *
 * @version 1.0
 * @see
 */
public class CacheConstants {
	
    /**
     * 缓存模式
     */
    public static final String CACHE_MODEL_KEY = "cache.model";

    /**
     * 内存缓存模式
     */
    public static final String CACHE_MODEL_MEMORY = "memory";

    /**
     * Redis缓存模式
     */
    public static final String CACHE_MODEL_REDIS = "redis";
    
    /**
     * Redis服务地址
     */
    public static final String CACHE_REDIS_SERVICE_ADDRESS = "cache.redis.service.address";
    
    /**
     * Redis服务授权码
     */
    public static final String CACHE_REDIS_AUTHKEY = "cache.redis.authkey";
    
    /**
     * Redis最大链接
     */
    public static final String CACHE_REDIS_MAXTOTAL = "cache.redis.maxtotal";
    
    /**
     * Redis链接超时时间
     */
    public static final String CACHE_REDIS_CONNECTION_TIMEOUT = "cache.redis.connection.timeout";
    
    /**
     * Redis数据读取超时时间
     */
    public static final String CACHE_REDIS_READ_TIMEOUT = "cache.redis.read.timeout";

    /**
     * Redis最大空闲连接数
     */
    public static final String CACHE_REDIS_MAX_IDLE = "cache.redis.max.idle";
    
    /**
     * Redis最小空闲连接数
     */
    public static final String CACHE_REDIS_MIN_IDLE = "cache.redis.min.idle";
    
    /**
     * Redis当链接满了之后，调用者最大等待时间
     */
    public static final String CACHE_REDIS_MAX_WAIT_MILLIS = "cache.redis.max.wait.millis";
    
    /**
     * Redis获取链接时，是否对链接进行测试，无效链接被移除，<b>该请求会多走一次ping</b>
     */
    public static final String CACHE_REDIS_TEST_ON_BORROW = "cache.redis.test.on.borrow";
    
    /**
     * Redis默认数据库
     */
    public static final String CACHE_REDIS_DB = "cache.redis.db";
    
    /**
     * 默认的超时时间，链接超时时间、数据读取超时时间
     */
    public static final int DEFAULT_REDIS_TIMEOUT = 2000;
    
    /**
     * 默认数据库
     */
    public static final int DEFAULT_REDIS_DB = 0;
    
    /**
     * 默认最大的重定向数
     */
    public static final int DEFAULT_MAX_REDIRECTIONS = 5;
    
    /**
     * 当前登录用户
     */
    public static final String DEFAULT_CURRENT_LOGIN_USER = "current.login.user";
    
}
