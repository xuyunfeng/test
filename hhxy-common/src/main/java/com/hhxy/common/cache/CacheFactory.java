package com.hhxy.common.cache;

import com.hhxy.common.cache.impl.MemoryStorageCacheServiceImpl;
import com.hhxy.common.cache.impl.RedisCacheServiceImpl;
import com.hhxy.common.cache.impl.RedisSingleCacheServiceImpl;
import com.hhxy.common.exception.CacheException;
import org.apache.commons.lang3.StringUtils;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisPoolConfig;

import java.util.HashSet;
import java.util.Set;

/**
 * 缓存服务实例获取工厂
 * <p>
 * <br>
 * 使用方式</br>
 * <p>
 * 使用{@link CacheFactory#getInstance()} 获取实例
 * 
 *
 */
public class CacheFactory {

	private CacheService cacheService;
	
	private CacheService businessCacheService;

	private static CacheFactory cacheFactory;

	private CacheFactory() throws CacheException {
	}

	/**
	 * getInstance:获取缓存工厂实例. <br/>
	 * 缓存缓存工厂单例，此处可能因为缓存配置问题，或者秘钥加解密问题，导致启动失败，业务层决定如何处理.<br/>
	 *
	 * @author lenovo
	 * @return
	 * @throws CacheException
	 */
	public static CacheFactory getInstance() throws CacheException {
		if (cacheFactory == null) {
			cacheFactory = new CacheFactory();
		}
		return cacheFactory;
	}


	/**
	 * buildRedisCacheService:构建Redis缓存服务. <br/>
	 *
	 * @return
	 * @throws CacheException
	 */
	public void buildCache(CacheConfig cacheConfig) throws CacheException {
		if (CacheConstants.CACHE_MODEL_MEMORY.equalsIgnoreCase(cacheConfig.getModel())) {
			cacheService = new MemoryStorageCacheServiceImpl();
		} else {
			String authKey = StringUtils.isBlank(cacheConfig.getAuthKey()) ? null : cacheConfig.getAuthKey();
			Set<HostAndPort> addresses = buildHostAndPort(cacheConfig);
			JedisPoolConfig poolConfig = buildPoolConfig(cacheConfig);
			if (addresses.size() == 1) {
				HostAndPort hostAndPort = addresses.iterator().next();
				cacheService = new RedisSingleCacheServiceImpl(poolConfig, hostAndPort.getHost(), hostAndPort.getPort(), authKey, cacheConfig.getConnTimeout());
			} else {
				cacheService = new RedisCacheServiceImpl(poolConfig, addresses, authKey, cacheConfig.getConnTimeout(), cacheConfig.getReadTimeout());
			}
		}
	}

	/**
	 * buildHostAndPort:构建服务地址. <br/>
	 *
	 * @return
	 */
	protected Set<HostAndPort> buildHostAndPort(CacheConfig cacheConfig) {
		String[] servers = cacheConfig.getHostPorts().split("\\|");
		Set<HostAndPort> hostAndPorts = new HashSet<>(servers.length);
		for (String server : servers) {
			String[] hostAndPort = server.split(":");
			HostAndPort address = new HostAndPort(hostAndPort[0], Integer.parseInt(hostAndPort[1]));
			hostAndPorts.add(address);
		}
		return hostAndPorts;
	}

	/**
	 * buildPoolConfig:构建连接池配置. <br/>
	 *
	 * @return
	 */
	protected JedisPoolConfig buildPoolConfig(CacheConfig cacheConfig) {
		JedisPoolConfig config = new JedisPoolConfig();
		// 最大空闲链接
		config.setMaxIdle(cacheConfig.getMaxIdle());
		// 最大链接
		config.setMaxTotal(cacheConfig.getMaxTotal());
		// 最小链接
		config.setMinIdle(cacheConfig.getMinIdle());
		// 调用者最大等待时间，默认-1永久等待，不建议的值
		config.setMaxWaitMillis(cacheConfig.getMaxWait());
		// 获取链接之前先测试
		config.setTestOnBorrow(cacheConfig.isTestOnBorrow());
		return config;
	}

	public void buid(CacheService businessCacheService) {
		this.businessCacheService = businessCacheService;
	}

	private CacheService getCacheService() {
		if (this.businessCacheService != null) {
			return this.businessCacheService;
		}
		return this.cacheService;
	}
	
	public boolean add(String key, Object value) throws CacheException {
		return getCacheService().add(key, value);
	}

	public boolean add(String key, Object value, int timeout) throws CacheException {
		return getCacheService().add(key, value, timeout);
	}

	public boolean delete(String key) throws CacheException {
		return getCacheService().delete(key);
	}

	public boolean update(String key, Object value) throws CacheException {
		return getCacheService().update(key, value);
	}

	public boolean update(String key, Object value, int timeout) throws CacheException {
		return getCacheService().add(key, value, timeout);
	}

	public Object get(String key) throws CacheException {
		return getCacheService().get(key);
	}

	public <T> T get(String key, Class<T> clazz) throws CacheException {
		return getCacheService().get(key, clazz);
	}

	public boolean isExist(String key) {
		return getCacheService().isExist(key);
	}
}
