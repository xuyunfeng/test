/*
 * 版权声明 国家电网, 版权所有 违者必究
 *
 * 系统名称：国家电网公司统一权限管理平台
 * 模块名称：统一授权集成组件
 * 版本信息：
 * 版本		日期				作者				备注
 * 3.0		2015-1-4		xmk		                     新建
 */
package com.hhxy.common.cache;

import com.hhxy.common.exception.CacheException;

import java.io.Serializable;

public interface CacheService extends Serializable {

	/**
	 * 异步添加缓存
	 * 
	 * @param key
	 *            缓存key值
	 * @param value
	 *            需要缓存的数据
	 * @return 缓存的数据
	 * @throws CacheException
	 */
	public boolean add(String key, Object value) throws CacheException;

	/**
	 * 异步添加缓存
	 * 
	 * @param key 		缓存key值
	 * 
	 * @param value 	需要缓存的数据
	 * 
	 * @param timeout 	缓存过期时间
	 * 
	 * @return 缓存的数据
	 * 
	 * @throws CacheException
	 */
	public boolean add(String key, Object value, int timeout) throws CacheException;

	/**
	 * 删除缓存
	 * 
	 * @param key 	缓存的key值
	 * @return 	缓存的数据
	 * @throws CacheException
	 */
	public boolean delete(String key) throws CacheException;

	/**
	 * 更新缓存
	 * 
	 * @param key
	 *            缓存的key值
	 * @param value
	 *            缓存的数据
	 * @return 缓存的数据
	 * @throws CacheException
	 */
	public boolean update(String key, Object value) throws CacheException;

	/**
	 * 更新缓存
	 * 
	 * @param key
	 *            缓存key值
	 * 
	 * @param value
	 *            需要缓存的数据
	 * @param timeout
	 *            缓存过期时间
	 * @throws CacheException
	 * 
	 * @return 缓存的数据
	 */
	public boolean update(String key, Object value, int timeout) throws CacheException;

	/**
	 * 根据key值查询缓存数据
	 * 
	 * @param key
	 *            缓存key值
	 * @return 缓存的对象
	 * 
	 * @throws CacheException
	 * 
	 */
	public Object get(String key) throws CacheException;

	/**
	 * 根据key值查询缓存数据，并进行类型转换
	 * 
	 * @param key
	 *            缓存key值
	 * 
	 * @return 缓存的数据
	 * 
	 * @param clazz
	 *            数据类型
	 * 
	 * 
	 * 
	 * @throws CacheException
	 */
	public <T> T get(String key, Class<T> clazz) throws CacheException;

	/**
	 * 缓存中是否存在指定key值的缓存对象
	 * 
	 * @param key
	 *            缓存的key值
	 * 
	 * @return true / false
	 */
	public boolean isExist(String key);
}
