package com.hhxy.common.cache.impl;

import com.alibaba.fastjson.JSONObject;
import com.hhxy.common.cache.CacheService;
import com.hhxy.common.exception.CacheException;
import com.hhxy.common.serialize.JdkSerialization;
import com.hhxy.common.serialize.Serialization;

import java.io.IOException;

public abstract class AbstractCacheService implements CacheService {

	private static final long serialVersionUID = 5373878568328451547L;
	
	protected Serialization serialization = new JdkSerialization();
	
	/**
	 * 添加缓存
	 * 
	 * @param key 缓存key值
	 * 
	 * @param value 需要缓存的数据
	 * 
	 * @throws CacheException
	 */
	@Override
	public boolean add(String key, Object value) throws CacheException {
		notNullKey(key);
		notNullValue(value);
		return doAdd(key, value, 0);
	}

	/**
	 * 添加缓存
	 * 
	 * @param key 缓存key值
	 * 
	 * @param value 需要缓存的数据
	 * 
	 * @param timeout 缓存过期时间
	 * 
	 * @throws CacheException
	 */
	@Override
	public boolean add(String key, Object value, int timeout) throws CacheException {
		notNullKey(key);
		notNullValue(value);
		return doAdd(key, value, timeout);
	}
	
	/**
	 * 更新缓存
	 * 
	 * @param key 缓存key值
	 * 
	 * @param value 需要缓存的数据
	 * 
	 * @throws CacheException
	 */
	@Override
	public boolean update(String key, Object value) throws CacheException {
		notNullKey(key);
		notNullValue(value);
		return doUpdate(key, value, 0);
	}

	/**
	 * 更新缓存
	 * 
	 * @param key 缓存key值
	 * 
	 * @param value 需要缓存的数据
	 * 
	 * @param timeout 缓存过期时间
	 * 
	 * @throws CacheException
	 */
	@Override
	public boolean update(String key, Object value, int timeout) throws CacheException {
		notNullKey(key);
		notNullValue(value);
		return doUpdate(key, value, timeout);
	}

	/**
	 * 删除缓存
	 * 
	 * @param key 缓存key值
	 * 
	 * @return 缓存的数据
	 * 
	 * @throws CacheException
	 */
	@Override
	public boolean delete(String key) throws CacheException {
		notNullKey(key);
		return doDelete(key);
	}

	/**
	 * 根据key值查询缓存数据
	 * 
	 * @param key 缓存key值
	 * 
	 * @return 缓存的数据
	 * 
	 * @throws CacheException
	 */
	@Override
	public Object get(String key) throws CacheException {
		notNullKey(key);
		return doGet(key);
	}

	/**
	 * 根据key值查询缓存数据，并进行类型转换
	 * 
	 * @param key 缓存key值
	 * 
	 * @param clazz 数据类型
	 * 
	 * @return 缓存的数据
	 * 
	 * @throws CacheException
	 */
	@Override
	public <T> T get(String key, Class<T> clazz) throws CacheException {
		notNullKey(key);
		return doGet(key, clazz);
	}


	/**
	 * 缓存中是否存在指定key值的缓存对象
	 * 
	 * @param key 缓存的key值
	 * 
	 * @return true / false
	 */
	@Override
	public boolean isExist(String key) {
		return doExists(key);
	}

	/**
	 * key值不能为空
	 * 
	 * @param key
	 * @throws CacheException 
	 */
	private void notNullKey(String key) throws CacheException {
		if (key == null) {
			throw new CacheException("When adding a cache, the key value cannot be empty");
		}
	}

	/**
	 * value值不能为空
	 * 
	 * @param value
	 * @throws CacheException 
	 */
	private void notNullValue(Object value) throws CacheException {
		if (value == null) {
			throw new CacheException("When adding a cache, the value cannot be empty");
		}
	}

	/**
	 * 将需要缓存的数据序列化成字符串
	 * 
	 * @param value
	 * @return
	 * @throws CacheException 
	 */
	protected String toString(Object value) throws CacheException {
		if (value instanceof String) {
			return (String) value;
		}
		try {
			return JSONObject.toJSONString(value);
		} catch (Exception e) {
			throw new CacheException(e.getMessage(), e);
		}
	}

	/**
	 * 缓存数据类型转换
	 * 
	 * @param value
	 * @param clazz
	 * @return
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 * @throws CacheException 
	 */
	@SuppressWarnings("unchecked")
	protected <T> T toObject(Object value, Class<T> clazz) throws ClassNotFoundException, IOException, CacheException {
		if (value == null) {
			return null;
		}
		if (clazz.isAssignableFrom(String.class)) {
			return (T) new String((byte[])value);
		}
		return (T) serialization.deserialize((byte[])value, clazz);
	}


	/**
	 * 缓存更新
	 * 
	 * @param key 键
	 * @param value 值
	 * @return 成功与否
	 */
	protected abstract boolean doUpdate(String key, Object value, int timeout)throws CacheException;

	/**
	 * 缓存添加
	 * 
	 * @param key 键
	 * @param value 值
	 * @return 成功与否
	 * @throws CacheException 
	 */
	protected abstract boolean doAdd(String key, Object value, int timeout) throws CacheException;


	/**
	 * 缓存删除
	 * 
	 * @param key 键
	 * @return 成功与否
	 */
	protected abstract boolean doDelete(String key);

	/**
	 * 缓存删除
	 * 
	 * @param key 键
	 * @return 成功与否
	 */
	protected abstract boolean doExists(String key);

	/**
	 * 获取缓存信息
	 * 
	 * @param key 键
	 * @return 成功与否
	 */
	protected abstract Object doGet(String key);

	/**
	 * 获取缓存信息
	 * 
	 * @param key 键
	 * @return 成功与否
	 */
	protected abstract <T> T doGet(String key, Class<T> clazz)throws CacheException;
}
