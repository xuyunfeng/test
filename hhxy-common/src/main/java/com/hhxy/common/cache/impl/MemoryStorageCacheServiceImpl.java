/**
 * Project Name:BasicData
 * File Name:MemoryStorageCacheService.java
 * Package Name:com.aostarit.framework.cache.support
 * Date:2019年1月30日下午3:06:03
 * Copyright (c) 2019, yangzhi@sgitg.sgcc.com.cn All Rights Reserved.
 *
*/

package com.hhxy.common.cache.impl;

import com.hhxy.common.exception.CacheException;

import java.util.concurrent.ConcurrentHashMap;

/**
 * ClassName:MemoryStorageCacheServiceImpl <br/>
 * Function: 内存存储缓存. <br/>
 * Reason:实现基于内存的缓存存储实现. <br/>
 *
 * @version
 * @see ConcurrentHashMap
 */
public class MemoryStorageCacheServiceImpl extends AbstractCacheService {

    /**
     * serialVersionUID:序列化ID.
     * 
     */
    private static final long serialVersionUID = 2980703303789618246L;

    private ConcurrentHashMap<String, Object> memoryStorage = new ConcurrentHashMap<String, Object>();

    @Override
    protected boolean doUpdate(String key, Object value, int timeout) throws CacheException {
        memoryStorage.put(key, value);
        return true;
    }

    @Override
    protected boolean doAdd(String key, Object value, int timeout) {
        memoryStorage.put(key, value);
        return true;
    }

    @Override
    protected boolean doDelete(String key) {
        memoryStorage.remove(key);
        return true;
    }

    @Override
    protected boolean doExists(String key) {
        return memoryStorage.containsKey(key);
    }

    @Override
    protected Object doGet(String key) {
        Object obj = memoryStorage.get(key);
        return obj;
    }

    @Override
    protected <T> T doGet(String key, Class<T> clazz) throws CacheException {
        Object value = memoryStorage.get(key);
        return toObject(value, clazz);
    }
    
    @SuppressWarnings("unchecked")
	@Override
    public <T> T toObject(Object value, Class<T> clazz) throws CacheException {
    	if (value == null) {
			return null;
		}
		if (value.getClass().isAssignableFrom(clazz)) {
			return (T) value;
		}
		throw new CacheException("Serialize object class type is <" + value.getClass().getName() + ">, but want was <" + clazz.getClass().getName() + ">, can't serializable!");
    }
}
