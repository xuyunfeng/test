/**
 * Project Name:BasicData
 * File Name:RedisCacheService.java
 * Package Name:com.aostarit.framework.cache.support
 * Date:2019年2月18日下午4:27:20
 * Copyright (c) 2019, yangzhi@sgitg.sgcc.com.cn All Rights Reserved.
 *
 */

package com.hhxy.common.cache.impl;

import com.hhxy.common.cache.CacheConstants;
import com.hhxy.common.exception.CacheException;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPoolConfig;

import java.io.IOException;
import java.util.Set;


/**
 * ClassName:RedisCacheServiceImpl <br/>
 * Function: Redis缓存. <br/>
 * Reason: 基于Redis实现的缓存存储. <br/>
 *
 * @version
 * @see
 */
public class RedisCacheServiceImpl extends AbstractCacheService {

	private JedisCluster cluster;

	public RedisCacheServiceImpl(Set<HostAndPort> nodes, String authKey) {
		this(new JedisPoolConfig(), nodes, authKey);
	}

	public RedisCacheServiceImpl(JedisPoolConfig jedisPoolConfig, Set<HostAndPort> nodes, String authKey) {
		this(jedisPoolConfig, nodes, authKey, CacheConstants.DEFAULT_REDIS_TIMEOUT);
	}

	public RedisCacheServiceImpl(JedisPoolConfig jedisPoolConfig, Set<HostAndPort> nodes, String authKey, int connectionTimeout) {
		this(jedisPoolConfig, nodes, authKey, connectionTimeout, CacheConstants.DEFAULT_REDIS_TIMEOUT);
	}

	public RedisCacheServiceImpl(JedisPoolConfig jedisPoolConfig, Set<HostAndPort> nodes, String authKey, int connectionTimeout, int soTimeout) {
		this(jedisPoolConfig, nodes, authKey, connectionTimeout, soTimeout, CacheConstants.DEFAULT_MAX_REDIRECTIONS);
	}

	public RedisCacheServiceImpl(JedisPoolConfig jedisPoolConfig, Set<HostAndPort> nodes, String authKey, int connectionTimeout, int soTimeout, int maxAttempts) {
		cluster = new JedisCluster(nodes, connectionTimeout, soTimeout, maxAttempts, authKey, jedisPoolConfig);
	}

	/**
	 * 序列化ID
	 */
	private static final long serialVersionUID = -8565107256587713998L;

	@Override
	protected boolean doUpdate(String key, Object value, int timeout) throws CacheException {
		if (doExists(key)) {
			try {
				doGetSet(key, value, timeout);
			} catch (IOException e) {
				throw new CacheException(e);
			}
		} else {
			try {
				doSet(key, value, timeout);
			} catch (IOException e) {
				throw new CacheException(e);
			}
		}
		doExpire(key, timeout);
		return true;
	}

	protected void doSet(String key, Object value, int timeout) throws IOException {
		if (value instanceof String) {
			cluster.set(key.getBytes(), ((String) value).getBytes());
		} else {
			cluster.set(key.getBytes(), serialization.serialize(value));
		}
	}

	protected void doGetSet(String key, Object value, int timeout) throws IOException {
		if (value instanceof String) {
			cluster.getSet(key.getBytes(), ((String) value).getBytes());
		} else {
			cluster.getSet(key.getBytes(), serialization.serialize(value));
		}
	}

	protected void doExpire(String key, int timeout) {
		if (timeout > 0) {
			cluster.expire(key.getBytes(), timeout);
		}
	}

	@Override
	protected boolean doAdd(String key, Object value, int timeout) throws CacheException {
		try {
			doSet(key, value, timeout);
		} catch (IOException e) {
			throw new CacheException(e);
		}
		doExpire(key, timeout);
		return true;
	}

	@Override
	protected boolean doDelete(String key) {
		cluster.del(key.getBytes());
		return true;
	}

	@Override
	protected boolean doExists(String key) {
		return cluster.exists(key.getBytes());
	}

	@Override
	protected Object doGet(String key) {
		Object obj = cluster.get(key.getBytes());
		return obj;
	}

	@Override
	protected <T> T doGet(String key, Class<T> clazz) throws CacheException {
		Object obj = cluster.get(key.getBytes());
		try {
			return toObject(obj, clazz);
		} catch (ClassNotFoundException | IOException e) {
			throw new CacheException(e);
		}
	}

}
