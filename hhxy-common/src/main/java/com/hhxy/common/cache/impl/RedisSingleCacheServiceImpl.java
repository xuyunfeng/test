/**
 * Project Name:BasicData
 * File Name:RedisCacheService.java
 * Package Name:com.aostarit.framework.cache.support
 * Date:2019年2月18日下午4:27:20
 * Copyright (c) 2019, yangzhi@sgitg.sgcc.com.cn All Rights Reserved.
 *
 */

package com.hhxy.common.cache.impl;

import com.hhxy.common.cache.CacheConstants;
import com.hhxy.common.exception.CacheException;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.io.IOException;

/**
 * ClassName:RedisSingleCacheServiceImpl <br/>
 * Function: Redis缓存. <br/>
 * Reason: 基于Redis实现的缓存存储. <br/>
 *
 * @version
 * @see
 */
public class RedisSingleCacheServiceImpl extends AbstractCacheService {

	private JedisPool pool;

	public RedisSingleCacheServiceImpl(HostAndPort hostAndPort, String authKey) {
		this(new JedisPoolConfig(), hostAndPort.getHost(), hostAndPort.getPort(), authKey);
	}

	public RedisSingleCacheServiceImpl(String host, int port, String authKey) {
		this(new JedisPoolConfig(), host, port, authKey);
	}

	public RedisSingleCacheServiceImpl(JedisPoolConfig config, String host, int port, String authKey) {
		this(config, host, port, authKey, CacheConstants.DEFAULT_REDIS_TIMEOUT);
	}

	public RedisSingleCacheServiceImpl(JedisPoolConfig config, String host, int port, String authKey, int timeout) {
		this(config, host, port, authKey, timeout, CacheConstants.DEFAULT_REDIS_DB);
	}

	public RedisSingleCacheServiceImpl(JedisPoolConfig config, String host, int port, String authKey, int timeout, int database) {
		pool = new JedisPool(config, host, port, timeout, authKey, database);
	}
	
	/**
	 * 序列化ID
	 */
	private static final long serialVersionUID = -8565107256587713998L;

	/**
	 * getJedis:获取Jedis链接. <br/>
	 * 从连接池中获取Jedis链接.<br/>
	 * 
	 * @return
	 */
	protected Jedis getJedis() {
		Jedis jedis = pool.getResource();
		return jedis;
	}

	@Override
	protected boolean doUpdate(String key, Object value, int timeout) throws CacheException {
		Jedis jedis = null;
		boolean result = false;
		try {
			if (exists(jedis, key)) {
				doGetSet(jedis, key, value, timeout);
			} else {
				doSet(jedis, key, value, timeout);
			}
			doExpire(jedis, key, timeout);
			result = true;
		} catch (IOException e) {
			throw new CacheException(e);
		} finally {
			close(jedis);
		}
		return result;
	}

	protected boolean exists(Jedis jedis, String key) {
		return jedis.exists(key.getBytes());
	}

	protected void doSet(Jedis jedis, String key, Object value, int timeout) throws IOException {
		if (value instanceof String) {
			jedis.set(key.getBytes(), ((String) value).getBytes());
		} else {
			jedis.set(key.getBytes(), serialization.serialize(value));
		}
	}

	protected void doGetSet(Jedis jedis, String key, Object value, int timeout) throws IOException {
		if (value instanceof String) {
			jedis.getSet(key.getBytes(), ((String) value).getBytes());
		} else {
			jedis.getSet(key.getBytes(), serialization.serialize(value));
		}
	}

	protected void doExpire(Jedis jedis, String key, int timeout) {
		if (timeout > 0) {
			jedis.expire(key.getBytes(), timeout);
		}
	}

	protected void close(Jedis jedis) {
		if (jedis != null) {
			jedis.close();
		}
	}

	@Override
	protected boolean doAdd(String key, Object value, int timeout) throws CacheException {
		Jedis jedis = null;
		boolean result = false;
		try {
			jedis = getJedis();
			doSet(jedis, key, value, timeout);
			doExpire(jedis, key, timeout);
		} catch (IOException e) {
			throw new CacheException(e);
		} finally {
			close(jedis);
		}
		return result;
	}

	@Override
	protected boolean doDelete(String key) {
		Jedis jedis = null;
		boolean result = false;
		try {
			jedis = getJedis();
			jedis.del(key.getBytes());
			result = true;
		} finally {
			close(jedis);
		}
		return result;
	}

	@Override
	protected boolean doExists(String key) {
		Jedis jedis = null;
		boolean result = false;
		try {
			jedis = getJedis();
			exists(jedis, key);
			result = true;
		} finally {
			close(jedis);
		}
		return result;
	}

	@Override
	protected Object doGet(String key) {
		Jedis jedis = null;
		Object result = null;
		try {
			jedis = getJedis();
			result = jedis.get(key.getBytes());
		} finally {
			close(jedis);
		}
		return result;
	}

	@Override
	protected <T> T doGet(String key, Class<T> clazz) throws CacheException {
		Object obj = doGet(key);
		try {
			return toObject(obj, clazz);
		} catch (ClassNotFoundException | IOException e) {
			throw new CacheException(e);
		}
	}

}
