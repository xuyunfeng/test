package com.hhxy.common.cache.impl;

import com.hhxy.common.cache.CacheService;
import com.hhxy.common.exception.CacheException;

import java.util.concurrent.ConcurrentHashMap;


/**
 * 基于本地线程内存的缓存实现，默认采用 {@link ConcurrentHashMap}作为底层的缓存实现。
 * 
 * @see ThreadLocal
 * @see ConcurrentHashMap
 * 
 */
public final class ThreadLocalCacheServiceImpl extends AbstractCacheService {

    /**
     * 
     */
    private static final long serialVersionUID = -5424866395372093386L;

    private ThreadLocal<ConcurrentHashMap<String, Object>> threadLocal;

    private static CacheService instance;

    public static CacheService getIntance() {
        if (instance == null) {
            synchronized (ThreadLocalCacheServiceImpl.class) {
                if (instance == null) {
                    instance = new ThreadLocalCacheServiceImpl();
                }
            }
        }
        return instance;
    }

    private ThreadLocalCacheServiceImpl() {
        threadLocal = new ThreadLocal<ConcurrentHashMap<String, Object>>();
    }

    public ThreadLocal<ConcurrentHashMap<String, Object>> getThreadlocal() {
        if (threadLocal.get() == null) {
            threadLocal.set(new ConcurrentHashMap<String, Object>());
        }
        return threadLocal;
    }

    @Override
    protected boolean doUpdate(String key, Object value, int timeout) throws CacheException {
        throw new CacheException("Thread local do not support update!");
    }

    @Override
    protected boolean doAdd(String key, Object value, int timeout) {
        getThreadlocal().get().put(key, value);
        return true;
    }

    @Override
    protected boolean doDelete(String key) {
        getThreadlocal().get().remove(key);
        return true;
    }

    @Override
    public boolean delete(String key) {
        getThreadlocal().set(null);
        return true;
    }

    @Override
    protected boolean doExists(String key) {
        return getThreadlocal().get().containsKey(key);
    }

    @Override
    protected Object doGet(String key) {
        ConcurrentHashMap<String, Object> map = getThreadlocal().get();
        return map.get(key);
    }

    @Override
    protected <T> T doGet(String key, Class<T> clazz) throws CacheException {
        return toObject(getThreadlocal().get().get(key), clazz);
    }
    
    @SuppressWarnings("unchecked")
	@Override
    public <T> T toObject(Object value, Class<T> clazz) throws CacheException {
    	if (value == null) {
			return null;
		}
		if (value.getClass().isAssignableFrom(clazz)) {
			return (T) value;
		}
		throw new CacheException("Serialize object class type is <" + value.getClass().getName() + ">, but want was <" + clazz.getClass().getName() + ">, can't serializable!");
    }
}
