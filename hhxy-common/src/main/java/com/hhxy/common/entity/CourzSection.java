package com.hhxy.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author hhxy
 * @since 2021-05-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("courz_section")
public class CourzSection extends DataBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键自增
     */
    @TableId(value = "section_id", type = IdType.AUTO)
    private Long sectionId;

    /**
     * 课程id
     */
    private Long courseId;

    /**
     * 段落名称
     */
    private String sectionName;

    /**
     * 排序号
     */
    private Integer seqNum;

    /**
     * 路由路径
     */
    private String path;

    /**
     * 账号状态0正常1停用
     */
    private Integer status;

    /**
     * 删除标志（0存在2删除）
     */
    private Integer delFlag;

    /**
     * 备注
     */
    private String remark;

    /**
     * 组件路径
     */
    private String component;

    /**
     * 是否外连接（0是1否）
     */
    private Integer isFrame;

    /**
     * 大纲类型
     */
    private String type;

    /**
     * 菜单状态（0显示1隐藏）
     */
    private Integer visible;


}
