package com.hhxy.common.entity;

import lombok.Data;


/**
 * @since 2021/5/14 22:46
 */
@Data
public class DataBean {

    /**
     * 创建人
     */
    private Long createBy;

    /**
     * 创建时间
     */
    private Long createTime;

    /**
     * 更新人
     */
    private Long updateBy;

    /**
     * 更新时间
     */
    private Long updateTime;
}
