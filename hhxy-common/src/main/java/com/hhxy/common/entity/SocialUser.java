package com.hhxy.common.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import com.hhxy.common.resp.RespBabyInfo;
import com.hhxy.common.util.DateUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author hhxy
 * @since 2021-05-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("social_user")
@ApiModel("用户")
public class SocialUser extends User implements Serializable {

    private static final long serialVersionUID = 1L;

    private String babyName;

    private String babySex;

    private String babyBirthday;

    private Integer babyAge;

    private String babyGrade;

    public RespBabyInfo buildBabyInfo() {
        RespBabyInfo babyInfo = new RespBabyInfo();
        babyInfo.setBabyBirthday(getBabyBirthday());
        babyInfo.setBabyName(getBabyName());
        babyInfo.setBabySex(getBabySex());
        babyInfo.setBabyGrade(getBabyGrade());
        babyInfo.setBabyAge(getBabyAge());
        return babyInfo;
    }

    public int getAge(String birthday) {
        int age = 0;
        try {
            Date babyBirth = DateUtil.parseDate(getBabyBirthday(), DateUtil.DATE_PATTERN_LINE);
            Calendar now = Calendar.getInstance();
            now.setTime(new Date());// 当前时间

            Calendar birth = Calendar.getInstance();
            birth.setTime(babyBirth);

            if (birth.after(now)) {//如果传入的时间，在当前时间的后面，返回0岁
                age = 0;
            } else {
                age = now.get(Calendar.YEAR) - birth.get(Calendar.YEAR);
                if (now.get(Calendar.DAY_OF_YEAR) < birth.get(Calendar.DAY_OF_YEAR)) {
                    age -= 1;
                }
            }
            return age;
        } catch (Exception e) {
            return 0;
        }
    }
}
