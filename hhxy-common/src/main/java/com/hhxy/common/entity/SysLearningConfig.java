package com.hhxy.common.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import com.hhxy.common.resp.RespSysConfig;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author hhxy
 * @since 2021-05-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sys_learning_config")
public class SysLearningConfig extends DataBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "config_learning_id", type = IdType.AUTO)
    private Long configLearningId;

    /**
     * 教学设置名称
     */
    private String configLearningName;

    /**
     * 顺序
     */
    private Integer seqNum;

    /**
     * 大纲类型
     */
    private Integer type;

    /**
     * 配置值，与界面固定配置
     */
    private Integer configValue;

    /**
     * 状态0正常1停用
     */
    private Integer status;

    /**
     * 删除标志0存在2删除
     */
    private Integer delFlag;

    /**
     * 描述
     */
    private String remark;

    public RespSysConfig buildResp() {
        RespSysConfig config = new RespSysConfig();
        config.setConfigLearningName(getConfigLearningName());
        config.setConfigValue(String.valueOf(getConfigValue()));
        return config;
    }

}
