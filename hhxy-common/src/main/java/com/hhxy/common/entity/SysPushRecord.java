package com.hhxy.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author hhxy
 * @since 2021-05-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sys_push_record")
public class SysPushRecord extends DataBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "push_id", type = IdType.AUTO)
    private Long pushId;

    /**
     * 推送内容
     */
    private String pushContent;

    /**
     * 推送类型1app2短信
     */
    private Integer pushType;

    /**
     * 推送用户
     */
    private Long pushUserId;

    /**
     * 状态0正常1停用
     */
    private Integer status;

    /**
     * 删除标志0存在2删除
     */
    private Integer delFlag;

    /**
     * 描述
     */
    private String remark;


}
