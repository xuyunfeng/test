package com.hhxy.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author hhxy
 * @since 2021-05-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sys_role")
public class SysRole extends DataBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键自增
     */
    @TableId(value = "role_id", type = IdType.AUTO)
    private Long roleId;

    /**
     * 角色权限字符
     */
    private String roleKey;

    /**
     * 角色名称
     */
    private String roleName;

    /**
     * 排序号
     */
    private Integer roleSort;

    /**
     * 数据范围（1全部权限数据、2自定义权限数据、3本部门权限数据、4本部门以及下级数据权限）
     */
    private Integer dataScope;

    /**
     * 状态0正常1停用
     */
    private Integer status;

    /**
     * 删除标志（0存在2删除）
     */
    private Integer delFlag;

    /**
     * 备注
     */
    private String remark;

    /**
     * 角色类型 1普通，2管理
     */
    private Integer roleType;
}
