package com.hhxy.common.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author hhxy
 * @since 2021-05-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sys_user")
public class SysUser extends User implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 组织编号
     */
    private Long deptId;

    /**
     * 项目名称
     */
    private String projectName;

    /**
     * 系统版本
     */
    private String version;

    /**
     * 是否最新版本0最新1不是
     */
    private Integer isLatest;

    /**
     * 项目类型
     */
    private Integer projectType;

    /**
     * model类型
     */
    private Integer modelType;

    /**
     * biz类型
     */
    private Integer bizType;

    /**
     * 部门
     */
    private String bu;

    /**
     * 用户性别（0男1女2未知）
     */
    private Integer sex;

    /**
     * md5密码
     */
    private String password;

    /**
     * 删除标志（0存在2删除）
     */
    private Integer delFlag;

    /**
     * 最后登陆ip
     */
    private String loginIp;

    /**
     * 最后登陆时间
     */
    private Long loginDate;

    /**
     * 备注
     */
    private String remark;


    /**
     * 创建人
     */
    private Long createBy;

    /**
     * 更新人
     */
    private Long updateBy;

    /**
     * 更新时间
     */
    private Long updateTime;
}
