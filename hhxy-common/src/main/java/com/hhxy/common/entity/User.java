package com.hhxy.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * @since 2021/5/15 22:02
 */
@Data
public class User {
    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(name = "id", value = "主键", required = true)
    private Long id;

    /**
     * 用户登录名
     */
    @ApiModelProperty(name = "userName", value = "用户登录名", required = true)
    private String userName;

    /**
     * 昵称
     */
    @ApiModelProperty(name = "nickName", value = "用户昵称", required = true)
    private String nickName;

    /**
     * 手机号
     */
    @ApiModelProperty(name = "phoneNumber", value = "用户手机号", required = true)
    private String phoneNumber;

    /**
     * 邮箱
     */
    @ApiModelProperty(name = "email", value = "用户邮箱地址", required = true)
    private String email;

    /**
     * 用户状态1正常0禁用
     */
    @ApiModelProperty(name = "status", value = "用户状态", required = true)
    private Integer status;

    /**
     * 创建时间
     */
    @ApiModelProperty(name = "createTime", value = "创建时间", required = true)
    private Long createTime;

    /**
     * 上一次访问时间
     */
    @ApiModelProperty(name = "lastVisitTime", value = "最后访问时间", required = true)
    private Long lastVisitTime;

    /**
     * 头像地址
     */
    @ApiModelProperty(name = "avatar", value = "头像地址", required = true)
    private String avatar;

    @ApiModelProperty(name = "avatar", value = "头像类型，1自定义、0否", required = true)
    private String avatarType;

    /**
     * 克隆自身，原因为真实对象可能为子类，而缓存需要当前类，否则出现序列化问题
     * @return
     */
    public User cloneSelf() {
        User user = new User();
        user.setAvatar(getAvatar());
        user.setId(getId());
        user.setCreateTime(getCreateTime());
        user.setEmail(getEmail());
        user.setLastVisitTime(getLastVisitTime());
        user.setNickName(getNickName());
        user.setPhoneNumber(getPhoneNumber());
        user.setStatus(getStatus());
        user.setUserName(getUserName());
        user.setAvatar(getAvatar());
        user.setAvatarType(getAvatarType());
        return user;
    }
}
