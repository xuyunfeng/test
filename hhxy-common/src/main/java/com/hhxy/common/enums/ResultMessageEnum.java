package com.hhxy.common.enums;

import lombok.Getter;

/**
 * 消息返回信息
 *
 * @author legao
 */
@Getter
public enum ResultMessageEnum {
    /**
     * 未知异常
     */
    UNKNOW(-1, "未知异常！"),
    SUCCESS(200, "服务请求成功！"),

    ILLEGAL_REQ(110, "非法请求，请联系管理员！"),
    PARAM_ERR(403, "参数校验失败，请重新输入！"),
    USER_IS_NULL(111, "非法请求，无法获取到用户信息！"),
    USER_PD_ERR(120, "用户密码不正确，请重新输入或者联系管理员！"),

    USER_NOT_EXIST(500, "请求失败，用户信息不存在，请联系管理员！"),

    LOGIN_HANDLER_NULL(501, "登录处理类为空，请联系管理员！"),
    LOGIN_UNABLE_HANDLER(502, "登录方式无法处理，请联系管理员！"),

    // 用户信息相关
    ACCESS_TOKEN_EXP(10001, "授权码已失效，请重新获取！"),
    REFRESH_TOKEN_EXP(10002, "授权码已失效，请重新登陆！"),
    ACCESS_TOKEN_NOT_EXIST(10003, "授权码不存在，请重新登陆！"),

    // 短信相关
    SMS_ERR(40001, "短信异常，请稍后再试！"),

    // 手机登陆相关
    TEL_EMPTY(20001, "手机号不允许为空！"),
    TEL_NOT_REGISTER(20002, "手机号未注册，请前往注册！"),
    TEL_ERR(20003, "手机号格式不正确！"),


    // 微信相关错误
    WECHAT_ERR(30001, "微信不可用，请稍后再试！"),

    // 课程信息
    COURSE_EXIST(60001, "课程已存在！"),

    PAY_NOT_FOUND(7001, "未找到用户相关的订单信息！"),


    //组织相关
    DEPT_EXISTS(90101, "组织已存在！"),

    //用户相关
    USER_EXISTS(90301, "用户已注册！"),

    //角色相关
    ROLE_EXISTS(90501, "角色已存在！"),



    //菜单相关
    MENU_EXISTS(90701, "菜单已创建！"),

    //菜单相关
    SYS_CONFIG_NOT_EXISTS(90801, "系统配置信息未找到！");

    private int code;
    private String message;

    private ResultMessageEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }


}
