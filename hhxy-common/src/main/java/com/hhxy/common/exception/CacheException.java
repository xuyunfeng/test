package com.hhxy.common.exception;

import com.hhxy.common.enums.ResultMessageEnum;

/**
 * @since 2021/3/17 22:15
 */
public class CacheException extends HhxyException {

    private static final int cacheCode = 40000;

    public CacheException() {
        super(cacheCode);
    }

    public CacheException(String message) {
        this(message, cacheCode);
    }

    public CacheException(ResultMessageEnum messageEnum) {
        this(messageEnum.getMessage(), messageEnum.getCode());
    }

    public CacheException(String message, int code) {
        super(message, code);
    }

    public CacheException(String message, Throwable cause) {
        this(message, cause, cacheCode);
    }

    public CacheException(String message, Throwable cause, int code) {
        super(message, cause, code);
    }

    public CacheException(Throwable cause) {
        super(cause, cacheCode);
    }

    public CacheException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace, cacheCode);
    }
}
