package com.hhxy.common.exception;

import lombok.Getter;
import com.hhxy.common.enums.ResultMessageEnum;

/**
 * 业务异常
 *
 * @author legao
 */
@Getter
public class HhxyException extends RuntimeException {

    protected int code;

    public HhxyException(ResultMessageEnum messageEnum) {
        this(messageEnum.getMessage(), messageEnum.getCode());
    }

    public HhxyException(int code) {
        this.code = code;
    }

    public HhxyException(String message, int code) {
        super(message);
        this.code = code;
    }

    public HhxyException(String message, Throwable cause, int code) {
        super(message, cause);
        this.code = code;
    }

    public HhxyException(Throwable cause, int code) {
        super(cause);
        this.code = code;
    }

    public HhxyException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, int code) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.code = code;
    }
}
