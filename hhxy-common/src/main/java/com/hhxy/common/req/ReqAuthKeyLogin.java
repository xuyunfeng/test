package com.hhxy.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @since 2021/5/12 22:37
 */
@Getter
@Setter
@ApiModel("用户口令登陆")
public class ReqAuthKeyLogin extends ReqLogin{

    @ApiModelProperty(name = "loginName", value = "登陆名", notes = "用户登录名，注册时录入")
    private String loginName;

    @ApiModelProperty(name = "password", value = "用户口令", notes = "用户口令，注册时录入")
    private String password;
}
