package com.hhxy.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @since 2021/5/16 22:29
 */
@ApiModel("宝宝信息补录")
@Getter
@Setter
public class ReqBabyInfo {

    /**
     *
     */
    @ApiModelProperty(name = "babyName", value = "宝宝名称", required = true)
    private String babyName;

    @ApiModelProperty(name = "babySex", value = "宝宝性别，女宝宝、男宝宝", required = true)
    private String babySex;

    @ApiModelProperty(name = "babyBirthday", value = "宝宝生日，yyyy-DD-MM", required = true)
    private String babyBirthday;

    @ApiModelProperty(name = "babyGrade", value = "宝宝年级", required = true)
    private String babyGrade;

    @ApiModelProperty(name = "babyAge", value = "宝宝年龄", required = true)
    private Integer babyAge;
}
