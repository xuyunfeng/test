package com.hhxy.common.req;

import com.hhxy.common.entity.DataBean;
import com.hhxy.common.util.ThreadLocalUtil;

/**
 * @since 2021/5/14 22:52
 */
public class ReqDataBean {

    public void build(DataBean dataBean, boolean create) {
        if (create) {
            dataBean.setCreateBy(ThreadLocalUtil.getUserId());
            dataBean.setCreateTime(System.currentTimeMillis());
        }
        dataBean.setUpdateTime(System.currentTimeMillis());
        dataBean.setUpdateBy(ThreadLocalUtil.getUserId());
    }
}
