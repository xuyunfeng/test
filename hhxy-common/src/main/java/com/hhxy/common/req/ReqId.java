package com.hhxy.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @since 2021/5/14 23:03
 */
@Getter
@Setter
@ApiModel("获取数据对象")
public class ReqId {

    @ApiModelProperty(name = "id", value = "数据对象的主键,用于获取数据对象或者删除等操作", notes = "数据对象的主键,用于获取数据对象或者删除等操作", required = true)
    private Integer id;
}
