package com.hhxy.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @since 2021/5/12 22:35
 */
@Getter
@Setter
@ApiModel("登陆基类")
public class ReqLogin {

    /**
     * 登陆方式: 1用户口令 2微信 3短信
     */
    @ApiModelProperty(name = "loginType", value = "登陆方式，1用户口令、2微信、3短信", required = true, allowableValues = "1、2、3")
    private int loginType = 2;

    /**
     * 登陆传值json，用于转换成对应的处理类
     */
    @ApiModelProperty(name = "dataJson", value = "登陆传值JSON串，如微信登陆{\"code\":\"xxx\"}", required = true)
    private String dataJson;
}
