package com.hhxy.common.req;

import com.hhxy.common.entity.SysRole;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @since 2021/5/14 22:05
 */
@ApiModel("角色信息")
@Getter
@Setter
public class ReqRole extends ReqDataBean{

    @ApiModelProperty(name = "roleId", value = "主键", notes = "角色主键", required = true)
    private Long roleId;

    @ApiModelProperty(name = "roleKey", value = "角色权限字符")
    private String roleKey;

    @ApiModelProperty(name = "roleName", value = "角色名称", required = true)
    private String roleName;

    @ApiModelProperty(name = "roleSort", value = "角色序号", required = false)
    private Integer roleSort;

    @ApiModelProperty(name = "dataScope", value = "数据范围，1全部权限数据、2自定义权限数据、3本部门权限数据、4本部门以及下级数据权限", required = true)
    private Integer dataScope;

    @ApiModelProperty(name = "status", value = "角色状态", notes = "0正常 1停用", required = true)
    private Integer status;

    @ApiModelProperty(name = "remark", value = "角色描述", required = true)
    private String remark;

    @ApiModelProperty(name = "roleType", value = "角色类型， 1普通角色 2管理角色", required = true)
    private Integer roleType;

    public SysRole build() {
        SysRole role = new SysRole();
        role.setRoleId(getRoleId());
        role.setRoleKey(getRoleKey());
        role.setRoleName(getRoleName());
        role.setRoleSort(getRoleSort());
        role.setDataScope(getDataScope());
        role.setStatus(getStatus());
        role.setRemark(getRemark());
        role.setRoleType(getRoleType());
        build(role, roleId == null);
        return role;
    }
}
