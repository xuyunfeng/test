package com.hhxy.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @since 2021/5/21 21:07
 */
@Getter
@Setter
@ApiModel("系统配置")
public class ReqSystemConfig extends ReqDataBean {

    // 手机号
    @ApiModelProperty(name = "configLearningName", value = "教学设置名称，固定值learning_speed（学习速度）、service_time（服务时长）、animation_effect(动效画面)、music_ctrl（音乐控制）、music(音乐）", required = true)
    private String configLearningName;

    @ApiModelProperty(name = "configValue", value = "设置值，固定值learning_speed（1、2、3、4）、service_time（1、2、3、4）、animation_effect(1、2)、music_ctrl（0、1）、music(1、2）", required = true)
    private String configValue;
}
