package com.hhxy.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @since 2021/5/12 22:38
 */
@Getter
@Setter
@ApiModel("手机登陆")
public class ReqTelLogin extends ReqLogin {

    // 手机号
    @ApiModelProperty(name = "tel", value = "手机号", notes = "手机号，注册时录入")
    private String tel;

    // 手机验证码
    @ApiModelProperty(name = "verifyCode", value = "验证码", notes = "验证码，登陆时获取")
    private String verifyCode;
}
