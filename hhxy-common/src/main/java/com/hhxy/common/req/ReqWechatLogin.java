package com.hhxy.common.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @since 2021/5/12 22:38
 */
@Getter
@Setter
@ApiModel("微信登陆")
public class ReqWechatLogin extends ReqLogin {

    // 微信一次性授权码，用于获取accessToken
    @ApiModelProperty(name = "code", value = "微信授权码", notes = "微信一次性授权码，用于与微信交互")
    private String code;
}
