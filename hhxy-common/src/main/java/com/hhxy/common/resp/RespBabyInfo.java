package com.hhxy.common.resp;

import com.hhxy.common.req.ReqBabyInfo;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

/**
 * @since 2021/5/16 22:47
 */
@Getter
@Setter
@ApiModel("宝宝信息")
public class RespBabyInfo extends ReqBabyInfo {
}
