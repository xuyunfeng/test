package com.hhxy.common.resp;

import com.hhxy.common.entity.User;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @since 2021/5/15 21:38
 */
@Getter
@Setter
public class RespLoginUser {

    @ApiModelProperty(name = "id", value = "主键", required = true)
    private Long id;

    /**
     * 用户登录名
     */
    @ApiModelProperty(name = "userName", value = "用户登录名", required = true)
    private String userName;

    /**
     * 昵称
     */
    @ApiModelProperty(name = "nickName", value = "用户昵称", required = true)
    private String nickName;

    /**
     * 头像地址
     */
    @ApiModelProperty(name = "avatar", value = "头像地址", required = true)
    private String avatar;

    @ApiModelProperty(name = "accessToken", value = "授权码", required = true)
    private String accessToken;

    @ApiModelProperty(name = "refreshToken", value = "刷新授权码", required = true)
    private String refreshToken;

    @ApiModelProperty(name = "exp", value = "失效时间", required = true)
    private int exp;

    public RespLoginUser build(User user) {
        setAvatar(user.getAvatar());
        setId(user.getId());
        setNickName(user.getNickName());
        setUserName(user.getUserName());
        return this;
    }
}
