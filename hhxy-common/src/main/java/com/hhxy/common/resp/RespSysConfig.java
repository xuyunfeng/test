package com.hhxy.common.resp;

import com.hhxy.common.req.ReqSystemConfig;
import io.swagger.annotations.ApiModel;

/**
 * @since 2021/5/21 23:35
 */
@ApiModel("响应系统配置信息")
public class RespSysConfig extends ReqSystemConfig {
}
