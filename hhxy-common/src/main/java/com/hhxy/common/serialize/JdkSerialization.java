/**
 * Project Name:BasicData
 * File Name:JdkSerialization.java
 * Package Name:com.aostarit.framework.serialize
 * Date:2019-4-23下午2:18:56
 * Copyright (c) 2019, yangzhi@sgitg.sgcc.com.cn All Rights Reserved.
 *
 */
package com.hhxy.common.serialize;

import java.io.*;


/**
 * ClassName:JdkSerialization <br/>
 * Function: 基于JDK的序列化实现. <br/>
 * Reason: 基于JDK的序列化与反序列化实现. <br/>
 * Date: 2019-4-23 下午2:18:56 <br/>
 * 
 * 
 * @version
 * @see ByteArrayOutputStream
 * @see ObjectOutputStream
 * @see ByteArrayInputStream
 * @see ObjectInputStream
 */
public class JdkSerialization implements Serialization {

	@Override
	public byte[] serialize(Object obj) throws IOException {
		if (obj == null) {
			return null;
		}
		byte[] result = null;
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		ObjectOutputStream outputStream = null;
		try {
			outputStream = new ObjectOutputStream(byteArrayOutputStream);
			outputStream.writeObject(obj);
			result = byteArrayOutputStream.toByteArray();
		} catch (IOException e) {
			throw e;
		} finally {
			close(outputStream);
			close(byteArrayOutputStream);
		}
		return result;
	}

	protected void close(Closeable closeable) throws IOException {
		if (closeable != null) {
			closeable.close();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T deserialize(byte[] bytes, Class<T> type) throws IOException, ClassNotFoundException {
		if (bytes == null) {
			return null;
		}
		Object result = null;
		ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
		ObjectInputStream inputStream = null;
		try {
			inputStream = new ObjectInputStream(byteArrayInputStream);
			result = inputStream.readObject();
		} catch (IOException | ClassNotFoundException e) {
			throw e;
		} finally {
			close(inputStream);
			close(byteArrayInputStream);
		}
		if (type.isAssignableFrom(result.getClass())) {
			return (T) result;
		} else {
			throw new ClassNotFoundException("Serialize object class type is <" + result.getClass().getName() + ">, but want was <" + type.getName() + ">, can't serializable!");
		}
		
	}

}
