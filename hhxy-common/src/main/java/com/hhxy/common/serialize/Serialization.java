/**
 * Project Name:BasicData
 * File Name:Serialization.java
 * Package Name:com.aostarit.framework.serialize.inter
 * Date:2019-4-23下午2:18:34
 * Copyright (c) 2019, yangzhi@sgitg.sgcc.com.cn All Rights Reserved.
 *
 */

package com.hhxy.common.serialize;

import java.io.IOException;

/**
 * ClassName:Serialization <br/>
 * Function: 序列化接口. <br/>
 * Reason: 用于对对象进行序列化. <br/>
 * Date: 2019-4-23 下午2:18:34 <br/>
 * 
 *
 * @version
 */
public interface Serialization {

	/**
	 * serialize:序列化对象. <br/>
	 *
	 *
	 * @param obj
	 * @return
	 * @throws IOException 
	 */
	public byte[] serialize(Object obj) throws IOException;

	/**
	 * deserialize:反序列化成对象. <br/>
	 *
	 *
	 * @param bytes
	 * @param type 对象类型
	 * @return
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	public <T> T deserialize(byte[] bytes, Class<T> type) throws IOException, ClassNotFoundException;
}
