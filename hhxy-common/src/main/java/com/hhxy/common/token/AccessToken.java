package com.hhxy.common.token;

import com.hhxy.common.entity.User;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @since 2021/3/18 22:41
 */
public class AccessToken implements IToken, Serializable {

    private String id;

    private LocalDateTime createTime;

    private LocalDateTime lastVisit;

    private User user;

    public AccessToken(String id, User user) {
        this.id = id;
        this.createTime = LocalDateTime.now();
        this.lastVisit = LocalDateTime.now();
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    @Override
    public String getId() {
        return prefix() + "-" + id;
    }

    @Override
    public boolean expired() {
        // （最后一次访问时间+失效时间）在当前时间之前，说明已经失效，在之后说明未失效，exp单位秒
        return lastVisit.plusMinutes(exp() * 1000).isBefore(LocalDateTime.now());
    }

    @Override
    public void renewal() {
        this.lastVisit = LocalDateTime.now();
    }
}
