package com.hhxy.common.token;

import com.hhxy.common.entity.User;

/**
 * @since 2021/3/18 22:35
 */
public interface IToken {

    /**
     * 获取tokenid
     * @return
     */
    String getId();

    /**
     * 获取用户
     * @return
     */
    User getUser();

    /**
     * token前缀，默认AT
     * @return
     */
    default String prefix() {
        return "AT";
    }

    default String getToken() {
        return getId().split("-")[1];
    }

    /**
     * 是否已失效
     * @return
     */
    boolean expired();

    /**
     * 续期
     */
    void renewal();

    /**
     * 默认失效时间，单位秒
     * @return
     */
    default int exp() {
        return  60 * 24 * 30;
    }
}
