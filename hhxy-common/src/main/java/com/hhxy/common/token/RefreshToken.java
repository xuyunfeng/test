package com.hhxy.common.token;

import com.hhxy.common.entity.User;

import java.io.Serializable;

/**
 * @since 2021/3/18 23:03
 */
public class RefreshToken extends AccessToken implements Serializable {

    public RefreshToken(String id, User user) {
        super(id, user);
    }

    public String prefix() {
        return "RT";
    }

    /**
     * 一个月失效一次
     * @return
     */
    public int exp() {
        return 60 * 24 * 30 * 2;
    }
}
