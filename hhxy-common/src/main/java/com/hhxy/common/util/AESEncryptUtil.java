/**
 * Project Name:BasicData
 * File Name:AESEncryptUtil.java
 * Package Name:com.aostarit.framework.encrypt.util
 * Date:2019年1月13日上午11:02:42
 * Copyright (c) 2019, yangzhi@sgitg.sgcc.com.cn All Rights Reserved.
 *
*/

package com.hhxy.common.util;

import org.apache.commons.lang3.StringUtils;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;

/**
 * @version 1.0
 */
public class AESEncryptUtil {

    public static final String DEFAULT_KEY_SEED = "d2e65192cad545dc9bac531e7ca5515e";

    /**
     * 
     * AESEncryptUtil:私有构造方法，避免实例化
     *
     */
    private AESEncryptUtil() {
    }

    public static String encrypt(String message) throws NoSuchPaddingException, BadPaddingException, NoSuchAlgorithmException, IllegalBlockSizeException, UnsupportedEncodingException, InvalidKeyException {
        return encrypt(message, DEFAULT_KEY_SEED);
    }

    /**
     * encrypt:加密方法. <br/>
     * 对消息进行对称加密<br/>
     * 
     * @param message 消息
     * @param keySeed 加密因子
     * @return 加密HexString<br/>
     *         如果消息体或加密因子为空，返回NULL
     * @throws Exception
     */
    public static String encrypt(String message, String keySeed) throws NoSuchAlgorithmException, UnsupportedEncodingException, NoSuchPaddingException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        if (StringUtils.isBlank(message) || StringUtils.isBlank(keySeed)) {
            return null;
        }
        KeyGenerator kgen = KeyGenerator.getInstance("AES");
        // 解决linux环境下密码解密问题
        SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");
        secureRandom.setSeed(keySeed.getBytes("UTF-8"));
        kgen.init(128, secureRandom);
        SecretKey secretKey = kgen.generateKey();
        byte[] enCodeFormat = secretKey.getEncoded();
        SecretKeySpec key = new SecretKeySpec(enCodeFormat, "AES");
        Cipher cipher = Cipher.getInstance("AES");// 创建密码器
        byte[] byteContent = message.getBytes("utf-8");
        cipher.init(Cipher.ENCRYPT_MODE, key);// 初始化
        byte[] result = cipher.doFinal(byteContent);
        return ByteUtils.bytesToHex(result); // 加密
    }

    /**
     * 对于js加密<br/>
     * 与前端js加密保持一直
     * 
     * @param message 消息
     * @param keySeed 解密密钥
     * @return 加密HexString<br/>
     *         如果消息体或加密因子为空，返回NULL
     * @throws Exception
     */
    public static String encryptForJS(String message, String keySeed) throws UnsupportedEncodingException, NoSuchPaddingException, NoSuchAlgorithmException, BadPaddingException, IllegalBlockSizeException, InvalidKeyException {
        if (StringUtils.isBlank(message) || StringUtils.isBlank(keySeed)) {
            return null;
        }
        SecretKeySpec key = getKeySpecFromBytes(keySeed.toUpperCase());
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, key);
        byte[] byteEnc = cipher.doFinal(message.getBytes("UTF-8"));
        return ByteUtils.bytesToHex(byteEnc);
    }

    /**
     * 从十六进制字符串生成Key
     * 
     * @param strBytes str字节
     * @return
     * @throws NoSuchAlgorithmException
     */
    private static SecretKeySpec getKeySpecFromBytes(String strBytes) throws UnsupportedEncodingException {
        return new SecretKeySpec(hex2Byte(strBytes.getBytes("UTF-8")), "AES");
    }

    /**
     * 十六进制字符串到字节转换
     * 
     * @param b byte类型的数组
     * @return
     */
    private static byte[] hex2Byte(byte[] b) throws UnsupportedEncodingException {
        if ((b.length % 2) != 0) {
            throw new IllegalArgumentException("长度不是偶数!");
        }
        byte[] b2 = new byte[b.length / 2];

        for (int n = 0; n < b.length; n += 2) {
            String item = null;
            // 判断n值是否在b字节长度范围之内，否则，造成堆内存溢出
            if (n + 2 <= b.length) {
                item = new String(b, n, 2, "UTF-8");
                b2[n / 2] = (byte) Integer.parseInt(item, 16);
            }
            item = null;
        }
        // 在垃圾回收延迟的情况下，进行记忆清楚，避免信息被窃取
        byte temp = 0;
        // 将字节数组赋值为0，删除原有数据
        Arrays.fill(b, temp);
        return b2;
    }

    /**
     * decryptForJS:基于JS加密串的解密. <br/>
     * 与前端JS加解密保持一直
     *
     *
     * @param encryptCode 加密编码
     * @param keySeed     加密因子
     * @return 解密结果<br/>
     *         如果加密编码或加密因子为空，返回NULL
     * @throws Exception
     */
    public static String decryptForJS(String encryptCode, String keySeed) throws UnsupportedEncodingException, InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException, BadPaddingException, IllegalBlockSizeException {
        if (StringUtils.isBlank(encryptCode) || StringUtils.isBlank(keySeed)) {
            return null;
        }
        SecretKeySpec key = getKeySpecFromBytes(keySeed.toUpperCase());
        Cipher cipher = Cipher.getInstance("AES/ECB/NoPadding");
        cipher.init(Cipher.DECRYPT_MODE, key);
        String result = new String(cipher.doFinal(hex2Byte(encryptCode.getBytes("UTF-8"))), "UTF-8");
        return result.trim();
    }

    /**
     * decrypt:解密方法.
     *
     *
     * @param encryptCode 加密编码
     * @param keySeed     加密因子
     * @return 解密结果<br/>
     *         如果加密编码或加密因子为空，返回NULL
     * @throws Exception
     */
    public static String decrypt(String encryptCode, String keySeed) throws NoSuchAlgorithmException, UnsupportedEncodingException, NoSuchPaddingException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        if (StringUtils.isBlank(encryptCode) || StringUtils.isBlank(keySeed)) {
            return null;
        }
        KeyGenerator kgen = KeyGenerator.getInstance("AES");
        // 解决linux环境下密码解密问题
        SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");
        secureRandom.setSeed(keySeed.getBytes("UTF-8"));
        kgen.init(128, secureRandom);
        SecretKey secretKey = kgen.generateKey();
        byte[] enCodeFormat = secretKey.getEncoded();
        SecretKeySpec key = new SecretKeySpec(enCodeFormat, "AES");
        Cipher cipher = Cipher.getInstance("AES");// 创建密码器
        cipher.init(Cipher.DECRYPT_MODE, key);// 初始化
        String result = new String(cipher.doFinal(hex2Byte(encryptCode.getBytes("UTF-8"))), "UTF-8");
        return result.trim();
    }
}
