package com.hhxy.common.util;

import java.io.UnsupportedEncodingException;

/**
 * @since 2021/3/18 22:27
 */
public class ByteUtils {

    private static final String HEX_STRING_MAPPING = "0123456789ABCDEF";
    public static final String CHARSET_NAME = "UTF-8";

    public static String bytesToHex(byte[] paramArrayOfByte) {
        if (paramArrayOfByte == null) {
            return null;
        }
        if (paramArrayOfByte.length <= 0) {
            return "";
        }
        StringBuilder localStringBuilder = new StringBuilder("");
        for (int k : paramArrayOfByte) {
            int m = k & 0xFF;
            String str = Integer.toHexString(m);
            if (str.length() < 2) {
                localStringBuilder.append(0);
            }
            localStringBuilder.append(str);
        }
        return localStringBuilder.toString().toUpperCase();
    }

    public static byte[] hexToBytes(String paramString) {
        if (paramString == null) {
            return null;
        }
        if (paramString.length() <= 0) {
            return new byte[0];
        }
        paramString = paramString.toUpperCase();
        int i = paramString.length() / 2;
        char[] arrayOfChar = paramString.toCharArray();
        byte[] arrayOfByte = new byte[i];
        for (int j = 0; j < i; j++) {
            int k = j * 2;
            arrayOfByte[j] = (byte) (charToByte(arrayOfChar[k]) << 4 | charToByte(arrayOfChar[(k + 1)]));
        }
        return arrayOfByte;
    }

    public static String bytesToString(byte[] paramArrayOfByte) {
        try {
            return new String(paramArrayOfByte, "UTF-8");
        } catch (UnsupportedEncodingException localUnsupportedEncodingException) {
        }
        return null;
    }

    public static byte[] stringToBytes(String paramString) {
        try {
            return paramString.getBytes("UTF-8");
        } catch (UnsupportedEncodingException localUnsupportedEncodingException) {
        }
        return null;
    }

    public static String hexToString(String paramString) {
        return bytesToString(hexToBytes(paramString));
    }

    public static String stringToHex(String paramString) {
        return bytesToHex(stringToBytes(paramString));
    }

    private static byte charToByte(char paramChar) {
        return (byte) "0123456789ABCDEF".indexOf(paramChar);
    }
}
