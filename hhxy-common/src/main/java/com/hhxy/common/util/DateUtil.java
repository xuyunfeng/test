package com.hhxy.common.util;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Time;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.regex.Pattern;

/**
 * 日期操作工具类
 * 
 */
public final class DateUtil {

    private static final Logger logger = LoggerFactory.getLogger(DateUtil.class);

    /* 私有化构造函数 */
    private DateUtil() {
    }

    /* 日期格式转化类 */
    private static ThreadLocal<SimpleDateFormat> dateFormat = new ThreadLocal<SimpleDateFormat>() {
        @Override
        protected SimpleDateFormat initialValue() {
            return new SimpleDateFormat();
        }
    };

    /**
     * 日期格式:yyyy-mm-dd<br>
     * 例如:2005-11-02
     */
    public static final String DATE_PATTERN_LINE = "yyyy-MM-dd";

    /**
     * 日期格式:yyyy/mm/dd<br>
     * 例如:2005/11/02
     */
    public static final String DATE_PATTERN_BIAS = "yyyy/MM/dd";

    /**
     * 日期时间格式(24小时制):yyyy-mm-dd HH:mm:ss<br>
     * 例如:2005-11-02 23:01:01
     */
    public static final String DATETIME24_PATTERN_LINE = "yyyy-MM-dd HH:mm:ss";

    /**
     * 日期格式:yyyyMMdd<br>
     * 例如:2005/11/02
     */
    public static final String DATE_PATTERN_BIASSS = "yyyyMMdd";

    /**
     * 日期时间格式(12小时制):yyyy-mm-dd hh:mm:ss<br>
     * 例如:2005-11-02 11:01:01
     */
    public static final String DATETIME12_PATTERN_LINE = "yyyy-MM-dd hh:mm:ss";

    /**
     * 日期时间格式(24小时制):yyyy/mm/dd HH:mm:ss<br>
     * 例如:2005/11/02 23:01:01
     */
    public static final String DATETIME24_PATTERN_BIAS = "yyyy/MM/dd HH:mm:ss";

    /**
     * 日期时间格式(12小时制):yyyy/mm/dd hh:mm:ss<br>
     * 例如:2005/11/02 11:01:01
     */
    public static final String DATETIME12_PATTERN_BIAS = "yyyy/MM/dd hh:mm:ss";
    
    


    /**
     * 
     * getCurrentTime:(获取当前时间). <br/>
     *
     *
     * @return
     */
    public static Date getCurrentTime() {
        return new Date(getCurrentTimeAsLong());
    }

    /**
     * 
     * getCurrentTimeAsStr:(获取24小时制的时间). <br/>
     *
     *
     * @return
     */
    public static String getCurrentTimeAsStr() {
        return format(getCurrentTime(), DATETIME24_PATTERN_LINE);
    }

    /**
     * 
     * getCurrentTimeAsStr:(获取自定义格式的时间). <br/>
     *
     *
     * @param pattern 自定义格式
     * @return String
     */
    public static String getCurrentTimeAsStr(String pattern) {
        return format(getCurrentTime(), pattern);
    }

    /**
     * 
     * getCurrentTimeAsLong:(获取当前时间戳). <br/>
     *
     *
     * @return
     */
    public static long getCurrentTimeAsLong() {
        return System.currentTimeMillis();
    }

    /**
     * 
     * getCurrentTimeAsLong:(获取当前时间戳对象). <br/>
     *
     *
     * @return
     */
    public static Timestamp getCurrentTimestamp() {
        return new Timestamp(getCurrentTimeAsLong());
    }

    /**
     * 
     * getCurrentTimeAsLong:(获取当前日期). <br/>
     *
     *
     * @return
     */
    public static Calendar getCurrentCalendar() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(getCurrentTimeAsLong());
        return calendar;
    }

    /**
     * 
     * getCurrentTimeAsLong:(获取当前日期). <br/>
     *
     *
     * @return
     */
    public static Calendar getCalendar() {
        Calendar calendar = Calendar.getInstance();
        return calendar;
    }

    /**
     * 
     * getCurrentTimeAsLong:(获取当前日期). <br/>
     *
     *
     * @return
     */
    public static Calendar getCalendar(Date datetime) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(datetime);
        return calendar;
    }

    /**
     * 
     * getCurrentTimeAsLong:(获取当前日期). <br/>
     *
     *
     * @return
     */
    public static Calendar getCalendar(long datetime) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(datetime);
        return calendar;
    }

    /**
     * 
     * getCurrentTimeAsLong:(获取当前日期). <br/>
     *
     *
     * @return
     * @throws Exception
     */
    public static Calendar getCalendar(String datetime) throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(parseDate(datetime));
        return calendar;
    }

    /**
     * 
     * getCurrentTimeAsLong:(获取当前日期). <br/>
     *
     *
     * @return
     */
    public static Calendar getCalendar(Timestamp datetime) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(datetime);
        return calendar;
    }

    /**
     * 
     * getCurrentTimeAsLong:(获取：年). <br/>
     *
     *
     * @return
     * @throws Exception
     */
    public int getYear(String datetime) throws Exception {
        String s = DateUtil.format(parseDate(datetime), DATE_PATTERN_LINE);
        // A "NullPointerException" could be thrown; "s" is nullable here.
        if (StringUtils.isNotBlank(s)) {
            try {
                return Integer.parseInt(s.substring(0, 4));
            } catch (Exception e) {
                if (logger.isErrorEnabled()) {
                    logger.error(e.getMessage(), e);
                }
                throw new Exception(e.getMessage(), e);
            }
        }
        return 0;
    }

    /**
     * 
     * getMonth:(获取：月). <br/>
     *
     *
     * @param datetime
     * @return
     * @throws Exception
     */
    public int getMonth(String datetime) throws Exception {
        String s = format(parseDate(datetime), DATE_PATTERN_LINE);
        // A "NullPointerException" could be thrown; "s" is nullable here.
        if (StringUtils.isNotBlank(s)) {
            try {
                return Integer.parseInt(s.substring(5, 7));
            } catch (Exception e) {
                if (logger.isErrorEnabled()) {
                    logger.error(e.getMessage(), e);
                }
                throw new Exception(e.getMessage(), e);
            }
        }
        return 0;
    }

    /**
     * 
     * getDay:(获取：天). <br/>
     *
     *
     * @param datetime
     * @return
     * @throws Exception
     */
    public static int getDay(String datetime) throws Exception {
        String s = DateUtil.format(parseDate(datetime), DATE_PATTERN_LINE);
        if (StringUtils.isNotBlank(s)) {
            try {
                return Integer.parseInt(s.substring(8, 10));
            } catch (Exception e) {
                if (logger.isErrorEnabled()) {
                    logger.error(e.getMessage(), e);
                }
                throw new Exception(e.getMessage(), e);
            }
        }
        return 0;
    }

    /**
     * 
     * getHour:(获取：小时). <br/>
     *
     *
     * @param datetime
     * @return
     * @throws Exception
     */
    public static int getHour(String datetime) throws Exception {
        String s = format(parseDate(datetime), DATETIME24_PATTERN_LINE);
        // A "NullPointerException" could be thrown; "s" is nullable here.
        if (StringUtils.isNotBlank(s)) {
            try {
                return Integer.parseInt(s.substring(11, 13));
            } catch (Exception e) {
                if (logger.isErrorEnabled()) {
                    logger.error(e.getMessage(), e);
                }
                throw new Exception(e.getMessage(), e);
            }
        }
        return 0;
    }

    /**
     * 
     * getMinute:(获取：分). <br/>
     *
     *
     * @param datetime
     * @return
     * @throws Exception
     */
    public int getMinute(String datetime) throws Exception {
        String s = format(parseDate(datetime), DATETIME24_PATTERN_LINE);
        // A "NullPointerException" could be thrown; "s" is nullable here.
        if (StringUtils.isNotBlank(s)) {
            try {
                return Integer.parseInt(s.substring(14, 16));
            } catch (Exception e) {
                if (logger.isErrorEnabled()) {
                    logger.error(e.getMessage(), e);
                }
                throw new Exception(e.getMessage(), e);
            }
        }
        return 0;
    }

    /**
     * 
     * getSeconds:(获取：秒). <br/>
     *
     *
     * @param datetime
     * @return
     * @throws Exception
     */
    public int getSeconds(String datetime) throws Exception {
        String s = format(parseDate(datetime), DATETIME24_PATTERN_LINE);
        // A "NullPointerException" could be thrown; "s" is nullable here.
        if (StringUtils.isNotBlank(s)) {
            try {
                return Integer.parseInt(s.substring(17, 19));
            } catch (Exception e) {
                if (logger.isErrorEnabled()) {
                    logger.error(e.getMessage(), e);
                }
                throw new Exception(e.getMessage(), e);
            }
        }
        return 0;
    }

    /**
     * 
     * parseDate:(long类型转Date). <br/>
     *
     *
     * @param datetime long型时间
     * @return
     */
    public static Date parseDate(long datetime) {
        return new Date(datetime);
    }

    /**
     * 
     * parseDate:(String类型转Date). <br/>
     *
     *
     * @param datetime 时间字符串
     * @return
     * @throws Exception
     */
    public static Date parseDate(String datetime) throws Exception {
        return parseDate(datetime, DATETIME24_PATTERN_LINE);
    }

    /**
     * 
     * parseDate:(时间字符串转时间). <br/>
     *
     *
     * @param datetime 时间
     * @param pattern  格式
     * @return
     * @throws Exception
     */
    public static Date parseDate(String datetime, String pattern) throws Exception {
        if (StringUtils.isEmpty(datetime)) {
            return null;
        }
        try {
            SimpleDateFormat dtFmt = dateFormat.get();
            dtFmt.applyPattern(pattern);
            return dtFmt.parse(datetime);
        } catch (ParseException e) {
            if (logger.isErrorEnabled()) {
                logger.error(e.getMessage(), e);
            }
            throw new Exception(e.getMessage(), e);
        }
    }

    /**
     * 
     * parseTimestamp:(解析时间戳). <br/>
     *
     *
     * @param datetime 时间
     * @param pattern  格式
     * @return 时间戳
     * @throws Exception
     */
    public static Timestamp parseTimestamp(String datetime, String pattern) throws Exception {
        // A "NullPointerException" could be thrown; "parseDate" is nullable here.
        Date parseDate = parseDate(datetime, pattern);
        if (parseDate == null) {
            return null;
        }
        return new Timestamp(parseDate.getTime());
    }

    /**
     * 解析时间戳
     * 
     * @param str
     * @return
     * @throws Exception
     */
    public static Timestamp parseTimestamp(String str) throws Exception {
        if ("".equals(str) || str == null) {
            return null;
        }
        Timestamp dt = null;
        SimpleDateFormat dtFmt = dateFormat.get();
        dtFmt.applyPattern(DATE_PATTERN_LINE);
        try {
            dt = new Timestamp(dtFmt.parse(str).getTime());
        } catch (ParseException e) {
            if (logger.isErrorEnabled()) {
                logger.error(e.getMessage(), e);
            }
            throw new Exception(e.getMessage(), e);
        }
        return dt;
    }

    /**
     * 
     * doFormat:(解析时间格式). <br/>
     *
     *
     * @param obj     时间类
     * @param pattern 格式
     * @return String
     */
    public static String doFormat(Object obj, String pattern) {
        if (obj == null) {
            return null;
        }
        SimpleDateFormat dtFmt = dateFormat.get();
        dtFmt.applyPattern(pattern);
        if (obj instanceof Date) {
            return dtFmt.format((Date) obj);

        } else if (obj instanceof Timestamp) {
            return dtFmt.format((Timestamp) obj);

        } else if (obj instanceof Calendar) {
            return dtFmt.format(((Calendar) obj).getTime());

        } else if (obj instanceof Number) {
            return dtFmt.format(parseDate(((Number) obj).longValue()));
        } else {
            return dtFmt.format(obj);
        }
    }

    /**
     * 
     * format:(时间格式化). <br/>
     *
     *
     * @param datetime 时间
     * @return String
     */
    public static String format(Date datetime) {
        return format(datetime, DATETIME24_PATTERN_LINE);
    }

    /**
     * 
     * format:(时间格式化). <br/>
     *
     *
     * @param datetime 时间
     * @return String
     */
    public static String format(Date date, String pattern) {
        return doFormat(date, pattern);
    }

    /**
     * 
     * format:(时间格式化). <br/>
     *
     *
     * @param datetime 时间
     * @return String
     */
    public static String format(Timestamp timestamp) {
        return format(timestamp, DATETIME24_PATTERN_LINE);
    }

    /**
     * 
     * format:(时间格式化). <br/>
     *
     *
     * @param datetime 时间
     * @return String
     */
    public static String format(Timestamp timestamp, String pattern) {
        return doFormat(timestamp, pattern);
    }

    /**
     * 
     * format:(时间格式化). <br/>
     *
     *
     * @param datetime 时间
     * @return String
     */
    public static String format(Calendar time) {
        return format(time, DATETIME24_PATTERN_LINE);
    }

    /**
     * 
     * format:(时间格式化). <br/>
     *
     *
     * @param datetime 时间
     * @return String
     */
    public static String format(Calendar time, String pattern) {
        return format(time.getTime(), pattern);
    }

    /**
     * 
     * format:(时间格式化). <br/>
     *
     *
     * @param datetime 时间
     * @return String
     * @throws Exception
     */
    public static String format(String datetime) throws Exception {
        return format(datetime, DATETIME24_PATTERN_LINE);
    }

    /**
     * 
     * format:(时间格式化). <br/>
     *
     *
     * @param datetime 时间
     * @return String
     * @throws Exception
     */
    public static String format(String datetime, String pattern) throws Exception {
        return format(parseDate(datetime, pattern), pattern);
    }

    /**
     * 
     * format:(时间格式化). <br/>
     *
     *
     * @param datetime 时间
     * @return String
     */
    public static String format(long datetime) {
        return format(parseDate(datetime), DATETIME24_PATTERN_LINE);
    }

    /**
     * 
     * format:(时间格式化). <br/>
     *
     *
     * @param datetime 时间
     * @return String
     */
    public static String format(long datetime, String pattern) {
        return format(parseDate(datetime), pattern);
    }

    /**
     * 
     * format:(时间格式化). <br/>
     *
     *
     * @param datetime 时间
     * @return String
     */
    public static String format(java.sql.Date date) {
        return format(toUtilDate(date), DATETIME24_PATTERN_LINE);
    }

    /**
     * 
     * format:(时间格式化). <br/>
     *
     *
     * @param datetime 时间
     * @return String
     */
    public static String format(java.sql.Date date, String pattern) {
        return format(toUtilDate(date), pattern);
    }

    /**
     * 
     * addYearAsStr:(增加年). <br/>
     *
     *
     * @param datetime
     * @param years
     * @return
     * @throws Exception
     */
    public static String addYearAsStr(String datetime, int years) throws Exception {
        return format(addYear(datetime, years));
    }

    /**
     * 
     * addYearAsStr:(增加年). <br/>
     *
     *
     * @param datetime 时间
     * @param years    几年
     * @param pattern  格式
     * @return String
     * @throws Exception
     */
    public static String addYearAsStr(String datetime, int years, String pattern) throws Exception {
        return format(addYear(datetime, years), pattern);
    }

    /**
     * 
     * addYear:(增加年). <br/>
     *
     *
     * @param datetime
     * @param years
     * @return Date
     * @throws Exception
     */
    public static Date addYear(String datetime, int years) throws Exception {
        return addYear(parseDate(datetime), years);
    }

    /**
     * 
     * addYearAsStr:(增加年). <br/>
     *
     *
     * @param datetime
     * @param years
     * @return String
     */
    public static String addYearAsStr(Date datetime, int years) {
        return format(addYear(datetime, years));
    }

    /**
     * 
     * addYearAsStr:(增加年). <br/>
     *
     *
     * @param datetime
     * @param years
     * @param pattern
     * @return String
     */
    public static String addYearAsStr(Date datetime, int years, String pattern) {
        return format(addYear(datetime, years), pattern);
    }

    /**
     * 
     * addYear:(增加年). <br/>
     *
     *
     * @param datetime 时间
     * @param years    几年后
     * @return Date返回指定几年后的时间
     */
    public static Date addYear(Date datetime, int years) {
        Calendar calendar = getCalendar(datetime);
        calendar.add(Calendar.YEAR, years);
        return calendar.getTime();
    }

    /**
     * 
     * addDayAsStr:(几天后的时间). <br/>
     *
     *
     * @param datetime
     * @param days
     * @return String
     * @throws Exception
     */
    public static String addDayAsStr(String datetime, int days) throws Exception {
        return format(addDay(datetime, days));
    }

    /**
     * 
     * addDayAsStr:(几天后). <br/>
     *
     *
     * @param datetime
     * @param days
     * @param pattern
     * @return String
     * @throws Exception
     */
    public static String addDayAsStr(String datetime, int days, String pattern) throws Exception {
        return format(addDay(datetime, days), pattern);
    }

    /**
     * 
     * addDay:(几天后). <br/>
     *
     *
     * @param datetime
     * @param days
     * @return Date
     * @throws Exception
     */
    public static Date addDay(String datetime, int days) throws Exception {
        return addDay(parseDate(datetime), days);
    }

    /**
     * 
     * addDay:(几天后). <br/>
     *
     *
     * @param datetime
     * @param days
     * @return Date
     */
    public static Date addDay(Date datetime, int days) {
        Calendar calendar = getCalendar(datetime);
        calendar.add(Calendar.DAY_OF_YEAR, days);
        return calendar.getTime();
    }

    /**
     * 
     * addMonthAsStr:(几个月后). <br/>
     *
     *
     * @param datetime
     * @param months
     * @param pattern
     * @return String
     */
    public static String addMonthAsStr(Date datetime, int months, String pattern) {
        return format(addMonth(datetime, months), pattern);
    }

    /**
     * 
     * addMonthAsStr:(几个月后). <br/>
     *
     *
     * @param datetime
     * @param months
     * @return String
     */
    public static String addMonthAsStr(Date datetime, int months) {
        return addMonthAsStr(datetime, months);
    }

    /**
     * 
     * addMonth:(几个月后). <br/>
     *
     *
     * @param datetime
     * @param months
     * @return Date
     * @throws Exception
     */
    public static Date addMonth(String datetime, int months) throws Exception {
        return addMonth(parseDate(datetime), months);
    }

    /**
     * 
     * addMonth:(几个月后). <br/>
     *
     *
     * @param datetime
     * @param months
     * @return Date
     */
    public static Date addMonth(Date datetime, int months) {
        Calendar calendar = getCalendar(datetime);
        calendar.add(Calendar.MONTH, months);
        return calendar.getTime();
    }

    /**
     * 
     * addSecondsAsStr:(几秒后). <br/>
     *
     *
     * @param datetime
     * @param seconds
     * @return String
     * @throws Exception
     */
    public static String addSecondsAsStr(String datetime, int seconds) throws Exception {
        return format(addSeconds(parseDate(datetime), seconds));
    }

    /**
     * 
     * addSecondsAsStr:(几秒后). <br/>
     *
     *
     * @param datetime
     * @param seconds
     * @param pattern
     * @return
     * @throws Exception
     */
    public static String addSecondsAsStr(String datetime, int seconds, String pattern) throws Exception {
        return format(addSeconds(parseDate(datetime), seconds), pattern);
    }

    /**
     * 
     * addSecondsAsStr:(几秒后). <br/>
     *
     *
     * @param datetime
     * @param seconds
     * @return
     */
    public static String addSecondsAsStr(Date datetime, int seconds) {
        return format(addSeconds(datetime, seconds));
    }

    /**
     * 
     * addSecondsAsStr:(几秒后). <br/>
     *
     *
     * @param datetime
     * @param seconds
     * @param pattern
     * @return String
     */
    public static String addSecondsAsStr(Date datetime, int seconds, String pattern) {
        return format(addSeconds(datetime, seconds), pattern);
    }

    /**
     * 
     * addSeconds:(几秒后). <br/>
     *
     *
     * @param datetime
     * @param seconds
     * @return
     * @throws Exception
     */
    public static Date addSeconds(String datetime, int seconds) throws Exception {
        return addSeconds(parseDate(datetime), seconds);
    }

    /**
     * 
     * addSeconds:(几秒后). <br/>
     *
     *
     * @param datetime
     * @param seconds
     * @return
     */
    public static Date addSeconds(Date datetime, int seconds) {
        Calendar calendar = getCalendar(datetime);
        calendar.add(Calendar.SECOND, seconds);
        return calendar.getTime();
    }

    /**
     * 
     * minus:(取时间差，开始时间与结束时间). <br/>
     *
     *
     * @param start 开始时间
     * @param end   结束时间
     * @return
     * @throws Exception
     */
    public static long minus(String start, String end) throws Exception {
        Date startDate = parseDate(start);
        Date endDate = parseDate(end);
        if (startDate != null && endDate != null) {
            return minus(startDate, endDate);
        }
        return 0;
    }

    /**
     * 
     * minus:(取时间差). <br/>
     *
     *
     * @param start
     * @param end
     * @return
     */
    public static long minus(Date start, Date end) {
        return minus(start.getTime(), end.getTime());
    }

    /**
     * 
     * minus:(取时间差). <br/>
     *
     *
     * @param start
     * @param end
     * @return
     */
    public static long minus(Timestamp start, Timestamp end) {
        return minus(start.getTime(), end.getTime());
    }

    /**
     * 
     * minus:(取时间差). <br/>
     *
     *
     * @param start
     * @param end
     * @return
     */
    public static long minus(long start, long end) {
        return end - start;
    }

    /**
     * 
     * isAfter:(判断是否是该时间是否在当前时间之后). <br/>
     *
     *
     * @param start 开始时间
     * @return boolean
     */
    public static boolean isAfter(Date start) {
        return isAfter(start, getCurrentTime());
    }

    /**
     * 
     * isAfter:(判断结束时间是否在开始时间之后). <br/>
     *
     *
     * @param start
     * @param end
     * @return
     */
    public static boolean isAfter(Date start, Date end) {
        return (start != null && end != null && start.after(end));
    }

    /**
     * 将java.util.Date转换为java.sql.Date数据类型
     * 
     * @param date 需要转换的java.sql.Date数据
     * @return 转换后的java.sql.Timestamp数据
     */
    public static Timestamp toSqlTimestamp(Date date) {
        if (date == null) {
            return null;
        } else {
            return new Timestamp(date.getTime());
        }
    }

    /**
     * 
     * toTimestamp:(将java.util.Date转换为java.sql.Timestamp时间戳数据类型). <br/>
     *
     *
     * @param date 需要转换的java.sql.Date数据
     * @return 转换后的java.sql.Timestamp数据
     */
    public static Timestamp toTimestamp(Date date) {
        return new Timestamp(date.getTime());
    }

    /**
     * 
     * toTime:(将java.util.Date转换为java.sql.Time数据类型). <br/>
     *
     *
     * @param date 需要转换的java.sql.Date数据
     * @return 转换后的java.sql.Time数据
     */
    public static Time toTime(Date date) {
        return new Time(date.getTime());
    }

    /**
     * 添加日期时间之前
     * 
     * @param datetime 日期时间
     * @param yrs      年
     * @return
     * @throws Exception
     */
    public static String addDatetimeByYear(String datetime, int yrs) throws Exception {
        Date date1 = DateUtil.parseDate(datetime);
        // 使用日历加天数
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date1);
        calendar.add(Calendar.YEAR, yrs);
        // 返回结果
        return getDate(calendar.getTime());
    }

    /**
     * 日期，时间按系统设置的格式转换成字符串
     * 
     * @param date 日期
     * @return
     */
    public static String toString(Date date) {
        if (date == null) {
            return null;
        }
        SimpleDateFormat dtFmt = dateFormat.get();
        dtFmt.applyPattern(DATE_PATTERN_LINE);
        return dtFmt.format(date);
    }

    /**
     * 将日期，时间按系统设置的格式转换成字符串
     * 
     * @param date    时间
     * @param pattern 模式
     * @return
     */
    public static String toString(Date date, String pattern) {
        if (date == null) {
            return null;
        }
        SimpleDateFormat dtFmt = dateFormat.get();
        dtFmt.applyPattern(pattern);
        return dtFmt.format(date);
    }

    /**
     * 以字符串
     * 
     * @param timestamp 时间邮票
     * @param pattern   模式
     * @return
     */
    public static String toString(Timestamp timestamp, String pattern) {
        if (timestamp == null) {
            return null;
        }
        SimpleDateFormat dtFmt = dateFormat.get();
        dtFmt.applyPattern(pattern);
        return dtFmt.format(timestamp);
    }

    /**
     * 添加日期时间按年
     * 
     * @param datetime 日期时间
     * @param yrs      年
     * @return
     * @throws Exception
     */
    public static Date addDatetimeByYear(Date datetime, int yrs) throws Exception {
        return DateUtil.parseDate(addDatetimeByYear(DateUtil.toString(datetime), yrs));
    }

    /**
     * 将指定的日期时间字符串加上天数返回新的日期时间字符串。如将1999-12-31 23:59:59 加上1天，结果是2000-01-01 12:59:59
     * 
     * @param datetime 日期时间字符串
     * @param days     天数
     * @return java.lang.String 日期时间字符串
     * @throws Exception
     */
    public static String addDatetimeByDay(String datetime, int days) throws Exception {
        Date date1 = DateUtil.parseDate(datetime, DATETIME24_PATTERN_LINE);
        // 使用日历加天数
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date1);
        calendar.add(Calendar.DAY_OF_YEAR, days);
        // 返回结果
        return getDate(calendar.getTime(), DATETIME24_PATTERN_LINE);
    }

    /**
     * 添加日期时间按日
     * 
     * @param datetime 日期时间
     * @param days     天
     * @return
     * @throws Exception
     */
    public static Date addDatetimeByDay(Date datetime, int days) throws Exception {
        return DateUtil.parseDate(addDatetimeByDay(DateUtil.toString(datetime, DATETIME24_PATTERN_LINE), days));
    }

    /**
     * 将指定的日期时间字符串加上月数返回新的日期时间字符串。如将1999-12-31 23:59:59 加上1月，结果是2000-01-31 23:59:59
     * 
     * @param datetime 日期时间字符串
     * @param months   月数
     * @return java.lang.String 日期时间字符串
     */
    public static String addDatetimeByMonth(Date datetime, int months, String pattern) {
        // 使用日历加月份
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(datetime);
        calendar.add(Calendar.MONTH, months);
        // 返回结果
        return getDate(calendar.getTime(), pattern);
    }

    /**
     * 添加日期时间按月
     * 
     * @param datetime
     * @param months   月
     * @return
     * @throws Exception
     */
    public static String addDatetimeByMonth(String datetime, int months) throws Exception {
        return addDatetimeByMonth(DateUtil.parseDate(datetime), months, DATE_PATTERN_LINE);
    }

    /**
     * 将指定的日期时间字符串加上秒数返回新的日期时间字符串。如将1999-12-31 23:59:59 加上3600秒，结果是2000-01-01
     * 00:59:59
     * 
     * @param datetime 日期时间字符串
     * @param seconds  秒数
     * @return java.lang.String 日期时间字符串
     * @throws Exception
     */
    public static String addDatetimeBySecond(String datetime, int seconds) throws Exception {
        Date date1 = DateUtil.parseDate(datetime);
        // 使用日历加秒数
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date1);
        calendar.add(Calendar.SECOND, seconds);
        return getDate(calendar.getTime(), DATETIME12_PATTERN_LINE);
    }

    /**
     * 将指定的日期时间字符串加上秒数返回新的日期时间字符串。如将1999-12-31 23:59:59 加上3600秒，结果是2000-01-01
     * 00:59:59
     * 
     * @param datetime 日期时间字符串
     * @param seconds  秒数
     * @return java.lang.String 日期时间字符串
     * @throws Exception
     */
    public static String addDatetimeBySecond24(String datetime, int seconds) throws Exception {
        Date date1 = DateUtil.parseDate(datetime);
        // 使用日历加秒数
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date1);
        calendar.add(Calendar.SECOND, seconds);
        return getDate(calendar.getTime(), DATETIME24_PATTERN_LINE);
    }

    /**
     * 添加日期时间按秒
     * 
     * @param datetime 日期时间字符串
     * @param seconds  秒
     * @return
     * @throws Exception
     */
    public static Date addDatetimeBySecond(Date datetime, int seconds) throws Exception {
        return DateUtil.parseDate(addDatetimeBySecond(DateUtil.toString(datetime, DATETIME12_PATTERN_LINE), seconds));
    }

    /**
     * 添加日期时间按秒 24小时制
     * 
     * @param datetime 日期时间字符串
     * @param seconds  秒
     * @return
     * @throws Exception
     */
    public static Date addDatetimeBySecond24(Date datetime, int seconds) throws Exception {
        return DateUtil.parseDate(addDatetimeBySecond24(DateUtil.toString(datetime, DATETIME24_PATTERN_LINE), seconds));
    }

    /**
     * 得到当前日期，格式2002-02-01
     * 
     * @return java.lang.String -返回当前日期字符串，格式为：Year-Month-day
     */
    public static String getDate() {
        return getDate(new Date());
    }

    /**
     * 获取当前日期，时间
     */
    public static Date getCurrentDate() {
        return new Date(System.currentTimeMillis());
    }

    /**
     * 得到当前日期，格式2002-02-01
     * 
     * @param pattern 模式
     * @return java.lang.String -返回当前日期字符串，格式为：Year-Month-day
     */
    public static String getDate(String pattern) {
        return toString(getCurrentDate(), pattern);
    }

    /**
     * 将指定日期转换为格式字符串，例如2002-12-12
     * 
     * @param date    日期对象
     * @param pattern 模式
     * @return java.lang.String
     */
    public static String getDate(Date date, String pattern) {
        SimpleDateFormat formatter = dateFormat.get();
        formatter.applyPattern(pattern);
        return formatter.format(date);
    }

    /**
     * 将指定日期转换为格式字符串，例如2002-12-12
     * 
     * @param date 日期对象
     * @return java.lang.String
     */
    public static String getDate(Date date) {
        SimpleDateFormat formatter = dateFormat.get();
        formatter.applyPattern(DATE_PATTERN_LINE);
        return formatter.format(date);
    }

    /**
     * 返回两个日期时间字符串之间的差,日期尾减日期头，以秒为单位。 如果值为负数，说明日期头大于日期尾 尾
     * 
     * @param headDatetime - 日期头 格式为1999-10-10 12:12:12
     * @param tailDatetime - 日期尾 格式为1999-10-10 12:12:12
     * @return long 差
     * @throws Exception
     */
    public static long getDatetimeGap(String headDatetime, String tailDatetime) throws Exception {
        Date date1 = DateUtil.parseDate(headDatetime);
        Date date2 = DateUtil.parseDate(tailDatetime);
        // A "NullPointerException" could be thrown
        if (date1 != null && date2 != null) {
            long apple = date2.getTime() - date1.getTime();
            // 返回秒
            return apple / 1000;
        }
        return 0;
    }

    public static String getDataString(String s) {
        String y = s.substring(0, 4);
        String m = s.substring(5, 7);
        String d = s.substring(8, 10);
        String h = s.substring(11, 13);
        String mi = s.substring(14, 16);
        String se = s.substring(17, 19);
        return y + m + d + h + mi + se;
    }

    /**
     * get the frist day of this month
     * 
     * @param datetime 时间日期字符串
     * @return
     * @throws Exception
     */
    public static Date getFristDayOfThisMonth(String datetime) throws Exception {
        Date date = getFristDayOfThisMonth(parseDate(datetime));
        return date;
    }

    /**
     * 
     * @param datetime
     * @return
     */
    public static Date getFristDayOfThisMonth(Date datetime) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(datetime);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        Date date = new Date(((Date) calendar.getTime()).getTime());
        return date;
    }

    /**
     * get date by year and month the datetime format should be 'yyyy-mm'
     * 
     * @param datetime
     * @return
     */
    public static Date getDateTimeByYearAndMonth(String datetime) {
        int year = Integer.parseInt(datetime.substring(0, 4));
        int month = Integer.parseInt(datetime.substring(5, datetime.length()));
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month - 1);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        Date date = new Date(((Date) calendar.getTime()).getTime());
        return date;
    }

    /**
     * get current month end
     * 
     * @return
     */
    public static String getCurrentMonthEnd() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(DateUtil.getCurrentDate());
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);

        if (month == 0) {
            year = year - 1;
            month = 12;
        } else {
            month = month - 1;
        }
        String currentMonthEnd = String.valueOf(year) + String.valueOf(month);
        return currentMonthEnd;
    }

    /**
     * 比较传入的时间是否是当前日期之后
     * 
     * @param date
     * @return
     */
    public static boolean isAfterToday(Date date) {
        if (date != null && date.after(new Date())) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 将java.sql.Date转换为java.util.Date数据类型
     * 
     * @param date 需要转换的java.sql.Date数据
     * @return 转换后的java.util.Date数据
     */
    public static Date toUtilDate(java.sql.Date date) {
        if (date == null) {
            return null;
        } else {
            return new Date(date.getTime());
        }
    }

    /**
     * 将java.util.Date转换为java.sql.Date数据类型
     * 
     * @param date 需要转换的java.sql.Date数据
     * @return 转换后的java.sql.Date数据
     */
    public static Time toSqlTime(Date date) {
        if (date == null) {
            return null;
        } else {
            return new Time(date.getTime());
        }
    }

    /**
     * 将java.util.Date转换为java.sql.Date数据类型
     * 
     * @param date 需要转换的java.sql.Date数据
     * @return 转换后的java.sql.Date数据
     */
    public static java.sql.Date toSqlDate(Date date) {
        if (date == null) {
            return null;
        } else {
            return new java.sql.Date(date.getTime());
        }
    }

    /**
     * 根据指定的格式化模式,格式化日历数据<br>
     * 默认使用yyyy-MM-dd HH:mm:ss
     * 
     * @param now 给定日期
     * @return 被格式化后的字符串
     */
    public static String formatDate(Calendar now) {
        return formatDate(now, DATETIME24_PATTERN_LINE);
    }

    /**
     * 根据指定的格式化模式,格式化日历数据<br>
     * 如果格式化模式为null或者为空,则默认使用yyyy-MM-dd HH:mm:ss
     * 
     * @param now            给定日期
     * @param formatePattern 格式化模式
     * @return 被格式化后的字符串<br>
     */
    public static String formatDate(Calendar now, String formatePattern) {
        if (now == null) {
            return null;
        }
        if (formatePattern == null || formatePattern.trim().length() <= 0) {
            formatePattern = DATETIME24_PATTERN_LINE;
        }
        Date tempDate = now.getTime();
        SimpleDateFormat formatter = dateFormat.get();
        formatter.applyPattern(formatePattern);
        return formatter.format(tempDate);
    }

    /**
     * 将java.util.Date数据转换为指定格式的字符串<br>
     * 如果格式化模式为null或者为空,则默认使用yyyy-MM-dd HH:mm:ss
     * 
     * @param date           java.util.Date类型数据
     * @param formatePattern 指定的日期格式化模式
     * @return 格式化后的日期的字符串形式<br>
     * 
     */
    public static String formatDate(Date date, String formatePattern) {
        if (formatePattern == null || formatePattern.trim().length() <= 0) {
            formatePattern = DATETIME24_PATTERN_LINE;
        }
        if (date == null) {
            return "";
        } else {
            SimpleDateFormat formatter = dateFormat.get();
            formatter.applyPattern(formatePattern);
            return formatter.format(date);
        }
    }

    /**
     * 将java.sql.Timestamp数据转换为指定格式的字符串<br>
     * 如果格式化模式为null或者为空,则默认使用yyyy-MM-dd HH:mm:ss
     * 
     * @param date           Timestamp数据
     * @param formatePattern 日期格式化模式
     * @return 格式化后的日期的字符串形式
     */
    public static String formatDate(Timestamp date, String formatePattern) {
        if (formatePattern == null || formatePattern.trim().length() <= 0) {
            formatePattern = DATETIME24_PATTERN_LINE;
        }
        if (date == null) {
            return "";
        } else {
            SimpleDateFormat formatter = dateFormat.get();
            formatter.applyPattern(formatePattern);
            return formatter.format(date);
        }
    }

    /**
     * 将java.util.Date数据转换为指定格式的字符串<br>
     * 如果格式化模式为null或者为空,则默认使用yyyy-MM-dd
     * 
     * @param date java.util.Date类型数据
     * @return 格式化后的日期的字符串形式<br>
     */
    public static String formatDate(Date date) {
        return formatDate(date, DATE_PATTERN_LINE);
    }

    /**
     * 将代表日期的长整形数值转换为yyyy-MM-dd HH:mm:ss格式的字符串<br>
     * 
     * @param datetime 需要转换的日期的长整形数值
     * @return 格式化后的日期字符串
     * @throws Exception
     */
    public static String formatDate(long datetime) {
        return formatDate(datetime, DATETIME24_PATTERN_LINE);
    }

    /**
     * 将代表日期的字符串转换yyyy-MM-dd HH:mm:ss格式的字符串
     * 
     * @param datetime 需要转换的日期
     * @return 格式化后的日期字符串
     * @throws Exception
     */
    public static String formate(String datetime) throws Exception {
        return formatDate(datetime, DATETIME24_PATTERN_LINE);
    }

    /**
     * 将代表日期的字符串转换未指定格式的字符串<br>
     * 如果格式化模式为null或者为空,则默认使用yyyy-MM-dd HH:mm:ss
     * 
     * @param datetime       需要转换的日期的字符串
     * @param formatePattern 指定的日期格式
     * @return 格式化后的日期字符串
     * @throws Exception
     */
    public static String formatDate(String datetime, String formatePattern) throws Exception {
        String dateStr = "";
        if (datetime == null || datetime.trim().length() <= 0) {
            return "";
        }
        try {
            Date date = null;
            if (formatePattern != null
                    && (formatePattern.equals(DATE_PATTERN_BIAS) || formatePattern.equals(DATE_PATTERN_LINE))) {
                date = parseDate(datetime);
            } else {
                date = parseDate(datetime);
            }

            dateStr = formatDate(date, formatePattern);
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error(e.getMessage(), e);
            }
            throw new Exception(e.getMessage(), e);
        }
        return dateStr;
    }

    /**
     * 将代表日期的长整形数值转换为y指定格式的字符串<br>
     * 如果格式化模式为null或者为空,则默认使用yyyy-MM-dd HH:mm:ss
     * 
     * @param datetime       需要转换的日期的长整形数值
     * @param formatePattern 指定的日期格式
     * @return 格式化后的日期字符串
     * @throws Exception
     */
    public static String formatDate(long datetime, String formatePattern) {
        String dateStr = "";
        if (datetime <= 0) {
            return "";
        }
        Date date = new Date(datetime);
        dateStr = formatDate(date, formatePattern);
        return dateStr;
    }

    /**
     * 将java.sql.Date数据转换为指定格式的字符串<br>
     * 默认使用yyyy-MM-dd HH:mm:ss
     * 
     * @param date java.sql.Date类型数据
     * @return 格式化后的日期的字符串形式<br>
     */
    public static String formatDate(java.sql.Date date) {
        return formatDate(toUtilDate(date));
    }

    /**
     * 
     * @param start
     * @param end
     * @return
     */
    public static String calculateTimeInterval(Timestamp start, Timestamp end) {
        if (start != null && end != null) {
            return (end.getTime() - start.getTime()) + "";
        }
        return null;
    }

    /**
     * 将时间转换为国际标准时间
     * 
     * @param date 日期字符串
     * @return
     * @throws Exception
     */
    public static Date convertSTD(String date) throws Exception {
        try {
            SimpleDateFormat formatter = dateFormat.get();
            formatter.applyPattern("yyyy-MM-dd HH:mm:ss");
            return formatter.parse(date);
        } catch (ParseException e) {
            if (logger.isErrorEnabled()) {
                logger.error(e.getMessage(), e);
            }
            throw new Exception(e.getMessage(), e);
        }
    }

    /**
     * 将时间转换为sql国际标准时间
     * 
     * @param date 日期字符串
     * @return
     * @throws Exception
     */
    public static java.sql.Date convertSQLSTD(String date) throws Exception {
        try {
            SimpleDateFormat formatter = dateFormat.get();
            formatter.applyPattern("yyyy-MM-dd HH:mm:ss");
            long time = formatter.parse(date).getTime();
            return new java.sql.Date(time);
        } catch (ParseException e) {
            if (logger.isErrorEnabled()) {
                logger.error(e.getMessage(), e);
            }
            throw new Exception(e.getMessage(), e);
        }
    }

    /**
     * 将时间转换为国际标准时间
     * 
     * @param date 日期对象
     * @return
     */
    public static String convertDTS(Date date) {
        SimpleDateFormat formatter = dateFormat.get();
        formatter.applyPattern("yyyy-MM-dd HH:mm:ss");
        return formatter.format(date);
    }

    /**
     * 功能描述：获取距离当前时间days天内的时间戳，即前days的时间戳
     * 
     * @param days 距离天数
     * @return 返回值：返回时间信息
     */
    public static Timestamp getTimestampNowSomeDayBefore(int days) {
        long st = System.currentTimeMillis();
        long temp = st - days * 86400000L;
        if (temp < 0) {
            temp = 0;
        }
        return new Timestamp(temp);
    }

    /**
     * 获取当前日期的最近几天的日期格式<br>
     * days可为负数为取前几天
     * 
     * @param days    前几天
     * @param pattern 格式
     * @return
     */
    public static String getLastFewDays(int days, String pattern) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, days);
        SimpleDateFormat formatter = dateFormat.get();
        formatter.applyPattern(pattern);
        return formatter.format(calendar.getTime());
    }

    /**
     * 判断当前字符串是否是日期格式
     * 
     * @param p_str 字符串
     * @return
     */
    public static boolean isDate(String p_str) {
        String regEx = "((((1[6-9]|[2-9]\\d)\\d{2})-(0?[13578]|1[02])-(0?[1-9]|[12]\\d|3[01]))|(((1[6-9]|[2-9]\\d)\\d{2})-(0?[13456789]|1[012])-(0?[1-9]|[12]\\d|30))|"
                + "(((1[6-9]|[2-9]\\d)\\d{2})-0?2-(0?[1-9]|1\\d|2[0-8]))|(((1[6-9]|[2-9]\\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))-0?2-29-)) "
                + "(20|21|22|23|[0-1]?\\d):[0-5]?\\d:[0-5]?\\d|((((1[6-9]|[2-9]\\d)\\d{2})-(0?[13578]|1[02])-(0?[1-9]|[12]\\d|3[01]))|"
                + "(((1[6-9]|[2-9]\\d)\\d{2})-(0?[13456789]|1[012])-(0?[1-9]|[12]\\d|30))|(((1[6-9]|[2-9]\\d)\\d{2})-0?2-(0?[1-9]|1\\d|2[0-8]))|"
                + "(((1[6-9]|[2-9]\\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))-0?2-29-))";
        if (Pattern.compile(regEx).matcher(p_str).matches()) {
            return true;
        }
        return false;
    }

    public static String getWeekDays() {
        String[] weekDays = { "sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday" };

        Calendar cal = Calendar.getInstance();
        Date date = new Date();
        cal.setTime(date);
        int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (w < 0) {
            w = 0;
        }
        return weekDays[w];
    }

    public static String getTimeDifference(Date begin, Date end) throws Exception {
        long between = 0;
        try {
            between = (end.getTime() - begin.getTime());// 得到两者的毫秒数
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error(e.getMessage(), e);
            }
            throw new Exception(e.getMessage(), e);
        }
        long day = between / (24 * 60 * 60 * 1000);
        long hour = (between / (60 * 60 * 1000) - day * 24);
        long min = ((between / (60 * 1000)) - day * 24 * 60 - hour * 60);
        long s = (between / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
        long ms = (between - day * 24 * 60 * 60 * 1000 - hour * 60 * 60 * 1000 - min * 60 * 1000 - s * 1000);
        String timeDifference = day + "天" + hour + "小时" + min + "分" + s + "秒" + ms + "毫秒";
        return timeDifference;
    }

    /*
     * 将字符串按系统设置的格式转换成字符串，并且加一天
     */
    public static Date addParseDate(String str, String pattern) throws Exception {
        if (str == null || str.equals(""))
            return null;

        Date dt = null;
        DateFormat dtFmt = new SimpleDateFormat(pattern);
        try {
            dt = new Date(dtFmt.parse(str).getTime());
        } catch (ParseException e) {
            throw e;
        }
        Calendar ca = new GregorianCalendar();
        ca.setTime(dt);
        ca.add(Calendar.DATE, 1);
        dt = ca.getTime();
        return dt;
    }
    
    
    
    /**
     * 计算2个日期之间相差的  以年、月、日为单位，各自计算结果是多少
     * 比如：2011-02-02 到  2017-03-02
     * 以年为单位相差为：6年
     * @param fromDate
     * @param toDate
     * @return
     */
    public static int yearCompare(Date fromDate,Date toDate){
        Calendar  from  =  Calendar.getInstance();
        from.setTime(fromDate);
        Calendar  to  =  Calendar.getInstance();
        to.setTime(toDate);
        //只要年月
        int fromYear = from.get(Calendar.YEAR);
        int toYear = to.get(Calendar.YEAR);
        int year = toYear  -  fromYear;
        return year;
    }
    
    /**
     * 计算2个日期之间相差的  以年、月、日为单位，各自计算结果是多少
     * 比如：2011-02-02 到  2017-03-02
     * 以月为单位相差为：73个月
     * @param fromDate
     * @param toDate
     * @return
     */
    public static int monthCompare(Date fromDate,Date toDate){
        Calendar  from  =  Calendar.getInstance();
        from.setTime(fromDate);
        Calendar  to  =  Calendar.getInstance();
        to.setTime(toDate);
        //只要年月
        int fromYear = from.get(Calendar.YEAR);
        int fromMonth = from.get(Calendar.MONTH);
     
        int toYear = to.get(Calendar.YEAR);
        int toMonth = to.get(Calendar.MONTH);
        int month = toYear *  12  + toMonth  -  (fromYear  *  12  +  fromMonth);
        return month;
    }
    
    /**
     * 计算2个日期之间相差的  以年、月、日为单位，各自计算结果是多少
     * 比如：2011-02-02 到  2017-03-02
     * 以日为单位相差为：2220天
     * @param fromDate
     * @param toDate
     * @return
     */
    public static int dayCompare(Date fromDate,Date toDate){
        Calendar  from  =  Calendar.getInstance();
        from.setTime(fromDate);
        Calendar  to  =  Calendar.getInstance();
        to.setTime(toDate);
        int day = (int) ((to.getTimeInMillis()  -  from.getTimeInMillis())  /  (24  *  3600  *  1000));
        return day;
    }

}
