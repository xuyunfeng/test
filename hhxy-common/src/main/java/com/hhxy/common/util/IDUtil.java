package com.hhxy.common.util;

import java.util.UUID;

/**
 * @since 2021/3/17 23:45
 */
public class IDUtil {

    public static String nextId() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }
}
