package com.hhxy.common.util;

import java.math.BigInteger;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.util.Enumeration;

/**
 * @since 2021/4/5 21:41
 */
public final class IPUtil {
    public static final int IP4 = 0;
    public static final int IP6 = 1;
    public static final int IP46 = 2;
    private static final int IP6INADDRSZ = 16;
    private static final int INT16SZ = 2;

    private IPUtil() {
    }

    public static BigInteger StringToBigInt(String ipInStr) {
        if (ipInStr == null) {
            return null;
        } else {
            ipInStr = ipInStr.replace(" ", "");
            byte[] bytes;
            if (ipInStr.contains(":")) {
                bytes = ipv6ToBytes(ipInStr);
            } else {
                bytes = ipv4ToBytes(ipInStr);
            }

            return new BigInteger(bytes);
        }
    }

    public static String BigIntToString(BigInteger ipInBigInt, int type) {
        byte[] bytes = ipInBigInt.toByteArray();
        byte[] unsignedBytes;
        if (type == 0) {
            unsignedBytes = new byte[4];
            if (bytes.length < 4) {
                System.arraycopy(bytes, 0, unsignedBytes, 4 - bytes.length, bytes.length);
                bytes = unsignedBytes;
            } else if (bytes.length >= 5) {
                System.arraycopy(bytes, bytes.length - 4, unsignedBytes, 0, 4);
                bytes = unsignedBytes;
            }

            return numericIp4ToTextFormat(bytes);
        } else if (type == 1) {
            return numericIp6ToTextFormat(generateIp6Bytes(bytes));
        } else if (type == 2) {
            unsignedBytes = generateIp6Bytes(bytes);
            byte[] ip4TempTypes = new byte[4];
            System.arraycopy(unsignedBytes, 12, ip4TempTypes, 0, 4);
            String ip4 = numericIp4ToTextFormat(ip4TempTypes);
            String ip6 = numericIp6ToTextFormat(unsignedBytes);
            String ip46 = ip6.substring(0, ip6.lastIndexOf(":")).trim();
            ip46 = ip46.substring(0, ip46.lastIndexOf(":") + 1).trim();
            ip46 = ip46 + ip4.substring(ip4.indexOf(47) + 1).trim();
            return ip46.substring(ip46.indexOf(47) + 1).trim().toUpperCase();
        } else {
            return null;
        }
    }

    private static byte[] generateIp6Bytes(byte[] bytes) {
        byte[] tempTypes = new byte[16];
        if (bytes.length < 16) {
            System.arraycopy(bytes, 0, tempTypes, 16 - bytes.length, bytes.length);
            bytes = tempTypes;
        } else if (bytes.length >= 17) {
            System.arraycopy(bytes, bytes.length - 16, tempTypes, 0, 16);
            bytes = tempTypes;
        }

        return bytes;
    }

    public static String BigIntToString(BigInteger ipInBigInt) {
        byte[] bytes = ipInBigInt.toByteArray();
        return bytes.length <= 5 ? BigIntToString(ipInBigInt, 0) : BigIntToString(ipInBigInt, 1);
    }

    private static byte[] copyOfRange(byte[] original, int from, int to) {
        int newLength = to - from;
        if (newLength < 0) {
            throw new IllegalArgumentException(from + " > " + to);
        } else {
            byte[] copy = new byte[newLength];
            System.arraycopy(original, from, copy, 0, Math.min(original.length - from, newLength));
            return copy;
        }
    }

    private static byte[] ipv6ToBytes(String ipv6) {
        byte[] ret = new byte[17];
        ret[0] = 0;
        int ib = 16;
        boolean comFlag = false;
        if (ipv6.startsWith(":")) {
            ipv6 = ipv6.substring(1);
        }

        String[] groups = ipv6.split(":");

        for(int ig = groups.length - 1; ig > -1; --ig) {
            if (groups[ig].contains(".")) {
                byte[] temp = ipv4ToBytes(groups[ig]);
                ret[ib--] = temp[4];
                ret[ib--] = temp[3];
                ret[ib--] = temp[2];
                ret[ib--] = temp[1];
                comFlag = true;
            } else {
                int temp;
                if ("".equals(groups[ig])) {
                    for(temp = 9 - (groups.length + (comFlag ? 1 : 0)); temp-- > 0; ret[ib--] = 0) {
                        ret[ib--] = 0;
                    }
                } else {
                    temp = Integer.parseInt(groups[ig], 16);
                    ret[ib--] = (byte)temp;
                    ret[ib--] = (byte)(temp >> 8);
                }
            }
        }

        return ret;
    }

    private static byte[] ipv4ToBytes(String ipv4) {
        byte[] ret = new byte[5];
        ret[0] = 0;
        int position1 = ipv4.indexOf(".");
        int position2 = ipv4.indexOf(".", position1 + 1);
        int position3 = ipv4.indexOf(".", position2 + 1);
        ret[1] = (byte)Integer.parseInt(ipv4.substring(0, position1));
        ret[2] = (byte)Integer.parseInt(ipv4.substring(position1 + 1, position2));
        ret[3] = (byte)Integer.parseInt(ipv4.substring(position2 + 1, position3));
        ret[4] = (byte)Integer.parseInt(ipv4.substring(position3 + 1));
        return ret;
    }

    public static boolean IsIp(String tip, String[][] myRange) {
        boolean flag = false;
        BigInteger tbig = StringToBigInt(tip);
        int rangeLength = myRange.length;

        for(int i = 0; i < rangeLength; ++i) {
            for(int j = 0; j < myRange[i].length; ++j) {
                BigInteger sbig = StringToBigInt(myRange[i][j]);
                ++j;
                BigInteger ebig = StringToBigInt(myRange[i][j]);
                if (tbig.compareTo(sbig) == 0) {
                    flag = true;
                    break;
                }

                if (tbig.compareTo(sbig) == 1 && tbig.compareTo(ebig) == -1) {
                    flag = true;
                    break;
                }
            }
        }

        return flag;
    }

    public static int getIpType(String ip) {
        if (ip != null) {
            if (ip.contains(":") && ip.contains(".")) {
                return 2;
            }

            if (ip.contains(":") && !ip.contains(".")) {
                return 1;
            }

            if (!ip.contains(":") && ip.contains(".")) {
                return 0;
            }
        }

        return -1;
    }

    public static String getHostIP() {
        String sIP = "";
        InetAddress ip = null;

        try {
            if (isWindowsOS()) {
                ip = getLocalHost();
            } else {
                boolean bFindIP = false;
                Enumeration netInterfaces = NetworkInterface.getNetworkInterfaces();

                label43:
                while(true) {
                    while(true) {
                        if (!netInterfaces.hasMoreElements() || bFindIP) {
                            break label43;
                        }

                        NetworkInterface ni = (NetworkInterface)netInterfaces.nextElement();
                        Enumeration ips = ni.getInetAddresses();

                        while(ips.hasMoreElements()) {
                            ip = (InetAddress)ips.nextElement();
                            if (ip.isSiteLocalAddress() && !ip.isLoopbackAddress() && getInetAddress(ip).indexOf(":") == -1 && getInetAddress(ip).toString().toLowerCase() != null) {
                                bFindIP = true;
                                break;
                            }
                        }
                    }
                }
            }
        } catch (Exception var6) {
            throw new RuntimeException(var6.getMessage(), var6);
        }

        if (ip != null) {
            sIP = getInetAddress(ip);
        }

        return sIP;
    }

    private static boolean isWindowsOS() {
        boolean isWindowsOS = false;
        String osName = System.getProperty("os.name");
        if (osName.toLowerCase().indexOf("windows") > -1) {
            isWindowsOS = true;
        }

        return isWindowsOS;
    }

    public static boolean isIp(String ip) {
        if (ip == null) {
            return false;
        } else {
            boolean isIp = false;
            if (ip.contains(":")) {
                isIp = RegexUtil.match("^\\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3}))|:)))(%.+)?\\s*$", ip);
            } else {
                isIp = RegexUtil.match("^(\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])$", ip);
            }

            return isIp;
        }
    }

    private static String numericIp4ToTextFormat(byte[] src) {
        return (src[0] & 255) + "." + (src[1] & 255) + "." + (src[2] & 255) + "." + (src[3] & 255);
    }

    private static String numericIp6ToTextFormat(byte[] src) {
        StringBuffer sb = new StringBuffer(39);

        for(int i = 0; i < 8; ++i) {
            sb.append(Integer.toHexString(src[i << 1] << 8 & '\uff00' | src[(i << 1) + 1] & 255));
            if (i < 7) {
                sb.append(":");
            }
        }

        return sb.toString();
    }

    private static String getInetAddress(InetAddress ip) {
        return ip.getHostAddress();
    }

    private static InetAddress getLocalHost() {
        try {
            return InetAddress.getLocalHost();
        } catch (UnknownHostException var1) {
            throw new RuntimeException(var1);
        }
    }
}
