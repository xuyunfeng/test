package com.hhxy.common.util;

import org.apache.commons.lang3.StringUtils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;

/**
 * @version 1.0
 */
public class MD5EncryptUtil {

    private static final int MD5_SALT_LENGTH = 12;

    /**
     * 
     * MD5EncryptUtil：私有构造方法，避免实例化
     *
     */
    private MD5EncryptUtil() {
    }

    /**
     * encrypt：用MD5对密码加密
     * 
     * @param message 待加密信息
     * @return <String> 加密结果<br/>
     *         message为空或空串，返回NULL
     */
    public static String encrypt(String message) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        if (StringUtils.isBlank(message)) {
            return null;
        }
        // 声明加密后的口令数组变量
        byte[] pwd = null;
        // 随机数生成器
        SecureRandom random = new SecureRandom();
        // 声明盐数组变量
        byte[] salt = new byte[MD5_SALT_LENGTH];
        // 将随机数放入盐变量中
        random.nextBytes(salt);
        // 声明消息摘要对象
        MessageDigest md = null;
        // 创建消息摘要
        md = MessageDigest.getInstance("MD5");
        // 将盐数据传入消息摘要对象
        md.update(salt);
        // 将口令的数据传给消息摘要对象
        md.update(message.getBytes("UTF-8"));
        // 获得消息摘要的字节数组
        byte[] digest = md.digest();
        // 因为要在口令的字节数组中存放盐，所以加上盐的字节长度
        pwd = new byte[digest.length + MD5_SALT_LENGTH];
        // 将盐的字节拷贝到生成的加密口令字节数组的前12个字节，以便在验证口令时取出盐
        System.arraycopy(salt, 0, pwd, 0, MD5_SALT_LENGTH);
        // 将消息摘要拷贝到加密口令字节数组从第13个字节开始的字节
        System.arraycopy(digest, 0, pwd, MD5_SALT_LENGTH, digest.length);
        // 将字节数组格式加密后的口令转化为16进制字符串格式的口令
        return ByteUtils.bytesToHex(pwd);
    }

    /**
     * encryptNotSalt：用MD5对密码加密，不使用盐
     * 
     * @param message 待加密信息
     * @return <String> 加密结果<br/>
     *         message为空或空串，返回NULL
     */
    public static String encryptNotSalt(String message) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        if (StringUtils.isBlank(message)) {
            return null;
        }
        // 声明加密后的口令数组变量
        byte[] pwd = null;
        // 声明消息摘要对象
        MessageDigest md = null;
        // 创建消息摘要
        md = MessageDigest.getInstance("MD5");
        // 将口令的数据传给消息摘要对象
        md.update(message.getBytes("UTF-8"));
        // 获得消息摘要的字节数组
        byte[] digest = md.digest();
        // 因为要在口令的字节数组中存放盐，所以加上盐的字节长度
        pwd = new byte[digest.length];
        // 将消息摘要拷贝到加密口令字节数组从第13个字节开始的字节
        System.arraycopy(digest, 0, pwd, 0, digest.length);
        // 将字节数组格式加密后的口令转化为16进制字符串格式的口令
        return ByteUtils.bytesToHex(pwd);
    }
    
    // 全局数组
    private static final String[] STRDIGITS = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d",
            "e", "f" };

    /**
     * bByte:返回形式为数字跟字符串
     * 
     * @param bByte 字节
     * @return
     */
    private static String byteToArrayString(byte bByte) {
        int iRet = bByte;
        if (iRet < 0) {
            iRet += 256;
        }
        int iD1 = iRet / 16;
        int iD2 = iRet % 16;
        return STRDIGITS[iD1] + STRDIGITS[iD2];
    }

    /**
     * bByte:转换字节数组为16进制字串
     * 
     * @param bByte 字节
     * @return
     */
    private static String byteToString(byte[] bByte) {
        StringBuffer sBuffer = new StringBuffer();
        for (int i = 0; i < bByte.length; i++) {
            sBuffer.append(byteToArrayString(bByte[i]));
        }
        return sBuffer.toString();
    }

    /**
     * encryptByJs:基于JS加密算法
     * 
     * @param message 待加密消息
     * @return <String> 加密结果<br/>
     *         message为空或空串，返回NULL
     */
    public static String encryptByJs(String message) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        if (StringUtils.isBlank(message)) {
            return null;
        }
        String result = null;
        MessageDigest md = MessageDigest.getInstance("MD5");
        // md.digest() 该函数返回值为存放哈希值结果的byte数组
        result = byteToString(md.digest(message.getBytes("UTF-8")));
        // 在垃圾回收延迟的情况下，进行记忆清楚，避免信息被窃取
        return result;
    }

    /**
     * validPassword:验证密码是否匹配. <br/>
     * md5加密密码是否匹配，加密密码带加密盐.<br/>
     * 
     * 
     * @param password 明文密码
     * @param encode   加密密码
     * @return 匹配true，不匹配false<br>
     *         password为空或可空串或者encode为空或空串，返回false
     */
    public static boolean valid(String password, String encode) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        if (StringUtils.isBlank(password) || StringUtils.isBlank(encode)) {
            return false;
        }
        // 将16进制字符串格式口令转换成字节数组
        byte[] pwdInDb = ByteUtils.hexToBytes(encode);
        // 声明盐变量
        byte[] salt = new byte[MD5_SALT_LENGTH];
        // 将盐从数据库中保存的口令字节数组中提取出来
        System.arraycopy(pwdInDb, 0, salt, 0, MD5_SALT_LENGTH);
        // 创建消息摘要对象
        MessageDigest md = MessageDigest.getInstance("MD5");
        // 将盐数据传入消息摘要对象
        md.update(salt);
        // 将口令的数据传给消息摘要对象
        md.update(password.getBytes("UTF-8"));
        // 生成输入口令的消息摘要
        byte[] digest = md.digest();
        // 声明一个保存数据库中口令消息摘要的变量
        byte[] digestInDb = new byte[pwdInDb.length - MD5_SALT_LENGTH];
        // 取得数据库中口令的消息摘要
        System.arraycopy(pwdInDb, MD5_SALT_LENGTH, digestInDb, 0, digestInDb.length);
        // 比较根据输入口令生成的消息摘要和数据库中消息摘要是否相同
        if (Arrays.equals(digest, digestInDb)) {
            // 口令正确返回口令匹配消息
            return true;
        } else {
            // 口令不正确返回口令不匹配消息
            return false;
        }
    }
}
