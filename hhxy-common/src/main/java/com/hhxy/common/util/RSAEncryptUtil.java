package com.hhxy.common.util;

import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import javax.crypto.Cipher;
import java.math.BigInteger;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;

/**
 * 
 * ClassName: RSAEncryptUtil <br/>
 * Function: 非对称加密. <br/>
 * Reason: 提供国际非对称加密方式. <br/>
 *
 * @version 1.0
 */
public class RSAEncryptUtil {

    /**
     * 
     * RSAEncryptUtil:私有构造方法，避免实例化
     *
     */
    private RSAEncryptUtil() {
    }

    /** 默认的安全服务提供者 */
    public static final Provider DEFAULT_PROVIDER = new BouncyCastleProvider();

    /**
     * encrypt:基于publicKey进行加密. <br/>
     * Reason:使用publicKey对明文进行加密.<br/>
     * 
     * @param message   加密消息
     * @param publicKey 加密公钥
     * @return <String> 加密结果
     * @throws Exception
     */
    public static String encrypt(String message, RSAPublicKey publicKey) throws Exception {
        if (StringUtils.isBlank(message) || publicKey == null) {
            return null;
        }
        try {
            Cipher cipher = Cipher.getInstance("RSA", DEFAULT_PROVIDER);
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);
            // 模长
            int key_Len = publicKey.getModulus().bitLength() / 8;
            // 加密数据长度 <= 模长-11
            String[] datas = splitString(message, key_Len - 11);
            StringBuilder mi = new StringBuilder();
            // 如果明文长度大于模长-11则要分组加密
            for (String s : datas) {
                mi.append(bcdTwoStr(cipher.doFinal(s.getBytes("UTF-8"))));
            }
            return mi.toString();
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * 
     * splitString:字符串拆分. <br/>
     * Reason:将字符串按照RSA加密需要的格式进行拆分。<br/>
     *
     * @param string 字符串长度
     * @param len    拆分长度
     * @return
     */
    public static String[] splitString(String string, int len) {
        int x = string.length() / len;
        int y = string.length() % len;
        int z = 0;
        if (y != 0) {
            z = 1;
        }
        String[] strings = new String[x + z];
        String str = "";
        for (int i = 0; i < x + z; i++) {
            if (i == x + z - 1 && y != 0) {
                str = string.substring(i * len, i * len + y);
            } else {
                str = string.substring(i * len, i * len + len);
            }
            strings[i] = str;
        }
        return strings;
    }

    /**
     * BCD转字符串
     */
    public static String bcdTwoStr(byte[] bytes) {
        char temp[] = new char[bytes.length * 2];
        char val;

        for (int i = 0; i < bytes.length; i++) {
            val = (char) (((bytes[i] & 0xf0) >> 4) & 0x0f);
            temp[i * 2] = (char) (val > 9 ? val + 'A' - 10 : val + '0');

            val = (char) (bytes[i] & 0x0f);
            temp[i * 2 + 1] = (char) (val > 9 ? val + 'A' - 10 : val + '0');
        }
        return new String(temp);
    }

    /**
     * decrypt：私钥解密
     * 
     * @param encryptCode 加密编码
     * @param privateKey  私钥
     * @return
     * @throws Exception
     */
    public static String decrypt(String encryptCode, RSAPrivateKey privateKey) throws Exception {
        if (StringUtils.isBlank(encryptCode) || privateKey == null) {
            return null;
        }
        try {
            Cipher cipher = Cipher.getInstance("RSA", DEFAULT_PROVIDER);
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            // 模长
            int key_Len = privateKey.getModulus().bitLength() / 8;
            byte[] bytes = encryptCode.getBytes("UTF-8");
            byte[] bcd = ascii_To_BCD(bytes, bytes.length);
            // 如果密文长度大于模长则要分组解密
            StringBuilder mi = new StringBuilder();
            byte[][] arrays = splitArray(bcd, key_Len);
            for (byte[] arr : arrays) {
                mi.append(new String(cipher.doFinal(arr), "UTF-8"));
            }
            return mi.toString();
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * asc码转bcd码
     * 
     * @param asc 码
     * @return
     */
    public static byte asc_TO_BCD(byte asc) {
        byte bcd;

        if ((asc >= '0') && (asc <= '9')) {
            bcd = (byte) (asc - '0');
        } else if ((asc >= 'A') && (asc <= 'F')) {
            bcd = (byte) (asc - 'A' + 10);
        } else if ((asc >= 'a') && (asc <= 'f')) {
            bcd = (byte) (asc - 'a' + 10);
        } else {
            bcd = (byte) (asc - 48);
        }
        return bcd;
    }

    /**
     * ASCII码转BCD码
     * 
     */
    public static byte[] ascii_To_BCD(byte[] ascii, int asc_Len) {
        byte[] bcd = new byte[asc_Len / 2];
        int j = 0;
        for (int i = 0; i < (asc_Len + 1) / 2; i++) {
            bcd[i] = asc_TO_BCD(ascii[j++]);
            bcd[i] = (byte) (((j >= asc_Len) ? 0x00 : asc_TO_BCD(ascii[j++])) + (bcd[i] << 4));
        }
        return bcd;
    }

    /**
     * spllitArray:拆分数组
     */
    public static byte[][] splitArray(byte[] data, int len) {
        int x = data.length / len;
        int y = data.length % len;
        int z = 0;
        if (y != 0) {
            z = 1;
        }
        byte[][] arrays = new byte[x + z][];
        byte[] arr;
        for (int i = 0; i < x + z; i++) {
            arr = new byte[len];
            if (i == x + z - 1 && y != 0) {
                System.arraycopy(data, i * len, arr, 0, y);
            } else {
                System.arraycopy(data, i * len, arr, 0, len);
            }
            arrays[i] = arr;
        }
        return arrays;
    }

    /**
     * encrypt:基于私钥加密. <br/>
     *
     * @param message    待加密消息
     * @param privateKey 加密私钥
     * @return 加密HexString<br/>
     *         message为NULL或者空串或者privateKey为空时，返回NULL
     * @throws Exception
     */
    public static String encrypt(String message, RSAPrivateKey privateKey) throws Exception {
        if (StringUtils.isBlank(message) || privateKey == null) {
            return null;
        }
        try {
            Cipher cipher = Cipher.getInstance("RSA", DEFAULT_PROVIDER);
            cipher.init(Cipher.ENCRYPT_MODE, privateKey);
            // 模长
            int key_Len = privateKey.getModulus().bitLength() / 8;
            // 加密数据长度 <= 模长-11
            String[] datas = splitString(message, key_Len - 11);
            StringBuilder mi = new StringBuilder();
            // 如果明文长度大于模长-11则要分组加密
            for (String s : datas) {
                mi.append(bcdTwoStr(cipher.doFinal(s.getBytes("UTF-8"))));
            }
            return mi.toString();
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * decrypt:公钥解密
     * 
     * @param encryptCode 加密编码
     * @param publicKey   公钥
     * @return <String>解密结果<br/>
     *         encryptCode为空或空串或者publicKey为空时，不做处理返回NULL
     * @throws Exception
     */
    public static String decrypt(String encryptCode, RSAPublicKey publicKey) throws Exception {
        if (StringUtils.isBlank(encryptCode) || publicKey == null) {
            return null;
        }
        try {
            Cipher cipher = Cipher.getInstance("RSA", DEFAULT_PROVIDER);
            cipher.init(Cipher.DECRYPT_MODE, publicKey);
            // 模长
            int key_Len = publicKey.getModulus().bitLength() / 8;
            byte[] bytes = encryptCode.getBytes("UTF-8");
            byte[] bcd = ascii_To_BCD(bytes, bytes.length);
            // 如果密文长度大于模长则要分组解密
            StringBuilder mi = new StringBuilder();
            byte[][] arrays = splitArray(bcd, key_Len);
            for (byte[] arr : arrays) {
                mi.append(new String(cipher.doFinal(arr), "UTF-8"));
            }
            return mi.toString();
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * getPublicKey:基于模与指数获取加密公钥. <br/>
     *
     * @param modulus  模
     * @param exponent 指数
     * @return {@link RSAPublicKey} 加密公钥<br/>
     *         modules为空或空串或者exponent为空或者空串时，返回NULL
     * @throws Exception
     */
    public static RSAPublicKey getPublicKey(String modulus, String exponent) throws Exception {
        if (StringUtils.isBlank(modulus) || StringUtils.isBlank(exponent)) {
            return null;
        }
        BigInteger b1 = new BigInteger(modulus);
        BigInteger b2 = new BigInteger(exponent);
        RSAPublicKeySpec keySpec = new RSAPublicKeySpec(b1, b2);
        try {
            KeyFactory keyFactory = KeyFactory.getInstance("RSA", DEFAULT_PROVIDER);
            return (RSAPublicKey) keyFactory.generatePublic(keySpec);
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * getPublicKey:基于模与指数获取加密私钥. <br/>
     *
     * @param modulus  模
     * @param exponent 指数
     * @return {@link RSAPrivateKey} 加密私钥<br/>
     *         modules为空或空串或者exponent为空或者空串时，返回NULL
     * @throws Exception
     */
    public static RSAPrivateKey getPrivateKey(String modulus, String exponent) throws Exception {
        BigInteger b1 = new BigInteger(modulus);
        BigInteger b2 = new BigInteger(exponent);
        RSAPrivateKeySpec keySpec = new RSAPrivateKeySpec(b1, b2);
        try {
            KeyFactory keyFactory = KeyFactory.getInstance("RSA", DEFAULT_PROVIDER);
            return (RSAPrivateKey) keyFactory.generatePrivate(keySpec);
        } catch (Exception e) {
            throw new Exception(e.getMessage(), e);
        }
    }

    /**
     * generateKeyPair:动态生成公私玥. <br/>
     * 用于基于参数的公私玥生成方法.<br/>
     *
     * @param businessParameter 业务参数 <b>允许为空</b>
     * @return {@link RSAKeyInfo} 公私玥存储对象<br/>
     *         允许业务参数为空
     * @throws Exception
     */
    public static RSAKeyInfo generateKeyPair(String businessParameter) throws Exception {
        // 声明RSA公钥信息对象
        RSAKeyInfo keyInfo = new RSAKeyInfo();
        String random = null;
        KeyPair keyPair = null;
        if (StringUtils.isBlank(businessParameter)) {
            random = DateUtil.formatDate(System.currentTimeMillis());
        } else {
            random = DateUtil.formatDate(System.currentTimeMillis()) + businessParameter;
        }
        try {
            KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance("RSA",
                    DEFAULT_PROVIDER);
            // 根据业务系统id、当前时间戳连接生成安全 随机数
            // 根据当前安全随机数生成公钥、私钥密码对
            keyPairGen.initialize(1024, new SecureRandom(random.getBytes("UTF-8")));

            keyPairGen.generateKeyPair();
            keyPair = keyPairGen.generateKeyPair();
        } catch (Exception e) {
            throw new Exception(e.getMessage(), e);
        }
        RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();
        RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();
        keyInfo.setPublicKey(publicKey);
        keyInfo.setPrivateKey(privateKey);
        return keyInfo;
    }

    /**
     * rsa加密算法公钥、私钥对象
     * 
     */
    public static class RSAKeyInfo {

        /**
         * 公钥值
         */
        private RSAPublicKey publicKey;
        /**
         * 私钥值
         */
        private RSAPrivateKey privateKey;

        /**
         * 公钥值get方法
         * 
         * @return
         */
        public RSAPublicKey getPublicKey() {
            return publicKey;
        }

        /**
         * 公钥值set方法
         * 
         * @param publicKey
         */
        public void setPublicKey(RSAPublicKey publicKey) {
            this.publicKey = publicKey;
        }

        /**
         * 私钥值get方法
         * 
         * @return
         */
        public RSAPrivateKey getPrivateKey() {
            return privateKey;
        }

        /**
         * 私钥值set方法
         */
        public void setPrivateKey(RSAPrivateKey privateKey) {
            this.privateKey = privateKey;
        }

    }
}
