package com.hhxy.common.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 正则表达式验证工具
 * ClassName: RegexUtil <br/>
 * Function: 用于实现正则校验. <br/>
 * Reason: 基于正则表达式的正则校验，提供基础正则表达式与通用校验方式. <br/>
 * date: 2019-1-14 下午5:49:15 <br/>
 *
 * @version
 */
public class RegexUtil {
	
	/**
	 * 手机号码
	 */
	public static final String REGEX_PHONE_NUMBER = "^(?:\\+86)?(?:13\\d|14[57]|15[0-35-9]|17[135-8]|18\\d|19[189])\\d{8}$|^(?:\\+86)?170[057-9]\\d{7}$";

	/**
	 * 查找头尾空格
	 */
	public static final String REGEX_TRIM = "^\\s+|\\s+$";
	
	/**
	 * 邮箱
	 */
	public static final String REGEX_EMAIL = "^[-\\w\\+]+(?:\\.[-\\w]+)*@[-a-zA-Z0-9]+(?:\\.[a-zA-Z0-9]+)*(?:\\.[a-zA-Z]{2,})$";
	
	/**
	 * IPV4
	 */
	public static final String REGEX_IPV4 = "^(\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])$";
	
	/**
	 * IPV6
	 */
	public static final String REGEX_IPV6 = "^\\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3}))|:)))(%.+)?\\s*$";
	
	/**
	 * 18位身份证有效验证
	 */
	public static final String REGEX_18IDCARD = "^(?:1[1-5]|2[1-3]|3[1-7]|4[1-6]|5[0-4]|6[1-5])\\d{4}(?:1[89]|20)\\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\\d|3[01])\\d{3}(?:\\d|[xX])$";
	
	/**
	 * 15位身份证有效验证
	 */
	public static final String REGEX_15IDCARD = "^(?:1[1-5]|2[1-3]|3[1-7]|4[1-6]|5[0-4]|6[1-5])\\d{4}\\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\\d|3[01])\\d{3}$";
	
	/**
	 * URL正则校验
	 */
//    public static final String REGEX_URL = "^(https?|ftp):\\/\\/(((([a-zA-Z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(%[\\da-f]{2})|[!\\$&'\\(\\)\\*\\+,;=]|:)*@)?(((\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5]))|((([a-zA-Z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(([a-zA-Z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])([a-zA-Z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])*([a-zA-Z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])))\\.)+(([a-zA-Z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(([a-zA-Z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])([a-zA-Z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])*([a-zA-Z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])))\\.?)(:\\d*)?)(\\/((([a-zA-Z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(%[\\da-f]{2})|[!\\$&'\\(\\)\\*\\+,;=]|:|@)+(\\/(([a-zA-Z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(%[\\da-f]{2})|[!\\$&'\\(\\)\\*\\+,;=]|:|@)*)*)?)?(\\?((([a-zA-Z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(%[\\da-f]{2})|[!\\$&'\\(\\)\\*\\+,;=]|:|@)|[\\uE000-\\uF8FF]|\\/|\\?)*)?(\\#((([a-zA-Z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(%[\\da-f]{2})|[!\\$&'\\(\\)\\*\\+,;=]|:|@)|\\/|\\?)*)?$";
	public static final String REGEX_URL = "^(https|http)://" // 必须以https、http开头
			+ "(((\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5]))|" // 匹配IPV4地址
			+ "((((?!-)[a-zA-Z0-9-]{1,62}(?<!-)\\.)+)[a-zA-Z]{2,63}))"  // 或者域名，域名不以-开头，中间包含字母数字下划线，并且不以下划线结尾，以.分割，可以多次重复，并且最后以字母结尾
			+ "(((:(?!0)[0-9]{1,5})?)|())"    //  IP或者域名后面紧跟端口，或者默认端口80不用写，如http://www.google.com
			+ "(()|(/)|((/(?!_\\.)[a-zA-Z0-9_\\.]+)+))$"; // 在端口后面，紧跟具体地址信息，地址信息可以为空，也可以为/，或者为具体的地址，以/开头包含_和.，并且可以多次重复
	
    /**
     * 必须为中文
     */
    public static final String REGEX_CHINESE = "^[\\u4E00-\\u9FA5]+$";
    
    /**
     * 纯英文字母
     */
    public static final String REGEX_ENGLISH = "^[a-zA-z]+$";
    
    /**
     * 字母和数字
     */
    public static final String REGEX_ENGLISH_NUM = "^[a-zA-Z0-9]+$";
    
    /**
     * 字母和数字
     */
    public static final String REGEX_ENGLISH_NUM_POINT = "^[a-zA-Z0-9.]+$";
    
    /**
     * 是否为日期格式 "yyyy-MM-dd"
     */
    public static final String REGEX_DATE_OF_DAY = "^((\\d{2}(([02468][048])|([13579][26]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])))))|(\\d{2}(([02468][1235679])|([13579][01345789]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|(1[0-9])|(2[0-8]))))))?$";
    
    /**
     * 只包含字母数字下划线，并且以字母开头
     */
    public static final String REGEX_ENGLISH_NUM_UNDERLINE = "^[a-zA-Z][a-zA-Z0-9_]*$";

	/**
	 * 只包含字母数字下划线，并且以字母开头
	 */
	public static final String REGEX_ENGLISH_NUM_COMMA = "^[a-zA-Z][a-zA-Z0-9,]*$";
    
    /**
     * 只包含字母数字下划线，并且以数字或字母开头
     */
    public static final String REGEX_NUM_ENGLISH_UNDERLINE = "^[a-zA-Z0-9][a-zA-Z0-9_]*$";
    
    /**
     * 传真号码
     */
    public static final String REGEX_FAX = "^((\\(\\d{2,3}\\))|(\\d{3}\\-))?(\\(0\\d{2,3}\\)|0\\d{2,3}-)?[1-9]\\d{6,7}(\\-\\d{1,4})?$";
    
    /**
     * 数据库表字段，包含字母+下划线，字母开头，字母结尾
     */
    public static final String REGEX_DB_FIELD = "^[a-zA-Z][a-zA-Z_]*[A-Za-z]*$";
    
    /**
     * 必须只包含中文、字母、数字
     */
    public static final String REGEX_CHINESE_ENGLISH_NUMERIC =  "^([\\u4E00-\\uFA29]|[\\uE7C7-\\uE7F3]|[a-zA-Z0-9])*$";
	
    /**
     * 数字，如123，10.123
     */
    public static final String REGEX_NUMERIC = "^(?:[1-9]\\d*|0)(?:\\.\\d+)?$";

    
    /**
     * 数据字符串，注意与{@link #REGEX_NUMERIC}的区别，前者0.1验证true，01验证false，当下01验证true，0.1验证false
     */
    public static final String REGEX_NUMBER = "^[0-9]+$";
    
    /**
     * 不包含特殊字符
     */
    public static final String REGEX_NOSPECIAL = "^[0-9a-zA-Z_\\-\\u4e00-\\u9fa5\\(\\)\\.\\,，（）]*$";
    
    /**
     * 安全授权码
     */
    public static final String REGEX_SAFE_AUTHKEY = "^.*(?=.{8,32})(?=.*\\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[\\.=!@#$%^&*?\\(\\)~_-])[0-9A-Za-z\\.=!@#\\$%\\^&\\*\\?\\(\\)~_-]*$";
    
    /**
     * 邮政编码
     */
    public static final String REGEX_POSTAL_CODE = "^[0-9]{6}$";
    
    /**
     * 反向匹配字符串不包含<>标签
     */
    public static final String REGEX_NO_HTML_TAG = "^((?!<|>).)*$";
    
    /**
     * 字母、数字、特殊字符
     */
    public static final String REGEX_ENGLISH_NUM_SPECIFICCHAR = "^[a-zA-Z0-9_\\-@\\.\\|]+$";
    
    /**
     * 中文、字母、数字、特殊字符
     */
    public static final String REGEX_CHINESE_ENGLISH_NUMERIC_SPECIFICCHAR =  "^([\\u4E00-\\uFA29]|[\\uE7C7-\\uE7F3]|[a-zA-Z0-9_\\-@\\.\\|])*$";
    
	/**
	 * 构造函数
	 */
	private RegexUtil() {
	}
	

	/**
	 * getMatcher:获取匹配器. <br/>
	 *
	 * @param regex
	 * @param str
	 * @return
	 */
	private static Matcher getMatcher(String regex, String str) {
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(str);
		return matcher;
	}

	/**
	 * 对整个字符串进行匹配，只有整个字符串都匹配了才返回true
	 * 
	 * @param regex
	 *            正则表达式字符串
	 * @param str
	 *            要匹配的字符串
	 * @return 如果str 符合 regex的正则表达式格式,返回true, 否则返回 false;
	 */
	public static boolean match(String regex, String str) {
		Matcher matcher =getMatcher(regex, str);
		if (matcher == null) {
			return false;
		}
		return matcher.matches();
	}

	/**
	 * 对字符串进行匹配，匹配到的字符串可以在任何位置。
	 * 
	 * @param regex
	 *            正则表达式字符串
	 * @param str
	 *            要匹配的字符串
	 * @return 如果str 符合 regex的正则表达式格式,返回true, 否则返回 false;
	 */
	public static boolean find(String regex, String str) {
		Matcher matcher =getMatcher(regex, str);
		if (matcher == null) {
			Pattern pattern = Pattern.compile(regex);
			matcher = pattern.matcher(str);
		}
		return matcher.find();
	}
	
	/**
	 * lookingAt:对前面的字符串进行匹配,只有匹配到的字符串在最前面才返回true. <br/>
	 *
	 * @param regex 正则表达式字符串
	 * @param str 要匹配的字符串
	 * @return 如果str 符合 regex的正则表达式格式,返回true, 否则返回 false;
	 */
	public static boolean lookingAt(String regex, String str) {
		Matcher matcher =getMatcher(regex, str);
		if (matcher == null) {
			return false;
		}
		return matcher.lookingAt();
	}
	
	/**
	 * isChinese:必须为纯中文. <br/>
	 *
	 * @param value
	 * @return
	 */
	public static boolean isChinese(String value) {
		return match(REGEX_CHINESE, value);
	}
	
	/**
	 * isPhoneNumber:是手机号. <br/>
	 *
	 * @param value
	 * @return
	 */
	public static boolean isPhoneNumber(String value) {
		return match(REGEX_PHONE_NUMBER, value);
	}
	
	/**
	 * isEmail:是邮箱地址. <br/>
	 *

	 * @param value
	 * @return
	 */
	public static boolean isEmail(String value) {
		return match(REGEX_EMAIL, value);
	}
	
	/**
	 * isIPV4:是IPV4. <br/>
	 *

	 * @param value
	 * @return
	 */
	public static boolean isIPV4(String value) {
		return match(REGEX_IPV4, value);
	}
	
	/**
	 * isIPV6:是IPV6. <br/>
	 *

	 * @param value
	 * @return
	 */
	public static boolean isIPV6(String value) {
		return match(REGEX_IPV6, value);
	}
	
	/**
	 * isUrl:是URL地址. <br/>
	 *

	 * @param value
	 * @return
	 */
	public static boolean isUrl(String value) {
		return match(REGEX_URL, value);
	}
	
	/**
	 * isEnglish:是纯英文字母. <br/>
	 *
	 * @param value
	 * @return
	 */
	public static boolean isEnglish(String value) {
		return match(REGEX_ENGLISH, value);
	}
	
	/**
	 * isEnglishNum:是纯英文字母+数字. <br/>
	 *

	 * @param value
	 * @return
	 */
	public static boolean isEnglishNum(String value) {
		return match(REGEX_ENGLISH_NUM, value);
	}
	
	/**
	 * isEnglishNumPoint:是纯英文字母+数字+点. <br/>
	 *

	 * @param value
	 * @return
	 */
	public static boolean isEnglishNumPoint(String value) {
		return match(REGEX_ENGLISH_NUM_POINT, value);
	}
	
	/**
	 * isDateOfDay:是日期格式"yyyy-MM-dd". <br/>
	 *

	 * @param value
	 * @return
	 */
	public static boolean isDateOfDay(String value) {
		return match(REGEX_DATE_OF_DAY, value);
	}
	
	/**
	 * isEnglishNumUnderline:只包含字母数字下划线，并且以字母开头. <br/>
	 *

	 * @param value
	 * @return
	 */
	public static boolean isEnglishNumUnderline(String value) {
		return match(REGEX_ENGLISH_NUM_UNDERLINE, value);
	}
	
	/**
	 * isFax:传真号码. <br/>
	 *

	 * @param value
	 * @return
	 */
	public static boolean isFax(String value) {
		return match(REGEX_FAX, value);
	}
	
	/**
	 * isDBField:数据库表字段，包含字母+下划线，字母开头，字母结尾. <br/>
	 *

	 * @param value
	 * @return
	 */
	public static boolean isDBField(String value) {
		return match(REGEX_DB_FIELD, value);
	}
	
	/**
	 * containHtmlTag:包含html标签. <br/>
	 *

	 * @param value
	 * @return
	 */
	public static boolean containHtmlTag(String value) {
		return find(REGEX_NO_HTML_TAG, value);
	}
	
	/**
	 * isChineseEnglishNum:只包含中文、字母、数字. <br/>
	 *

	 * @param value
	 * @return
	 */
	public static boolean isChineseEnglishNum(String value) {
		return find(REGEX_CHINESE_ENGLISH_NUMERIC, value);
	}
	
	/**
	 * 身份证验证乘数常量
	 */
	private static final int[] IDCARD_MULTIPLIER = {7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2};
	
	/**
	 * 身份证验证的校验码
	 */
	private static final String[] IDCARD_CHECK_CODE = {"1", "0", "X", "9", "8", "7", "6", "5", "4", "3", "2"};
	
	/**
	 * isIDCard:判断是否为18位身份证码. <br/>
	 *

	 * @param value
	 * @return
	 */
	public static boolean isIDCard18(String value){
		if (!match(REGEX_18IDCARD, value)) {
			return false;
		}
		String year = value.substring(6, 10);
		String month = value.substring(10, 12);
		String day = value.substring(12, 14);
		if(match(REGEX_DATE_OF_DAY, year + "-" + month + "-" + day)) {
			int sum = 0;
			for (int i = 0; i < 17; i++) {
				sum +=Integer.parseInt(value.substring(i, i+1)) * IDCARD_MULTIPLIER[i];
			}
			int remainder = sum % 11;
			return String.valueOf(value.charAt(17)).equals(IDCARD_CHECK_CODE[remainder]);
		}
		return false;
	}
	
	/**
	 * isIPPort:是否是ip:port格式. <br/>
	 *

	 * @param value 待校验值
	 * @return
	 */
	public static boolean isIPPort(String value) {
		String[] ipPort = value.split(":");
		if (ipPort.length != 2) {
			return false;
		}
		Integer port = 0;
		try {
			port = Integer.parseInt(ipPort[1]);
		} catch (Exception e) {
			return false;
		}
		if (match(REGEX_IPV4, ipPort[0]) && (port > 0 && port < 70000)) {
			return true;
		}
		return false;
	}
	
	/**
	 * isNumeric:是数字. <br/>
	 * 如123,0.123.<br/>
	 *

	 * @param value
	 * @return
	 */
	public static boolean isNumeric(String value) {
		return match(REGEX_NUMERIC, value);
	}
	
	/**
	 * isNumber:是数字字符串. <br/>
	 * 注意与{@link RegexUtil#isNumeric(String)}的区别，前者校验01失败0.1成功，当前校验01成功0.1失败.<br/>
	 *

	 * @param value
	 * @return
	 */
	public static boolean isNumber(String value) {
		return match(REGEX_NUMBER, value);
	}
	
	/**
	 * isNoSpecial:不包含特殊字符. <br/>
	 * 自包含数字字母汉子.<br/>
	 *

	 * @param value
	 * @return
	 */
	public static boolean isNoSpecial(String value) {
		return match(REGEX_NOSPECIAL, value);
	}
	
	/**
	 * isSafeAuthKey:是否安全的授权码. <br/>
	 * 输入的内容必须是安全的密码字符(由字符和数字组成，至少8位)格式.<br/>
	 *

	 * @param value
	 * @return
	 */
	public static boolean isSafeAuthKey(String value) {
		return match(REGEX_SAFE_AUTHKEY, value);
	}
}