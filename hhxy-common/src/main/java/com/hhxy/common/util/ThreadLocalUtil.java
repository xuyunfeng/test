package com.hhxy.common.util;

import com.hhxy.common.cache.impl.ThreadLocalCacheServiceImpl;
import com.hhxy.common.constants.CommonConstants;
import com.hhxy.common.entity.User;
import com.hhxy.common.enums.ResultMessageEnum;
import com.hhxy.common.exception.HhxyException;

/**
 * @since 2021/5/14 22:43
 */
public class ThreadLocalUtil {

    /**
     * 获取本地线程变量中用户
     * @return
     */
    public static Long getUserId() {
        User user = ThreadLocalCacheServiceImpl.getIntance().get(CommonConstants.THREAD_LOCAL_USER_KEY, User.class);
        if (user == null) {
            throw new HhxyException(ResultMessageEnum.USER_IS_NULL);
        }
        return user.getId();
    }

    public static void setUser(User user) {
        if (user == null) {
            return;
        }
        ThreadLocalCacheServiceImpl.getIntance().add(CommonConstants.THREAD_LOCAL_USER_KEY, user);
    }

    public static void clean() {
        ThreadLocalCacheServiceImpl.getIntance().delete(CommonConstants.THREAD_LOCAL_USER_KEY);
    }
}
