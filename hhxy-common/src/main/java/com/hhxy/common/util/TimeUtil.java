package com.hhxy.common.util;

import java.util.concurrent.TimeUnit;

/**
 * @since 2021/3/18 23:21
 */
public class TimeUtil {

    /**
     * 支持时分秒毫秒转换成秒
     * @param time
     * @param timeUnit
     * @return
     */
    public static long seconds(int time, TimeUnit timeUnit) {
        if (TimeUnit.SECONDS.equals(timeUnit)) {
            return time;
        } else if (TimeUnit.MINUTES.equals(timeUnit)) {
            return time * 60;
        } else if (TimeUnit.MILLISECONDS.equals(timeUnit)) {
            return time / 1000;
        } else if (TimeUnit.HOURS.equals(timeUnit)) {
            return time * 60 * 60;
        } else if (TimeUnit.DAYS.equals(timeUnit)) {
            return time * 60 * 60 * 24;
        } else {
            return time;
        }
    }

    /**
     * 支持时分秒毫秒转换
     * @param time
     * @param timeUnit
     * @return
     */
    public static long minutes(int time, TimeUnit timeUnit) {
        if (TimeUnit.SECONDS.equals(timeUnit)) {
            return time / 60;
        } else if (TimeUnit.MINUTES.equals(timeUnit)) {
            return time;
        } else if (TimeUnit.MILLISECONDS.equals(timeUnit)) {
            return time/ 60 / 1000;
        } else if (TimeUnit.HOURS.equals(timeUnit)) {
            return time * 60;
        } else if (TimeUnit.DAYS.equals(timeUnit)) {
            return time * 60 * 24;
        } else {
            return time;
        }
    }

    /**
     * 支持时分秒毫秒转换
     * @param time
     * @param timeUnit
     * @return
     */
    public static long hours(int time, TimeUnit timeUnit) {
        if (TimeUnit.SECONDS.equals(timeUnit)) {
            return time / 60 / 60;
        } else if (TimeUnit.MINUTES.equals(timeUnit)) {
            return time / 60;
        } else if (TimeUnit.MILLISECONDS.equals(timeUnit)) {
            return time/ 60/ 60 / 1000;
        } else if (TimeUnit.HOURS.equals(timeUnit)) {
            return time;
        } else if (TimeUnit.DAYS.equals(timeUnit)) {
            return time * 24;
        } else {
            return time;
        }
    }
}
