package com.hhxy.common.vo;

import com.hhxy.common.enums.ResultMessageEnum;
import lombok.Data;
import org.springframework.util.Assert;

/**
 * @author legao
 */
@Data
public class ResultVo<T> {
    private int code;
    private String message;
    private T data;

    public ResultVo() {
        this.code = 200;
    }

    public ResultVo(int code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public static final <T> ResultVo<T> success() {
        return new ResultVo<>();
    }

    public static final <T> ResultVo<T> success(T data) {
        return new ResultVo<>(200, null, data);
    }

    public static final ResultVo failed(ResultMessageEnum msg) {
        Assert.notNull(msg, "状态及消息不能为空");
        return new ResultVo(msg.getCode(), msg.getMessage(), null);
    }
}
