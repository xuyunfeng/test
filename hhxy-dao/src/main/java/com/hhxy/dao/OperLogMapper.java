package com.hhxy.dao;

import com.hhxy.common.entity.OperLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hhxy
 * @since 2021-05-15
 */
public interface OperLogMapper extends BaseMapper<OperLog> {

}
