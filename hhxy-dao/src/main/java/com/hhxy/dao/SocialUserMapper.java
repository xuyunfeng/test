package com.hhxy.dao;

import com.hhxy.common.entity.SocialUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hhxy
 * @since 2021-05-15
 */
public interface SocialUserMapper extends BaseMapper<SocialUser> {

}
