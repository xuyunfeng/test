package com.hhxy.dao;

import com.hhxy.common.entity.SysLearningConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hhxy
 * @since 2021-05-21
 */
public interface SysLearningConfigMapper extends BaseMapper<SysLearningConfig> {

}
