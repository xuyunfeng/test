package com.hhxy.dao;

import com.hhxy.common.entity.SysPushRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hhxy
 * @since 2021-05-14
 */
public interface SysPushRecordMapper extends BaseMapper<SysPushRecord> {

}
