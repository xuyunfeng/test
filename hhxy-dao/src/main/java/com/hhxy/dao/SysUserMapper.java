package com.hhxy.dao;

import com.hhxy.common.entity.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hhxy
 * @since 2021-05-12
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

}
