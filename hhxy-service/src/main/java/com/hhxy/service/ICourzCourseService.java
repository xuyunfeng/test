package com.hhxy.service;

import com.hhxy.common.entity.CourzCourse;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hhxy
 * @since 2021-05-14
 */
public interface ICourzCourseService extends IService<CourzCourse> {

}
