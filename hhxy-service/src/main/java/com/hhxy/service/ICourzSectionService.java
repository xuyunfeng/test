package com.hhxy.service;

import com.hhxy.common.entity.CourzSection;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hhxy
 * @since 2021-05-14
 */
public interface ICourzSectionService extends IService<CourzSection> {

}
