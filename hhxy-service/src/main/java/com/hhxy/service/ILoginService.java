package com.hhxy.service;

import com.hhxy.common.entity.User;
import com.hhxy.common.req.ReqLogin;

/**
 * @since 2021/5/12 22:42
 */
public interface ILoginService {

    /**
     * 登陆接口
     * @param login
     * @return
     * @throws Exception
     */
    User login(ReqLogin login) throws Exception;

    /**
     * 构建登录类
     * @param reqLogin
     * @return
     */
    ReqLogin build(ReqLogin reqLogin);
}
