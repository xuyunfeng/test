package com.hhxy.service;

import com.hhxy.common.entity.OperLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hhxy
 * @since 2021-05-15
 */
public interface IOperLogService extends IService<OperLog> {

    void saveOperLog(OperLog log);
}
