package com.hhxy.service;

import com.hhxy.common.entity.SocialUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hhxy
 * @since 2021-05-15
 */
public interface ISocialUserService extends IService<SocialUser> {

    /**
     * 基于登录名获取用户
     * @param userName
     * @return
     */
    SocialUser queryByUserName(String userName);

    /**
     * 删除宝宝档案信息
     * @param user
     */
    void deleteBabyInfo(SocialUser user);
}
