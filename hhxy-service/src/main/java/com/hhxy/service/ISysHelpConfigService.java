package com.hhxy.service;

import com.hhxy.common.entity.SysHelpConfig;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hhxy
 * @since 2021-05-12
 */
public interface ISysHelpConfigService extends IService<SysHelpConfig> {

}
