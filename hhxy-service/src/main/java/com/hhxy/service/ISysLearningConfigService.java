package com.hhxy.service;

import com.hhxy.common.entity.SysLearningConfig;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hhxy
 * @since 2021-05-21
 */
public interface ISysLearningConfigService extends IService<SysLearningConfig> {

    /**
     * 根据配置名查询配置信息
     * @param configName
     * @return
     */
    SysLearningConfig query(String configName);
}
