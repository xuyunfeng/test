package com.hhxy.service;

import com.hhxy.common.entity.SysRole;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hhxy
 * @since 2021-05-14
 */
public interface ISysRoleService extends IService<SysRole> {

    /**
     * 判断角色名是否已存在
     * @param name
     * @return
     */
    List<SysRole> getByName(String name);
}
