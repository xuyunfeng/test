package com.hhxy.service;

import com.hhxy.common.entity.SysUserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hhxy
 * @since 2021-05-14
 */
public interface ISysUserRoleService extends IService<SysUserRole> {

}
