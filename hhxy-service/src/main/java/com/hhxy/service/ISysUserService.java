package com.hhxy.service;

import com.hhxy.common.entity.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hhxy
 * @since 2021-05-12
 */
public interface ISysUserService extends IService<SysUser> {

    /**
     * 根据用户名获取用户
     * @param userName
     * @return
     */
    SysUser queryByUserName(String userName);
}
