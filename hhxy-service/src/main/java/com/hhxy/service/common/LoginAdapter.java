package com.hhxy.service.common;

import com.hhxy.common.entity.SocialUser;
import com.hhxy.common.entity.SysUser;
import com.hhxy.common.entity.User;
import com.hhxy.common.enums.ResultMessageEnum;
import com.hhxy.common.exception.HhxyException;
import com.hhxy.common.req.ReqLogin;
import com.hhxy.service.ILoginService;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @since 2021/5/12 23:07
 */
@Component
public class LoginAdapter implements ApplicationContextAware {

    private List<ILoginService> loginHandlers = new ArrayList<>(3);

    private LoginAdapter(){
        adapter = this;
    }

    private static LoginAdapter adapter;

    public static LoginAdapter getAdapter() {
        return adapter;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        Map<String, ILoginService> beans = applicationContext.getBeansOfType(ILoginService.class);
        if (beans != null && !beans.isEmpty()) {
            for (String key : beans.keySet()) {
                loginHandlers.add(beans.get(key));
            }
        }
    }

    /**
     * 登录处理接口
     * @param login
     * @return
     * @throws Exception
     */
    public User handler(ReqLogin login) throws Exception {
        if (CollectionUtils.isEmpty(loginHandlers)) {
            throw new HhxyException(ResultMessageEnum.LOGIN_HANDLER_NULL);
        }
        for (ILoginService service : loginHandlers) {
            User user = service.login(login);
            if (user != null) {
                return user;
            }
        }
        throw new HhxyException(ResultMessageEnum.LOGIN_UNABLE_HANDLER);
    }

    public ReqLogin build(ReqLogin login) {
        if (CollectionUtils.isEmpty(loginHandlers)) {
            throw new HhxyException(ResultMessageEnum.LOGIN_HANDLER_NULL);
        }
        for (ILoginService service : loginHandlers) {
            ReqLogin reqLogin = service.build(login);
            if (reqLogin != null) {
                return reqLogin;
            }
        }
        throw new HhxyException(ResultMessageEnum.LOGIN_UNABLE_HANDLER);
    }
}
