package com.hhxy.service.impl;

import com.hhxy.common.entity.CourzAppraise;
import com.hhxy.dao.CourzAppraiseMapper;
import com.hhxy.service.ICourzAppraiseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hhxy
 * @since 2021-05-14
 */
@Service
public class CourzAppraiseServiceImpl extends ServiceImpl<CourzAppraiseMapper, CourzAppraise> implements ICourzAppraiseService {

}
