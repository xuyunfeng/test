package com.hhxy.service.impl;

import com.hhxy.common.entity.CourzChapter;
import com.hhxy.dao.CourzChapterMapper;
import com.hhxy.service.ICourzChapterService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hhxy
 * @since 2021-05-14
 */
@Service
public class CourzChapterServiceImpl extends ServiceImpl<CourzChapterMapper, CourzChapter> implements ICourzChapterService {

}
