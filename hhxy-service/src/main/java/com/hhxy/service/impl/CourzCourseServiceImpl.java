package com.hhxy.service.impl;

import com.hhxy.common.entity.CourzCourse;
import com.hhxy.dao.CourzCourseMapper;
import com.hhxy.service.ICourzCourseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hhxy
 * @since 2021-05-14
 */
@Service
public class CourzCourseServiceImpl extends ServiceImpl<CourzCourseMapper, CourzCourse> implements ICourzCourseService {

}
