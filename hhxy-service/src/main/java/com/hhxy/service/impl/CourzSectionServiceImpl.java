package com.hhxy.service.impl;

import com.hhxy.common.entity.CourzSection;
import com.hhxy.dao.CourzSectionMapper;
import com.hhxy.service.ICourzSectionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hhxy
 * @since 2021-05-14
 */
@Service
public class CourzSectionServiceImpl extends ServiceImpl<CourzSectionMapper, CourzSection> implements ICourzSectionService {

}
