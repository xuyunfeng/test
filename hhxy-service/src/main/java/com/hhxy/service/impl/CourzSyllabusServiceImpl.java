package com.hhxy.service.impl;

import com.hhxy.common.entity.CourzSyllabus;
import com.hhxy.dao.CourzSyllabusMapper;
import com.hhxy.service.ICourzSyllabusService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hhxy
 * @since 2021-05-14
 */
@Service
public class CourzSyllabusServiceImpl extends ServiceImpl<CourzSyllabusMapper, CourzSyllabus> implements ICourzSyllabusService {

}
