package com.hhxy.service.impl;

import com.hhxy.common.entity.OperLog;
import com.hhxy.dao.OperLogMapper;
import com.hhxy.service.IOperLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hhxy
 * @since 2021-05-15
 */
@Service
public class OperLogServiceImpl extends ServiceImpl<OperLogMapper, OperLog> implements IOperLogService {

    @Autowired
    private OperLogMapper logMapper;

    @Override
    public void saveOperLog(OperLog log) {
        save(log);
    }
}
