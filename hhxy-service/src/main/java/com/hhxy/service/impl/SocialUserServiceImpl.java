package com.hhxy.service.impl;

import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.conditions.update.UpdateChainWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hhxy.common.entity.SocialUser;
import com.hhxy.dao.SocialUserMapper;
import com.hhxy.service.ISocialUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hhxy
 * @since 2021-05-15
 */
@Service
public class SocialUserServiceImpl extends ServiceImpl<SocialUserMapper, SocialUser> implements ISocialUserService {

    @Autowired
    private SocialUserMapper userMapper;

    @Override
    public SocialUser queryByUserName(String userName) {
        return new LambdaQueryChainWrapper<SocialUser>(userMapper).eq(SocialUser::getUserName, userName).one();
    }

        @Override
    public void deleteBabyInfo(SocialUser user) {
        UpdateChainWrapper<SocialUser> updateChainWrapper = new UpdateChainWrapper<>(userMapper);
        updateChainWrapper.eq("id", user.getId());
        updateChainWrapper.set("babyName", null);
        updateChainWrapper.set("babySex", null);
        updateChainWrapper.set("babyBirthday", null);
        updateChainWrapper.set("babyAge", null);
        updateChainWrapper.set("babyGrade", null);
        this.update(updateChainWrapper);
    }
}
