package com.hhxy.service.impl;

import com.hhxy.common.entity.SysHelpConfig;
import com.hhxy.dao.SysHelpConfigMapper;
import com.hhxy.service.ISysHelpConfigService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hhxy
 * @since 2021-05-12
 */
@Service
public class SysHelpConfigServiceImpl extends ServiceImpl<SysHelpConfigMapper, SysHelpConfig> implements ISysHelpConfigService {

}
