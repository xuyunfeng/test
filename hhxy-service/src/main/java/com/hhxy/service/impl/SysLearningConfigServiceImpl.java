package com.hhxy.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.hhxy.common.entity.SysLearningConfig;
import com.hhxy.dao.SysLearningConfigMapper;
import com.hhxy.service.ISysLearningConfigService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hhxy
 * @since 2021-05-21
 */
@Service
public class SysLearningConfigServiceImpl extends ServiceImpl<SysLearningConfigMapper, SysLearningConfig> implements ISysLearningConfigService {

    @Autowired
    private SysLearningConfigMapper mapper;

    @Override
    public SysLearningConfig query(String configName) {
        if (StringUtils.isBlank(configName)) {
            return null;
        }
        return new LambdaQueryChainWrapper<SysLearningConfig>(mapper).eq(SysLearningConfig::getConfigLearningName, configName.toLowerCase()).one();
    }

}
