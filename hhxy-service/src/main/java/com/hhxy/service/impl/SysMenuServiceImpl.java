package com.hhxy.service.impl;

import com.hhxy.common.entity.SysMenu;
import com.hhxy.dao.SysMenuMapper;
import com.hhxy.service.ISysMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hhxy
 * @since 2021-05-14
 */
@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements ISysMenuService {

}
