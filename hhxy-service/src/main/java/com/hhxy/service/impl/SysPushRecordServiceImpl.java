package com.hhxy.service.impl;

import com.hhxy.common.entity.SysPushRecord;
import com.hhxy.dao.SysPushRecordMapper;
import com.hhxy.service.ISysPushRecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hhxy
 * @since 2021-05-14
 */
@Service
public class SysPushRecordServiceImpl extends ServiceImpl<SysPushRecordMapper, SysPushRecord> implements ISysPushRecordService {

}
