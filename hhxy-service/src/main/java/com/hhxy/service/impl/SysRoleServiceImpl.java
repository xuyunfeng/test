package com.hhxy.service.impl;

import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.hhxy.common.entity.SysRole;
import com.hhxy.common.entity.SysRoleMenu;
import com.hhxy.dao.SysRoleMapper;
import com.hhxy.service.ISysRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hhxy
 * @since 2021-05-14
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService {

    @Autowired
    private SysRoleMapper roleMapper;

    @Override
    public List<SysRole> getByName(String name) {
        return new LambdaQueryChainWrapper<SysRole>(roleMapper).eq(SysRole::getRoleName, name).list();
    }

}
