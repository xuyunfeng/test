package com.hhxy.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.hhxy.common.entity.SocialUser;
import com.hhxy.common.entity.User;
import com.hhxy.common.enums.ResultMessageEnum;
import com.hhxy.common.exception.HhxyException;
import com.hhxy.common.req.ReqAuthKeyLogin;
import com.hhxy.common.req.ReqLogin;
import com.hhxy.service.ILoginService;
import com.hhxy.service.ISocialUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @since 2021/5/21 21:52
 */
@Service("userPasswordServiceImpl")
public class UserPasswordServiceImpl implements ILoginService {

    @Autowired
    private ISocialUserService userService;

    @Override
    public User login(ReqLogin login) throws Exception {
        ReqAuthKeyLogin authKeyLogin = (ReqAuthKeyLogin) login;
        if (!"9527".equals(authKeyLogin.getPassword())) {
            throw new HhxyException(ResultMessageEnum.USER_PD_ERR);
        }
        SocialUser user = userService.queryByUserName(authKeyLogin.getLoginName());
        if (user == null) {
            user = new SocialUser();
            user.setUserName(authKeyLogin.getLoginName());
            user.setNickName(authKeyLogin.getLoginName());
            user.setCreateTime(System.currentTimeMillis());
        }
        user.setLastVisitTime(System.currentTimeMillis());
        userService.saveOrUpdate(user);
        return user.cloneSelf();
    }

    @Override
    public ReqLogin build(ReqLogin reqLogin) {
        if (reqLogin.getLoginType() == 1) {
            return JSONObject.parseObject(reqLogin.getDataJson(), ReqAuthKeyLogin.class);
        }
        return null;
    }
}
