package com.hhxy.service.impl;

import com.hhxy.common.entity.WordsCharacter;
import com.hhxy.dao.WordsCharacterMapper;
import com.hhxy.service.IWordsCharacterService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hhxy
 * @since 2021-05-14
 */
@Service
public class WordsCharacterServiceImpl extends ServiceImpl<WordsCharacterMapper, WordsCharacter> implements IWordsCharacterService {

}
