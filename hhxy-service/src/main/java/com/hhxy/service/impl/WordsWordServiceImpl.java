package com.hhxy.service.impl;

import com.hhxy.common.entity.WordsWord;
import com.hhxy.dao.WordsWordMapper;
import com.hhxy.service.IWordsWordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hhxy
 * @since 2021-05-14
 */
@Service
public class WordsWordServiceImpl extends ServiceImpl<WordsWordMapper, WordsWord> implements IWordsWordService {

}
