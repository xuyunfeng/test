package com.hhxy;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @since 2021/5/13 19:32
 */
@SpringBootApplication(scanBasePackages = {"com.hhxy"})
@MapperScan(basePackages = {"com.hhxy.dao"}, sqlSessionFactoryRef = "sqlSessionFactory")
public class HhxyApplication {
    public static void main(String[] args) {
        SpringApplication.run(HhxyApplication.class, args);
    }
}
