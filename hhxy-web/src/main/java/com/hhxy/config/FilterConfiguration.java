package com.hhxy.config;

import com.hhxy.filter.AuthFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

/**
 * @since 2021/3/25 22:53
 */
@Configuration
public class FilterConfiguration {

    @Bean
    public FilterRegistrationBean sgIdAuditFilter() {
        AuthFilter auditFilter = new AuthFilter();
        FilterRegistrationBean registrationBean = new FilterRegistrationBean(
                auditFilter);
        List<String> urlPatterns = new ArrayList<String>();
        urlPatterns.add("/*");
        registrationBean.setUrlPatterns(urlPatterns);
        registrationBean.setOrder(800);
        registrationBean.setName("authFilter");
        return registrationBean;

    }
}
