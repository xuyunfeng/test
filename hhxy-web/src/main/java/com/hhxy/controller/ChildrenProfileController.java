package com.hhxy.controller;

import com.hhxy.common.entity.SocialUser;
import com.hhxy.common.enums.ResultMessageEnum;
import com.hhxy.common.exception.HhxyException;
import com.hhxy.common.req.ReqBabyInfo;
import com.hhxy.common.resp.RespBabyInfo;
import com.hhxy.common.util.ThreadLocalUtil;
import com.hhxy.common.vo.ResultVo;
import com.hhxy.service.ISocialUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @since 2021/5/16 22:27
 */
@RestController
@Api(tags = "宝宝信息管理")
@RequestMapping("baby")
public class ChildrenProfileController {

    @Autowired
    private ISocialUserService userService;

    @RequestMapping(value = "/profile", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "updateInfo", notes = "更新宝宝信息")
    public ResultVo<Boolean> updateInfo(HttpServletRequest request, @RequestBody ReqBabyInfo babyInfo) {
        Long userId = ThreadLocalUtil.getUserId();
        SocialUser user = userService.getById(userId);
        if (user == null) {
            throw new HhxyException(ResultMessageEnum.USER_NOT_EXIST);
        }
        user.setBabyName(babyInfo.getBabyName());
        user.setBabyBirthday(babyInfo.getBabyBirthday());
        user.setBabySex(babyInfo.getBabySex());
        user.setBabyAge(user.getAge(babyInfo.getBabyBirthday()));
        user.setBabyGrade(babyInfo.getBabyGrade());
        userService.updateById(user);
        return ResultVo.success(true);
    }

    @RequestMapping(value = "/query", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "query", notes = "获取宝宝信息")
    public ResultVo<RespBabyInfo> query(HttpServletRequest request) {
        Long userId = ThreadLocalUtil.getUserId();
        SocialUser user = userService.getById(userId);
        if (user == null) {
            throw new HhxyException(ResultMessageEnum.USER_NOT_EXIST);
        }
        return ResultVo.success(user.buildBabyInfo());
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "delete", notes = "删除宝宝档案信息")
    public ResultVo<Boolean> delete(HttpServletRequest request) {
        Long userId = ThreadLocalUtil.getUserId();
        SocialUser user = userService.getById(userId);
        if (user == null) {
            throw new HhxyException(ResultMessageEnum.USER_NOT_EXIST);
        }
        userService.deleteBabyInfo(user);
        return ResultVo.success(true);
    }
}
