package com.hhxy.controller;

import com.hhxy.common.entity.SysLearningConfig;
import com.hhxy.common.enums.ResultMessageEnum;
import com.hhxy.common.exception.HhxyException;
import com.hhxy.common.req.ReqSystemConfig;
import com.hhxy.common.req.ReqSystemConfigQuery;
import com.hhxy.common.resp.RespSysConfig;
import com.hhxy.common.util.ThreadLocalUtil;
import com.hhxy.common.vo.ResultVo;
import com.hhxy.service.ISysLearningConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @since 2021/5/21 21:03
 */
@RestController
@RequestMapping("/sys")
@Api(tags = "系统配置")
public class ConfigController {

    @Autowired
    private ISysLearningConfigService configService;

    @RequestMapping(value = "/config", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "config", notes = "设置系统配置信息")
    public ResultVo<Boolean> config(HttpServletRequest request, @RequestBody ReqSystemConfig systemConfig) {
        SysLearningConfig config = configService.query(systemConfig.getConfigLearningName());
        if (config == null) {
            config = new SysLearningConfig();
        }
        systemConfig.build(config, config.getConfigLearningId() == null);
        // 更新配置信息
        config.setConfigLearningName(systemConfig.getConfigLearningName());
        config.setConfigValue(Integer.valueOf(systemConfig.getConfigValue()));
        configService.saveOrUpdate(config);
        return ResultVo.success(true);
    }

    @RequestMapping(value = "/query", method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "query", notes = "查询系统配置信息")
    public ResultVo<RespSysConfig> query(HttpServletRequest request, @RequestBody ReqSystemConfigQuery systemConfig) {
        SysLearningConfig config = configService.query(systemConfig.getConfigLearningName());
        if (config == null) {
            throw new HhxyException(ResultMessageEnum.SYS_CONFIG_NOT_EXISTS);
        }
        return ResultVo.success(config.buildResp());
    }
}
