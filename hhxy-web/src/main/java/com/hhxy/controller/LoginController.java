package com.hhxy.controller;

import com.hhxy.common.entity.User;
import com.hhxy.common.req.ReqLogin;
import com.hhxy.common.resp.RespLoginUser;
import com.hhxy.common.token.IToken;
import com.hhxy.common.vo.ResultVo;
import com.hhxy.service.IOperLogService;
import com.hhxy.service.common.LoginAdapter;
import com.hhxy.util.TokenFactory;
import com.hhxy.util.WebUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @since 2021/5/13 19:34
 */
@RestController
@Api(tags = "登陆")
public class LoginController {

    @Autowired
    private IOperLogService logService;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ApiOperation(value = "login", notes = "手机短信登陆")
    public ResultVo<RespLoginUser> login(HttpServletRequest request, @RequestBody ReqLogin login) throws Exception {
        ReqLogin reqLogin = LoginAdapter.getAdapter().build(login);
        User user = LoginAdapter.getAdapter().handler(reqLogin);
        // 构建返回用户信息
        RespLoginUser loginUser = new RespLoginUser().build(user);
        // 构建token信息
        IToken accessToken = TokenFactory.getInstance().build(user);
        IToken refreshToken = TokenFactory.getInstance().getRefreshToken(accessToken);
        loginUser.setAccessToken(accessToken.getId());
        loginUser.setRefreshToken(refreshToken.getId());
        loginUser.setExp(accessToken.exp());

        logService.saveOperLog(WebUtil.buildLoginLog(request, user));
        return ResultVo.success(loginUser);
    }
}
