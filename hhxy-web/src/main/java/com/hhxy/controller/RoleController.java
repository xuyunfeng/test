package com.hhxy.controller;

import com.hhxy.common.entity.SysRole;
import com.hhxy.common.enums.ResultMessageEnum;
import com.hhxy.common.exception.HhxyException;
import com.hhxy.common.req.ReqId;
import com.hhxy.common.req.ReqRole;
import com.hhxy.common.vo.ResultVo;
import com.hhxy.service.ISysRoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @since 2021/5/14 21:56
 */
@RestController
@Api(tags = "角色控制")
@RequestMapping("/role")
public class RoleController {

    @Autowired
    private ISysRoleService roleService;

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ApiOperation(value = "create", notes = "创建角色")
    public ResultVo<Boolean> create(HttpServletRequest request, @RequestBody ReqRole role) {
        if (checkExists(role.getRoleName(), role.getRoleId())) {
            throw new HhxyException(ResultMessageEnum.ROLE_EXISTS);
        }
        SysRole sysRole = role.build();
        roleService.saveOrUpdate(sysRole);
        return ResultVo.success(true);
    }

    /**
     * 检查角色名是否已存在
     * @param name
     * @param id
     * @return
     */
    private boolean checkExists(String name, Long id) {
        List<SysRole> roles = roleService.getByName(name);
        if (CollectionUtils.isEmpty(roles)) {
            return false;
        } else {
            if (id != null) {
                boolean exists = false;
                for (SysRole role : roles) {
                    if (role.getRoleId() != id) {
                        exists = true;
                        break;
                    }
                }
                return exists;
            } else {
                return true;
            }
        }
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ApiOperation(value = "update", notes = "更新角色")
    public ResultVo<Boolean> update(HttpServletRequest request, @RequestBody ReqRole role) {
       return this.create(request, role);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ApiOperation(value = "delete", notes = "删除角色")
    public ResultVo<Boolean> delete(HttpServletRequest request, @RequestBody ReqId id) {
        roleService.removeById(id.getId());
        return ResultVo.success(true);
    }

    @RequestMapping(value = "/get", method = RequestMethod.POST)
    @ApiOperation(value = "get", notes = "获取角色")
    public ResultVo<SysRole> get(HttpServletRequest request, @RequestBody ReqId id) {
        SysRole role = roleService.getById(id.getId());
        return ResultVo.success(role);
    }
}
