package com.hhxy.exception;

import com.hhxy.common.enums.ResultMessageEnum;
import com.hhxy.common.exception.HhxyException;
import com.hhxy.common.vo.ResultVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @since 2021/5/13 19:50
 */
@ControllerAdvice
@Slf4j
public class HhxyExceptionHandler {

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public ResultVo exceptionHandler(Exception e) {
        log.error("执行异常: {}", e.getMessage(), e);
        return ResultVo.failed(ResultMessageEnum.UNKNOW);
    }

    @ExceptionHandler(value = BindException.class)
    @ResponseBody
    public ResultVo exceptionHandler(BindException e) {
        log.error("参数校验失败：[{}]", e.getMessage(), e);
        return new ResultVo(ResultMessageEnum.PARAM_ERR.getCode(), e.getFieldErrors().get(0).getDefaultMessage(), null);
    }

    @ExceptionHandler(value = HhxyException.class)
    @ResponseBody
    public ResultVo exceptionHandler(HhxyException e) {
        log.error("执行异常: {}", e.getMessage(), e);
        return new ResultVo(e.getCode(), e.getMessage(), null);
    }
}
