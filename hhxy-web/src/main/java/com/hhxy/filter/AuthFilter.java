package com.hhxy.filter;

import com.google.gson.JsonObject;
import com.hhxy.common.cache.impl.ThreadLocalCacheServiceImpl;
import com.hhxy.common.entity.User;
import com.hhxy.common.enums.ResultMessageEnum;
import com.hhxy.common.exception.HhxyException;
import com.hhxy.common.util.ThreadLocalUtil;
import com.hhxy.util.TokenFactory;
import com.hhxy.util.WebUtil;
import org.apache.http.protocol.HTTP;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;
import org.springframework.web.util.UrlPathHelper;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @since 2021/3/24 23:46
 */
public class AuthFilter implements Filter {

    private UrlPathHelper urlPathHelper = new UrlPathHelper();

    private PathMatcher matcher = new AntPathMatcher();

    private String[] pattern = new String[]{"/**/login","/**/doc.html","/**/*.js", "/**/*.css", "/webjars/**", "/swagger*", "/v2/*"};

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthFilter.class);
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        String url = urlPathHelper.getLookupPathForRequest(request);
        String ip = WebUtil.getRemotIp(request);
        // 循环判断
        for (int i = 0; i <= pattern.length - 1; i++) {
            if (matcher.match(pattern[i], url)) {
                // 如果匹配，那么不进行登陆控制
                filterChain.doFilter(request, servletResponse);
                return;
            }
        }
        User user = null;
        JsonObject jsonObject = null;
        try {
            user = TokenFactory.getInstance().getUser(WebUtil.getToken(request));
            if (user == null) {
                throw new HhxyException(ResultMessageEnum.ACCESS_TOKEN_EXP);
            }
            // 处理用户信息报错
        } catch (HhxyException e) {
            jsonObject = new JsonObject();
            jsonObject.addProperty("code", e.getCode());
            jsonObject.addProperty("message", e.getMessage());
            LOGGER.error("Check user failed:{}, url[{}] ip[{}]", e.getMessage(), url, ip, e);
        } catch (Exception e) {
            jsonObject = new JsonObject();
            jsonObject.addProperty("code", ResultMessageEnum.UNKNOW.getCode());
            jsonObject.addProperty("message", ResultMessageEnum.UNKNOW.getMessage());
            LOGGER.error("Check user failed:{}, url[{}] ip[{}]", e.getMessage(), url, ip, e);
        }
        if (jsonObject != null) {
            PrintWriter out = null;
            try {
                servletResponse.setCharacterEncoding(HTTP.UTF_8);
                out = servletResponse.getWriter();
                out.println(jsonObject.toString());
            } catch (Exception e) {
                LOGGER.error("输出IO关闭异常[{}]!", e.getMessage(), e);
            } finally {
                if (out != null) {
                    out.flush();
                    out.close();
                }
            }
            return;
        }
        ThreadLocalUtil.setUser(user);
        LOGGER.info("request url, url[{}] ip[{}]", url, ip);
        filterChain.doFilter(servletRequest, servletResponse);
        ThreadLocalUtil.clean();
    }
}
