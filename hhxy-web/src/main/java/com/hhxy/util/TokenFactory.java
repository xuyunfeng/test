package com.hhxy.util;

import com.hhxy.common.cache.CacheFactory;
import com.hhxy.common.entity.User;
import com.hhxy.common.enums.ResultMessageEnum;
import com.hhxy.common.exception.HhxyException;
import com.hhxy.common.token.AccessToken;
import com.hhxy.common.token.IToken;
import com.hhxy.common.token.RefreshToken;
import com.hhxy.common.util.IDUtil;
import com.hhxy.common.util.TimeUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

/**
 * @since 2021/3/18 23:06
 */
public class TokenFactory {

    private static Logger LOGGER = LoggerFactory.getLogger(TokenFactory.class);

    private static TokenFactory instance;

    private TokenFactory(){

    }

    public static final String AT_RT_KEY = "at-rt.key:";

    public static TokenFactory getInstance() {
        if (instance == null) {
            instance = new TokenFactory();
        }
        return instance;
    }

    public <T> IToken get(String id, Class<T> clazz) {
        try {
            return (IToken) CacheFactory.getInstance().get(id, clazz);
        } catch (Exception e) {
            LOGGER.error("Get token err:" + e.getMessage(), e);
            return null;
        }
    }

    public IToken getNewToken(String id) {
        String rt = CacheFactory.getInstance().get(getAtRtKey(id), String.class);
        if (StringUtils.isBlank(rt)) {
            return null;
        }
        RefreshToken token = CacheFactory.getInstance().get(rt, RefreshToken.class);
        if (token == null) {
            return null;
        }
        String newId = IDUtil.nextId();
        AccessToken accessToken = new AccessToken(newId, token.getUser());
        String u = StringUtils.isBlank(token.getUser().getUserName()) ? token.getUser().getId().toString() : token.getUser().getUserName();
        addAccessToken(u, accessToken);
        CacheFactory.getInstance().add(getAtRtKey(accessToken.getId()), rt, (int) TimeUtil.seconds(accessToken.exp(), TimeUnit.MINUTES));
        return accessToken;
    }

    public IToken getRefreshToken(String token) {
       String rt = CacheFactory.getInstance().get(token, String.class);
       return get(rt, RefreshToken.class);
    }

    public void removeRefresToken(String token, String refreshToken) {
        CacheFactory.getInstance().delete(refreshToken);
        CacheFactory.getInstance().delete(token);
    }

    protected void addAccessToken(String user, AccessToken accessToken) {
        String atKey = accessToken.getId();
        CacheFactory.getInstance().add(atKey, accessToken, (int) TimeUtil.seconds(accessToken.exp(), TimeUnit.MINUTES));
        CacheFactory.getInstance().add("AT-" + user, atKey, (int) TimeUtil.seconds(accessToken.exp(), TimeUnit.MINUTES));
    }

    protected void addRefreshToken(String user, AccessToken accessToken, RefreshToken refreshToken) {
        String at = accessToken.getId();
        String rt = refreshToken.getId();
        CacheFactory.getInstance().add(rt, refreshToken, (int) TimeUtil.seconds(refreshToken.exp(), TimeUnit.MINUTES));
        CacheFactory.getInstance().add(getAtRtKey(at), rt, (int) TimeUtil.seconds(accessToken.exp(), TimeUnit.MINUTES));
        CacheFactory.getInstance().add("RT-" + user, rt, (int) TimeUtil.seconds(refreshToken.exp(), TimeUnit.MINUTES));
    }

    public IToken getRefreshToken(IToken accessToken) {
        String at = accessToken.getId();
        String rt = CacheFactory.getInstance().get(getAtRtKey(at), String.class);
        return get(rt, RefreshToken.class);
    }

    protected String getAtRtKey(String at) {
        return AT_RT_KEY + at;
    }

    /**
     * 让上一个用户失效
     * @param user
     */
    public void expireOnline(User user) {
        String u = StringUtils.isBlank(user.getUserName()) ? user.getId().toString() : user.getUserName();
        String at = CacheFactory.getInstance().get(u, String.class);
        String rt = CacheFactory.getInstance().get(u, String.class);
        if (StringUtils.isNotBlank(at)) {
            CacheFactory.getInstance().delete(at);
            CacheFactory.getInstance().delete("AT-" + u);
        }
        if (StringUtils.isNotBlank(rt)) {
            CacheFactory.getInstance().delete(rt);
            CacheFactory.getInstance().delete("RT-" + user);
        }

    }

    public User getUser(String id) {
        IToken token = get(id, AccessToken.class);
        if (token == null) {
            return null;
        } else if (token.expired()) {
            remove(id, AccessToken.class);
            return null;
        } else {
            token.renewal();
            CacheFactory.getInstance().update(id, token, (int) TimeUtil.seconds(token.exp(), TimeUnit.MINUTES));
            return token.getUser();
        }
    }

    public <T> void remove(String id, Class<T> clazz) {
        try {
            CacheFactory.getInstance().delete(id);
        } catch (Exception e) {
            LOGGER.error("Get token err:" + e.getMessage(), e);
        }
    }

    public IToken build(User user) {
        if (user == null) {
            return null;
        }
        User cacheUser = user.cloneSelf();
        String id = IDUtil.nextId();
        String rt = IDUtil.nextId();
        expireOnline(cacheUser);
        return build(id, rt, cacheUser);
    }

    public IToken build(String at, String rt, User user) {
        String u = StringUtils.isBlank(user.getUserName()) ? user.getId().toString() : user.getUserName();
        if (StringUtils.isBlank(at) || user == null) {
            return null;
        }
        AccessToken accessToken = new AccessToken(at, user);
        addAccessToken(u, accessToken);
        if (StringUtils.isNotBlank(rt)) {
            RefreshToken refreshToken = new RefreshToken(rt, user);
            addRefreshToken(u, accessToken, refreshToken);
        }
        expireOnline(user);
        return accessToken;
    }

    public String getUserId(String token) {
        if (StringUtils.isBlank(token)) {
            LOGGER.warn("用户token信息为空！");
            throw new HhxyException(ResultMessageEnum.ACCESS_TOKEN_NOT_EXIST);
        }
        IToken accessToken = get(token, AccessToken.class);
        if (accessToken == null || accessToken.expired()) {
            LOGGER.warn("用户已失效，请刷新或者重新登陆[{}]", token);
            throw new HhxyException(ResultMessageEnum.ACCESS_TOKEN_EXP);
        }
        return accessToken.getUser().getId().toString();
    }
}
