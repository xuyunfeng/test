package com.hhxy.util;

import com.hhxy.common.cache.impl.ThreadLocalCacheServiceImpl;
import com.hhxy.common.entity.OperLog;
import com.hhxy.common.entity.SocialUser;
import com.hhxy.common.entity.SysUser;
import com.hhxy.common.entity.User;
import com.hhxy.common.enums.ResultMessageEnum;
import com.hhxy.common.exception.HhxyException;
import com.hhxy.common.util.IPUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;

/**
 * @since 2021/3/24 23:39
 */
public class WebUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebUtil.class);

    public static String getToken(HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        if (StringUtils.isBlank(token)) {
            LOGGER.error("request token is null, illegal request!");
            throw new HhxyException(ResultMessageEnum.ILLEGAL_REQ);
        }
        return token;
    }
    public static Long getUserId() {
        SysUser user = ThreadLocalCacheServiceImpl.getIntance().get("user", SysUser.class);
        return user != null ? user.getId() : null;
    }

    public static String getRemotIp(HttpServletRequest request) {
        String ip = request.getRemoteAddr();
        ip = IPUtil.isIp(request.getRemoteAddr()) ? request.getRemoteAddr() : null;
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }

        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }

        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("x-forwarded-for");
        }

        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("http_client_ip");
        }

        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }

        ip = IPUtil.isIp(ip) ? ip : null;
        if (ip != null && ip.indexOf(",") != -1) {
            ip = ip.substring(ip.lastIndexOf(",") + 1, ip.length()).trim();
        }

        return ip;
    }

    public static OperLog buildLoginLog(HttpServletRequest request, User user) {
        OperLog log = new OperLog();
        log.setOperIp(getRemotIp(request));
        log.setUserId(user.getId());
        log.setUserName(user.getUserName());
        log.setOperType("login");
        log.setCreateTime(System.currentTimeMillis());
        return log;
    }
}
