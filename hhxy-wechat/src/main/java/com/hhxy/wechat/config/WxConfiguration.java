package com.hhxy.wechat.config;

import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.mp.api.WxMpInMemoryConfigStorage;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.impl.WxMpServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

/**
 * @since 2021/3/21 21:54
 */
@Slf4j
@Configuration
@EnableConfigurationProperties(WxProperties.class)
public class WxConfiguration {

    public static WxMpService getWxMpService() {
        return wxMpService;
    }

    private static WxMpService wxMpService;

    @Autowired
    private WxProperties properties;

    @PostConstruct
    public void initService() {
        WxMpInMemoryConfigStorage configStorage = new WxMpInMemoryConfigStorage();
        configStorage.setAesKey(properties.getAesKey());
        configStorage.setAppId(properties.getAppId());
        configStorage.setSecret(properties.getSecret());
        configStorage.setToken(properties.getToken());
        wxMpService = new WxMpServiceImpl();
        wxMpService.setWxMpConfigStorage(configStorage);
    }
}
