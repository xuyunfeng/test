package com.hhxy.wechat.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @since 2021/3/21 21:50
 */
@ConfigurationProperties(prefix = "wechat")
public class WxProperties {

    public boolean isEnable() {
        return enable;
    }

    public WxProperties setEnable(boolean enable) {
        this.enable = enable;
        return this;
    }

    private boolean enable=true;

    private String appId;

    private String secret;

    private String token;

    private String aesKey;

    private String mchid;

    private String mchSerialNo;

    private String notifyUrl;

    private String privateCertificate;

    private String payUrl = "https://api.mch.weixin.qq.com/v3/pay/transactions/app";

    private String statusUrl = "https://api.mch.weixin.qq.com/v3/pay/transactions/out-trade-no/";

    private String api3;

    public String getApi3() {
        return api3;
    }

    public WxProperties setApi3(String api3) {
        this.api3 = api3;
        return this;
    }

    public String getStatusUrl() {
        return statusUrl;
    }

    public WxProperties setStatusUrl(String statusUrl) {
        this.statusUrl = statusUrl;
        return this;
    }

    public String getPayUrl() {
        return payUrl;
    }

    public WxProperties setPayUrl(String payUrl) {
        this.payUrl = payUrl;
        return this;
    }

    public String getMchSerialNo() {
        return mchSerialNo;
    }

    public WxProperties setMchSerialNo(String mchSerialNo) {
        this.mchSerialNo = mchSerialNo;
        return this;
    }

    public String getPrivateCertificate() {
        return privateCertificate;
    }

    public WxProperties setPrivateCertificate(String privateCertificate) {
        this.privateCertificate = privateCertificate;
        return this;
    }

    public String getMchid() {
        return mchid;
    }

    public WxProperties setMchid(String mchid) {
        this.mchid = mchid;
        return this;
    }

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public WxProperties setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
        return this;
    }

    public String getAppId() {
        return appId;
    }

    public WxProperties setAppId(String appId) {
        this.appId = appId;
        return this;
    }

    public String getSecret() {
        return secret;
    }

    public WxProperties setSecret(String secret) {
        this.secret = secret;
        return this;
    }

    public String getToken() {
        return token;
    }

    public WxProperties setToken(String token) {
        this.token = token;
        return this;
    }

    public String getAesKey() {
        return aesKey;
    }

    public WxProperties setAesKey(String aesKey) {
        this.aesKey = aesKey;
        return this;
    }
}
