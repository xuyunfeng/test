package com.hhxy.wechat.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Data
public class Amount {

    /**
     * 总金额 单位分
     */
    private int total;

    /**
     * 用户支付金额
     */
    private int payer_total;

    /**
     * 货币类型
     * CNY：人民币，境内商户号仅支持人民币。
     * 示例值：CNY
     */
    private String currency;

    /**
     * 用户支付币种
     */
    private String payer_currency;

}
