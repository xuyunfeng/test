package com.hhxy.wechat.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @since 2021/3/27 23:10
 */
@Setter
@Getter
@Data
public class NotifyBody {

    /**
     * 公众号ID
     */
    private String appid;
    /**
     * 直连商户号
     */
    private String mchid;
    /**
     * 商户订单号
     */
    private String out_trade_no;
    /**
     * 微信支付订单号
     */
    private String transaction_id;
    /**
     * 交易类型
     * JSAPI：公众号支付
     * NATIVE：扫码支付
     * APP：APP支付
     * MICROPAY：付款码支付
     * MWEB：H5支付
     * FACEPAY：刷脸支付
     */
    private String trade_type;
    /**
     * 交易状态
     * SUCCESS：支付成功
     * REFUND：转入退款
     * NOTPAY：未支付
     * CLOSED：已关闭
     * REVOKED：已撤销（付款码支付）
     * USERPAYING：用户支付中（付款码支付）
     * PAYERROR：支付失败(其他原因，如银行返回失败)
     */
    private String trade_state;
    /**
     * 交易状态描述
     */
    private String trade_state_desc;
    /**
     * 付款银行
     * 银行类型，采用字符串类型的银行标识。
     * 示例值：CMC
     */
    private String bank_type;
    /**
     * 附加数据
     */
    private String attach;
    /**
     * 支付完成时间
     */
    private Date success_time;
    /**
     * 支付者
     */
    private Payer payer;
    /**
     * 订单金额
     */
    private Amount amount;
    /**
     * 场景信息
     */
    private ScenceInfo scene_info;


}
