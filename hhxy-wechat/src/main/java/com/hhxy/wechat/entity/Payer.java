package com.hhxy.wechat.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * @author xueyj
 * @date 13:56 2021/1/5
 */
@Data
@Setter
@Getter
public class Payer {

    /**
     * 用户标识
     */
    private String openid;
}
