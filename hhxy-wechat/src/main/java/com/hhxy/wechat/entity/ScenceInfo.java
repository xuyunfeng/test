package com.hhxy.wechat.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * @author xueyj
 * @date 14:01 2021/1/5
 */
@Setter
@Getter
@Data
public class ScenceInfo {


    /**
     * 商户端设备号
     * 终端设备号（门店号或收银设备ID）。
     * 特殊规则：长度最小7个字节
     * 示例值：POS1:1
     */
    private String device_id;


}
