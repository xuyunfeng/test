package com.hhxy.wechat.service;

import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.bean.result.WxMpOAuth2AccessToken;
import me.chanjar.weixin.mp.bean.result.WxMpUser;

/**
 * @since 2021/3/21 21:26
 */
public interface IWeChatService {

    /**
     * 基于一次性授权码获取accessToken
     * @param code
     * @return
     * @throws WxErrorException
     */
    WxMpOAuth2AccessToken getToken(String code) throws WxErrorException;

    /**
     * 基于accessToken获取用户信息
     * @param accessToken
     * @return
     * @throws WxErrorException
     */
    WxMpUser getUser(WxMpOAuth2AccessToken accessToken) throws WxErrorException;

    /**
     * 基于refreshToken获取授权码
     * @param refreshToken
     * @return
     * @throws WxErrorException
     */
    WxMpOAuth2AccessToken refreshToken(String refreshToken) throws WxErrorException;
}
