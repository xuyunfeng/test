package com.hhxy.wechat.service;

import com.alibaba.fastjson.JSONObject;
import com.hhxy.common.entity.SocialUser;
import com.hhxy.common.entity.User;
import com.hhxy.common.req.ReqLogin;
import com.hhxy.common.req.ReqWechatLogin;
import com.hhxy.service.ILoginService;
import com.hhxy.service.ISocialUserService;
import com.hhxy.wechat.config.WxConfiguration;
import com.hhxy.wechat.config.WxProperties;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.result.WxMpOAuth2AccessToken;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @since 2021/3/21 21:27
 */
@Service("wechatServiceImpl")
public class WeChatServiceImpl implements IWeChatService, ILoginService {

    @Autowired
    private ISocialUserService userService;

    @Autowired
    private WxProperties properties;

    @Override
    public WxMpOAuth2AccessToken getToken(String code) throws WxErrorException {
        WxMpService service = WxConfiguration.getWxMpService();
        WxMpOAuth2AccessToken auth2AccessToken = service.oauth2getAccessToken(code);
        return auth2AccessToken;
    }

    @Override
    public WxMpUser getUser(WxMpOAuth2AccessToken accessToken) throws WxErrorException {
        WxMpService service = WxConfiguration.getWxMpService();
        WxMpUser user = service.oauth2getUserInfo(accessToken, null);
        return user;
    }

    @Override
    public WxMpOAuth2AccessToken refreshToken(String refreshToken) throws WxErrorException {
        WxMpService service = WxConfiguration.getWxMpService();
        WxMpOAuth2AccessToken token = service.oauth2refreshAccessToken(refreshToken);
        return token;
    }

    @Override
    public User login(ReqLogin login) throws Exception {
        if (!(login instanceof ReqWechatLogin)) {
            return null;
        }
        WxMpUser user = null;
        if (properties.isEnable()) {
            WxMpOAuth2AccessToken accessToken = getToken(((ReqWechatLogin) login).getCode());
            user = getUser(accessToken);
        } else {
            user = new WxMpUser();
            user.setOpenId("oooxxxx");
            user.setNickname("李小二");
        }
        // 微信用户以openid作为登录名
        SocialUser sysUser = userService.queryByUserName(user.getOpenId());
        if (sysUser == null) {
            sysUser = new SocialUser();
            sysUser.setNickName(user.getNickname());
            sysUser.setUserName(user.getOpenId());
            sysUser.setCreateTime(System.currentTimeMillis());
            sysUser.setLastVisitTime(System.currentTimeMillis());
            sysUser.setAvatar(user.getHeadImgUrl());
            sysUser.setAvatarType("0");
            userService.save(sysUser);
        } else {
            sysUser.setLastVisitTime(System.currentTimeMillis());
            if (!"1".equals(sysUser.getAvatarType())) {
                sysUser.setAvatar(user.getHeadImgUrl());
            }
            userService.updateById(sysUser);
        }
        return sysUser;
    }

    @Override
    public ReqLogin build(ReqLogin reqLogin) {
        if (reqLogin.getLoginType() == 2) {
            return JSONObject.parseObject(reqLogin.getDataJson(), ReqWechatLogin.class);
        }
        return null;
    }
}
