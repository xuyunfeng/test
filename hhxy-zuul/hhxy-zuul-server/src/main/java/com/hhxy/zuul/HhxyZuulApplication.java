package com.hhxy.zuul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * @since 2021/5/13 21:12
 */
@SpringBootApplication
@EnableZuulProxy
public class HhxyZuulApplication {
    public static void main(String[] args) {
        SpringApplication.run(HhxyZuulApplication.class, args);
    }
}
